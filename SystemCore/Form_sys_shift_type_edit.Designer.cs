﻿namespace SystemCore
{
    partial class Form_sys_shift_type_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerBegin = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerValue = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCnl = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxShift = new System.Windows.Forms.TextBox();
            this.textBoxComm = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.buttonAddDep = new System.Windows.Forms.Button();
            this.buttonDelDep = new System.Windows.Forms.Button();
            this.checkBoxShift = new System.Windows.Forms.CheckBox();
            this.buttonAddDepTorg = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxDinner = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonWork = new System.Windows.Forms.Button();
            this.textBoxWork = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dateTimePickerBegin
            // 
            this.dateTimePickerBegin.Enabled = false;
            this.dateTimePickerBegin.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerBegin.Location = new System.Drawing.Point(193, 88);
            this.dateTimePickerBegin.Name = "dateTimePickerBegin";
            this.dateTimePickerBegin.ShowUpDown = true;
            this.dateTimePickerBegin.Size = new System.Drawing.Size(86, 20);
            this.dateTimePickerBegin.TabIndex = 0;
            this.dateTimePickerBegin.Value = new System.DateTime(2016, 7, 23, 0, 0, 0, 0);
            // 
            // dateTimePickerValue
            // 
            this.dateTimePickerValue.Enabled = false;
            this.dateTimePickerValue.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerValue.Location = new System.Drawing.Point(193, 124);
            this.dateTimePickerValue.Name = "dateTimePickerValue";
            this.dateTimePickerValue.ShowUpDown = true;
            this.dateTimePickerValue.Size = new System.Drawing.Size(86, 20);
            this.dateTimePickerValue.TabIndex = 1;
            this.dateTimePickerValue.Value = new System.DateTime(2016, 7, 23, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Смена";
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(377, 275);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "Ок";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCnl
            // 
            this.buttonCnl.Location = new System.Drawing.Point(458, 275);
            this.buttonCnl.Name = "buttonCnl";
            this.buttonCnl.Size = new System.Drawing.Size(75, 23);
            this.buttonCnl.TabIndex = 4;
            this.buttonCnl.Text = "Отмена";
            this.buttonCnl.UseVisualStyleBackColor = true;
            this.buttonCnl.Click += new System.EventHandler(this.buttonCnl_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Описание";
            // 
            // textBoxShift
            // 
            this.textBoxShift.Location = new System.Drawing.Point(110, 21);
            this.textBoxShift.MaxLength = 3;
            this.textBoxShift.Name = "textBoxShift";
            this.textBoxShift.Size = new System.Drawing.Size(48, 20);
            this.textBoxShift.TabIndex = 6;
            // 
            // textBoxComm
            // 
            this.textBoxComm.Location = new System.Drawing.Point(110, 57);
            this.textBoxComm.MaxLength = 50;
            this.textBoxComm.Name = "textBoxComm";
            this.textBoxComm.Size = new System.Drawing.Size(212, 20);
            this.textBoxComm.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(107, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Начало";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(107, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Длительность";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(351, 21);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(101, 173);
            this.listBox1.TabIndex = 10;
            // 
            // buttonAddDep
            // 
            this.buttonAddDep.Location = new System.Drawing.Point(458, 21);
            this.buttonAddDep.Name = "buttonAddDep";
            this.buttonAddDep.Size = new System.Drawing.Size(75, 23);
            this.buttonAddDep.TabIndex = 11;
            this.buttonAddDep.Text = "Добавить";
            this.buttonAddDep.UseVisualStyleBackColor = true;
            this.buttonAddDep.Click += new System.EventHandler(this.buttonAddDep_Click);
            // 
            // buttonDelDep
            // 
            this.buttonDelDep.Location = new System.Drawing.Point(458, 108);
            this.buttonDelDep.Name = "buttonDelDep";
            this.buttonDelDep.Size = new System.Drawing.Size(75, 23);
            this.buttonDelDep.TabIndex = 12;
            this.buttonDelDep.Text = "Удалить";
            this.buttonDelDep.UseVisualStyleBackColor = true;
            this.buttonDelDep.Click += new System.EventHandler(this.buttonDelDep_Click);
            // 
            // checkBoxShift
            // 
            this.checkBoxShift.AutoSize = true;
            this.checkBoxShift.Location = new System.Drawing.Point(27, 93);
            this.checkBoxShift.Name = "checkBoxShift";
            this.checkBoxShift.Size = new System.Drawing.Size(77, 17);
            this.checkBoxShift.TabIndex = 13;
            this.checkBoxShift.Text = "Диапазон";
            this.checkBoxShift.UseVisualStyleBackColor = true;
            this.checkBoxShift.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // buttonAddDepTorg
            // 
            this.buttonAddDepTorg.Location = new System.Drawing.Point(458, 50);
            this.buttonAddDepTorg.Name = "buttonAddDepTorg";
            this.buttonAddDepTorg.Size = new System.Drawing.Size(75, 23);
            this.buttonAddDepTorg.TabIndex = 14;
            this.buttonAddDepTorg.Text = "Доб Торг";
            this.buttonAddDepTorg.UseVisualStyleBackColor = true;
            this.buttonAddDepTorg.Click += new System.EventHandler(this.buttonAddDepTorg_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(247, 250);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 23);
            this.button1.TabIndex = 15;
            this.button1.Text = ".";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxDinner
            // 
            this.textBoxDinner.Enabled = false;
            this.textBoxDinner.Location = new System.Drawing.Point(110, 252);
            this.textBoxDinner.Name = "textBoxDinner";
            this.textBoxDinner.Size = new System.Drawing.Size(131, 20);
            this.textBoxDinner.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 255);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Питание";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(107, 168);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Окончание";
            // 
            // dateTimePickerEnd
            // 
            this.dateTimePickerEnd.Enabled = false;
            this.dateTimePickerEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerEnd.Location = new System.Drawing.Point(193, 162);
            this.dateTimePickerEnd.Name = "dateTimePickerEnd";
            this.dateTimePickerEnd.ShowUpDown = true;
            this.dateTimePickerEnd.Size = new System.Drawing.Size(86, 20);
            this.dateTimePickerEnd.TabIndex = 19;
            this.dateTimePickerEnd.Value = new System.DateTime(2016, 8, 5, 0, 0, 0, 0);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(458, 79);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 20;
            this.button2.Text = "Доб не тор";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonWork
            // 
            this.buttonWork.Location = new System.Drawing.Point(247, 207);
            this.buttonWork.Name = "buttonWork";
            this.buttonWork.Size = new System.Drawing.Size(27, 23);
            this.buttonWork.TabIndex = 21;
            this.buttonWork.Text = ".";
            this.buttonWork.UseVisualStyleBackColor = true;
            this.buttonWork.Click += new System.EventHandler(this.buttonWork_Click);
            // 
            // textBoxWork
            // 
            this.textBoxWork.Enabled = false;
            this.textBoxWork.Location = new System.Drawing.Point(110, 209);
            this.textBoxWork.Name = "textBoxWork";
            this.textBoxWork.Size = new System.Drawing.Size(131, 20);
            this.textBoxWork.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 212);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Тип смены";
            // 
            // Form_sys_shift_type_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 319);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxWork);
            this.Controls.Add(this.buttonWork);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dateTimePickerEnd);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxDinner);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonAddDepTorg);
            this.Controls.Add(this.checkBoxShift);
            this.Controls.Add(this.buttonDelDep);
            this.Controls.Add(this.buttonAddDep);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxComm);
            this.Controls.Add(this.textBoxShift);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonCnl);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePickerValue);
            this.Controls.Add(this.dateTimePickerBegin);
            this.Name = "Form_sys_shift_type_edit";
            this.Text = "Form_sys_shift_type_edit";
            this.Load += new System.EventHandler(this.Form_sys_shift_type_edit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePickerBegin;
        private System.Windows.Forms.DateTimePicker dateTimePickerValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCnl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxShift;
        private System.Windows.Forms.TextBox textBoxComm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button buttonAddDep;
        private System.Windows.Forms.Button buttonDelDep;
        private System.Windows.Forms.CheckBox checkBoxShift;
        private System.Windows.Forms.Button buttonAddDepTorg;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxDinner;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dateTimePickerEnd;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonWork;
        private System.Windows.Forms.TextBox textBoxWork;
        private System.Windows.Forms.Label label7;
    }
}