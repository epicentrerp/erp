﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SystemCore
{
    public partial class Form_sys_login : Form
    {
        private int old_x, old_y;

        public Form_sys_login()
        {
            InitializeComponent();
        }

        private void Form_sys_login_Load(object sender, EventArgs e)
        {
            UpdateLogin();
        }

        private void UpdateLoginGrid()
        {
            if (dataGridViewLogin.RowCount != 0)
            {
                old_y = dataGridViewLogin.CurrentCell.RowIndex;
                old_x = dataGridViewLogin.CurrentCell.ColumnIndex;
            }

            UpdateLogin();

            if (dataGridViewLogin.RowCount > old_y)
            {
                if (old_y == 0)
                    dataGridViewLogin.CurrentCell = dataGridViewLogin[1, 0];
                else
                    dataGridViewLogin.CurrentCell = dataGridViewLogin[old_x, old_y];
            }
            else
            {
                if (old_y != 0 && dataGridViewLogin.RowCount != 0)
                {
                    dataGridViewLogin.CurrentCell = dataGridViewLogin[old_x, dataGridViewLogin.RowCount - 1];
                }
            }
        }

        private void UpdateLogin()
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            string sql = "SELECT [id],[name_ru],[password],[status],[status_connect],[comp],[data_con] FROM [login_view]";
            SqlCommand sqlCmd = new SqlCommand(sql, sqlConn);

            sqlCmd.CommandType = CommandType.Text;

            sqlConn.Open();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = sqlCmd;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            dataGridViewLogin.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            UpdateLoginGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form_sys_login_edit editEmp = new Form_sys_login_edit("");
            editEmp.MdiParent = this.MdiParent;
            editEmp.FormClosing += (sender1, e1) =>
            {
                UpdateLoginGrid();
            };
            editEmp.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form_sys_login_edit editEmp = new Form_sys_login_edit(dataGridViewLogin[0, dataGridViewLogin.CurrentCell.RowIndex].Value.ToString());
            editEmp.MdiParent = this.MdiParent;
            editEmp.FormClosing += (sender1, e1) =>
            {
                UpdateLoginGrid();
            };
            editEmp.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            UpdateLogin();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Form_sys_login_edit editEmp = new Form_sys_login_edit(dataGridViewLogin[0, dataGridViewLogin.CurrentCell.RowIndex].Value.ToString());
            editEmp.MdiParent = this.MdiParent;
            editEmp.FormClosing += (sender1, e1) =>
            {
                UpdateLoginGrid();
            };
            editEmp.Show();
        }
    }
}
