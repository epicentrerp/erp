﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Journal;

namespace SystemCore
{
    public partial class Form_sys_section_edit : Form
    {
        private string id;
        Form_dep depSelect;

        public Form_sys_section_edit(string id)
        {
            InitializeComponent();
            this.id = id;
        }

        private void Form_sys_section_edit_Load(object sender, EventArgs e)
        {
            if (id != "0")
            {
                SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                SqlCommand sqlCmd = new SqlCommand();

                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Connection = sqlConn;
                string sql = "SELECT [dep],[name],[status],[id]"
                            + " FROM [section] WHERE id = @id_sec";
                sqlCmd.CommandText = sql;
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Parameters.AddWithValue("@id_sec", id);

                sqlConn.Open();

                SqlDataReader dr = sqlCmd.ExecuteReader();
                {
                    while (dr.Read())
                    {
                        textBoxSec.Text = dr.GetString(dr.GetOrdinal("name")).ToString();
                        textBoxDep.Text = dr.GetInt32(dr.GetOrdinal("dep")).ToString();
                        textBoxStat.Text = dr.GetInt32(dr.GetOrdinal("status")).ToString();

                        // чтение данных из ридера 
                    }
                }
                sqlConn.Close();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (depSelect == null || depSelect.IsDisposed)
            {
                depSelect = new Form_dep(ProSetting.User);
                depSelect.MdiParent = this.MdiParent;
                depSelect.FormClosing += (sender1, e1) =>
                {
                    if (depSelect.dep != null)
                    {
                        textBoxDep.Text = depSelect.dep;
                    }
                };
                depSelect.Show();
            }
            else
            {
                depSelect.Activate();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (id == "0")
            {
                try
                {
                    SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                    SqlCommand sqlCmd = new SqlCommand();

                    string sql = "INSERT INTO [balu].[dbo].[section] ([dep],[name],[status]"
                        +" VALUES(@dep, @name, @stat)";

                    sqlCmd.CommandType = CommandType.Text;
                    sqlCmd.CommandText = sql;
                    sqlCmd.Connection = sqlConn;

                    sqlCmd.Parameters.AddWithValue("@dep", textBoxDep.Text); // ели значение писаное из базы то через value
                    sqlCmd.Parameters.AddWithValue("@stat", textBoxStat.Text); // если значение записано хардкорно то через Item
                    sqlCmd.Parameters.AddWithValue("@name", textBoxSec.Text); // ели значение писаное из базы то через value

                    sqlConn.Open();
                    sqlCmd.ExecuteNonQuery();
                    sqlConn.Close();

                    this.Close();
                }
                catch (Exception )
                {
                    MessageBox.Show("Не всё поля заполненны корректно.");
                }
            }
            else
            {
                try
                {
                    SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                    SqlCommand sqlCmd = new SqlCommand();

                    string sql = "UPDATE [balu].[dbo].[section] SET [dep] = @dep, [name] = @name, [status] = @stat "
                    + " WHERE id = @id_sec ";

                    sqlCmd.CommandType = CommandType.Text;
                    sqlCmd.CommandText = sql;
                    sqlCmd.Connection = sqlConn;

                    sqlCmd.Parameters.AddWithValue("@dep", textBoxDep.Text); // ели значение писаное из базы то через value
                    sqlCmd.Parameters.AddWithValue("@stat", textBoxStat.Text); // если значение записано хардкорно то через Item
                    sqlCmd.Parameters.AddWithValue("@name", textBoxSec.Text); // ели значение писаное из базы то через value
                    sqlCmd.Parameters.AddWithValue("@id_sec", id); // ели значение писаное из базы то через value

                    sqlConn.Open();
                    sqlCmd.ExecuteNonQuery();
                    sqlConn.Close();

                    this.Close();
                }
                catch (Exception )
                {
                    MessageBox.Show("Не всё поля заполненны корректно.");
                }
            }
        }
    }
}
