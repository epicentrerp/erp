﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SystemCore
{
    public partial class Form_sys_dep_edit : Form
    {
        private string id_dep;

        public Form_sys_dep_edit(string id_dep)
        {
            InitializeComponent();
            this.id_dep = id_dep;
        }

        private void Form_sys_dep_edit_Load(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            string sql = "SELECT dep, dep_name FROM dep WHERE id = @id ";
            SqlCommand sqlCmd = new SqlCommand(sql, sqlConn);

            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.Parameters.AddWithValue("@id", id_dep);

            sqlConn.Open();

            using (SqlDataReader dr = sqlCmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    textBoxDep.Text = dr.GetInt32(dr.GetOrdinal("dep")).ToString();
                    textBoxDepName.Text = dr.GetString(dr.GetOrdinal("dep_name"));
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (textBoxDep.Text != "" & textBoxDepName.Text != "")
            {
                SqlConnection connection = new SqlConnection(ProSetting.ConnetionString);
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "UPDATE [dep] SET [dep] = @dep, [dep_name] = @dep_name "
                    + "WHERE [id] = @id";
                connection.Open();
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@dep", textBoxDep.Text);
                command.Parameters.AddWithValue("@dep_name", textBoxDepName.Text);
                command.Parameters.AddWithValue("@id", id_dep);
                command.ExecuteNonQuery();
                this.Close();
            }
            else
            {
                MessageBox.Show("Не все поля были заполнены.");
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
