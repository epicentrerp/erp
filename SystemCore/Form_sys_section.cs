﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SystemCore
{
    public partial class Form_sys_section : Form
    {
        public Form_sys_section()
        {
            InitializeComponent();
        }

        private void Form_section_Load(object sender, EventArgs e)
        {
            UpdateSec();
        }

        private void UpdateSec()
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            string sql = "SELECT [id],[dep],[name],[status] FROM [section] ORDER BY dep, status";
            SqlCommand sqlCmd = new SqlCommand(sql, sqlConn);

            sqlCmd.CommandType = CommandType.Text;

            sqlConn.Open();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = sqlCmd;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            dataGridViewSection.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }

        int old_y, old_x;
        private void UpdateSecGrid()
        {
            if (dataGridViewSection.RowCount != 0)
            {
                old_y = dataGridViewSection.CurrentCell.RowIndex;
                old_x = dataGridViewSection.CurrentCell.ColumnIndex;
            }

            UpdateSec();

            if (dataGridViewSection.RowCount > old_y)
            {
                if (old_y == 0)
                    dataGridViewSection.CurrentCell = dataGridViewSection[1, 0];
                else
                    dataGridViewSection.CurrentCell = dataGridViewSection[old_x, old_y];
            }
            else
            {
                if (old_y != 0 && dataGridViewSection.RowCount != 0)
                {
                    dataGridViewSection.CurrentCell = dataGridViewSection[old_x, dataGridViewSection.RowCount - 1];
                }
            }
        }

        private void dataGridViewSection_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Form_sys_section_edit editSec = new Form_sys_section_edit(dataGridViewSection[0, dataGridViewSection.CurrentCell.RowIndex].Value.ToString());
            editSec.MdiParent = this.MdiParent;
            editSec.FormClosing += (sender1, e1) =>
            {
                UpdateSecGrid();
            };
            editSec.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form_sys_section_edit editSec = new Form_sys_section_edit("0");
            editSec.MdiParent = this.MdiParent;
            editSec.FormClosing += (sender1, e1) =>
            {
                UpdateSecGrid();
            };
            editSec.Show();
        }
    }
}
