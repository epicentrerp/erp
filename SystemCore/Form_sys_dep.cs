﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SystemCore
{
    public partial class Form_sys_dep : Form
    {
        public Form_sys_dep()
        {
            InitializeComponent();
        }

        private void Form_sys_dep_Load(object sender, EventArgs e)
        {
            UpdateDepGrid();
        }

        private void dataGridViewDep_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Form_sys_dep_edit editDep = new Form_sys_dep_edit(dataGridViewDep[0, dataGridViewDep.CurrentCell.RowIndex].Value.ToString());
            editDep.MdiParent = this.MdiParent;
            editDep.FormClosing += (sender1, e1) =>
            {
                UpdateDepGrid();
            };
            editDep.Show();
        }

        int old_y, old_x;
        private void UpdateDepGrid()
        {
            if (dataGridViewDep.RowCount != 0)
            {
                old_y = dataGridViewDep.CurrentCell.RowIndex;
                old_x = dataGridViewDep.CurrentCell.ColumnIndex;
            }

            UpdateDep();

            if (dataGridViewDep.RowCount > old_y)
            {
                if (old_y == 0)
                    dataGridViewDep.CurrentCell = dataGridViewDep[1, 0];
                else
                    dataGridViewDep.CurrentCell = dataGridViewDep[old_x, old_y];
            }
            else
            {
                if (old_y != 0 && dataGridViewDep.RowCount != 0)
                {
                    dataGridViewDep.CurrentCell = dataGridViewDep[old_x, dataGridViewDep.RowCount - 1];
                }
            }
        }

        private void UpdateDep()
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            string sql = "SELECT id, dep, dep_name FROM [dep] ORDER BY dep";
            SqlCommand sqlCmd = new SqlCommand(sql, sqlConn);

            sqlCmd.CommandType = CommandType.Text;

            sqlConn.Open();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = sqlCmd;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            dataGridViewDep.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            Form_sys_dep_edit editDep = new Form_sys_dep_edit("");
            editDep.MdiParent = this.MdiParent;
            editDep.FormClosing += (sender1, e1) =>
            {
                UpdateDepGrid();
            };
            editDep.Show();
        }

    }
}
