﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SystemCore
{
    public partial class Form_sys_shift_type : Form
    {
        public Form_sys_shift_type()
        {
            InitializeComponent();
        }

        private void Form_shift_type_Load(object sender, EventArgs e)
        {
            UpdateShift();
        }

        private int old_x, old_y;
        private void UpdateShiftGrid()
        {
            if (dataGridViewShift.RowCount != 0)
            {
                old_y = dataGridViewShift.CurrentCell.RowIndex;
                old_x = dataGridViewShift.CurrentCell.ColumnIndex;
            }

            UpdateShift();

            if (dataGridViewShift.RowCount > old_y )
            {
                if (old_y == 0)
                    dataGridViewShift.CurrentCell = dataGridViewShift[1, 0];
                else
                    dataGridViewShift.CurrentCell = dataGridViewShift[old_x, old_y];
            }
            else
            {
                if (dataGridViewShift.RowCount != 0 && old_y != 0)
                {
                    dataGridViewShift.CurrentCell = dataGridViewShift[old_x, dataGridViewShift.RowCount - 1];
                }
            }
        }

        private void UpdateShift()
        {
            string sql = "SELECT shedule_shift_type.[id],[name],[shift_start],[shift_value],[comment],[shift_end],[dinner_type_name], work "
                + " FROM [shedule_shift_type] inner join [shedule_shift_dinner] "
                + " ON [shedule_shift_type].[dinner_type] = [shedule_shift_dinner].id_type "
                + " inner join [shedule_shift_work] ON shedule_shift_type.id_work = shedule_shift_work.id "
                + " ORDER BY name";

            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = sql;
            sqlCmd.Connection = sqlConn;

            sqlConn.Open();
            dataAdapter.SelectCommand = sqlCmd;
            dataAdapter.Fill(ds);
            dataGridViewShift.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            UpdateShiftGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form_sys_shift_type_edit shift_type_edit = new Form_sys_shift_type_edit("");
            shift_type_edit.MdiParent = this.MdiParent;
            shift_type_edit.FormClosing += (sender1, e1) =>
            {
                UpdateShiftGrid();
            };
            shift_type_edit.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form_sys_shift_type_edit shift_type_edit = new Form_sys_shift_type_edit(dataGridViewShift[0,dataGridViewShift.CurrentCell.RowIndex].Value.ToString());
            shift_type_edit.MdiParent = this.MdiParent;
            shift_type_edit.FormClosing += (sender1, e1) =>
            {
                UpdateShiftGrid();
            };
            shift_type_edit.Show();
        }

        private void dataGridViewShift_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Form_sys_shift_type_edit shift_type_edit = new Form_sys_shift_type_edit(dataGridViewShift[0, dataGridViewShift.CurrentCell.RowIndex].Value.ToString());
            shift_type_edit.MdiParent = this.MdiParent;
            shift_type_edit.FormClosing += (sender1, e1) =>
            {
                UpdateShiftGrid();
            };
            shift_type_edit.Show();
        }
    }
}
