﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Journal;

namespace SystemCore
{
    public partial class Form_sys_login_edit : Form
    {
        private string id, id_acc;
        Form_emp empSelect;
        Form_dep depSelect;
        Form_acc accSelect;

        private SortedList<int, string> depList = new SortedList<int, string>(); 

        public Form_sys_login_edit(string id)
        {
            InitializeComponent();
            this.id = id;
        }

        private void buttonEmp_Click(object sender, EventArgs e)
        {
            if (empSelect == null || empSelect.IsDisposed)
            {
                empSelect = new Form_emp("");
                empSelect.MdiParent = this.MdiParent;
                empSelect.FormClosing += (sender1, e1) =>
                {
                    if (empSelect.id != null)
                    {
                        textBoxEmp.Text = empSelect.name;
                        textBoxEmpId.Text = empSelect.id;
                    }
                };
                empSelect.Show();
            }
            else
            {
                empSelect.Activate();
            }
        }

        private void buttonCencel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonDep_Click(object sender, EventArgs e)
        {
            if (depSelect == null || depSelect.IsDisposed)
            {
                depSelect = new Form_dep(0);
                depSelect.MdiParent = this.MdiParent;
                depSelect.FormClosing += (sender1, e1) =>
                {
                    if (depSelect.dep != null)
                    {
                        try
                        {
                            depList.Add(Convert.ToInt32(depSelect.dep), depSelect.dep);
                        }
                        catch (ArgumentException)
                        {
                            MessageBox.Show("Такая запись уже существует.");
                        }

                        listBox1.Items.Clear();
                        for (int i = 0; i < depList.Count; i++)
                        {
                            listBox1.Items.Add(depList.Values[i]);
                        }
                    }
                };
                depSelect.Show();
            }
            else
            {
                depSelect.Activate();
            }
        }

        private void Form_sys_login_edit_Load(object sender, EventArgs e)
        {
            if (id == "")
            {
                buttonPass.Enabled = false;
            }
            else
            {
                buttonEmp.Enabled = false;
                // загрузить данные по пользователю

                SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                string sql = "SELECT dep FROM users_dep WHERE [id_users] = @id_users ";
                SqlCommand sqlCmd = new SqlCommand(sql, sqlConn);

                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Parameters.AddWithValue("@id_users", id);

                sqlConn.Open();

                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        listBox1.Items.Add(Convert.ToString(dr.GetInt32(dr.GetOrdinal("dep"))));
                        depList.Add(dr.GetInt32(dr.GetOrdinal("dep")), Convert.ToString(dr.GetInt32(dr.GetOrdinal("dep"))));
                    }
                }
                
                SqlCommand command = sqlConn.CreateCommand();
                command.CommandText = "SELECT [id_emp],[id_access] FROM [users]"
                    +" WHERE [id] = @id_user";
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@id_user", id);

                // загрузить данные по пользователю уровень доступа и айди пользователя
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        id_acc = Convert.ToString(dr.GetInt32(dr.GetOrdinal("id_access")));
                        textBoxEmpId.Text = Convert.ToString(dr.GetInt32(dr.GetOrdinal("id_emp")));
                    }
                }

                command.CommandText = "SELECT [name_ru] FROM [employees]"
                     + " WHERE [id] =(SELECT [id_emp]  FROM [users] WHERE [id] = @id_emp)";
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@id_emp", id);

                // загрузить данные по пользователю уровень доступа и айди пользователя
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        textBoxEmp.Text = dr.GetString(dr.GetOrdinal("name_ru"));
                    }
                }

                command.CommandText = "SELECT name_acccess FROM [users_access]"
                    + " WHERE [id_access] = @id_acc";
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@id_acc", id_acc);

                // загрузить данные по пользователю уровень доступа и айди пользователя
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        textBoxAcc.Text = dr.GetString(dr.GetOrdinal("name_acccess"));
                    }
                }

                sqlConn.Close();
            }
        }

        private void buttonDepCln_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (id == "" && id_acc != "")
            {
                // добавление нового пользователя
                SqlConnection connection = new SqlConnection(ProSetting.ConnetionString);

                SqlCommand sqlCmd = new SqlCommand("[users_create]", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@input_id_emp", textBoxEmpId.Text); // ели значение писаное из базы то через value
                sqlCmd.Parameters.AddWithValue("@input_id_acc", id_acc); // если значение записано хардкорно то через Item
                sqlCmd.Parameters.AddWithValue("@input_pass", textBoxPass.Text); // если значение записано хардкорно то через Item

                connection.Open();
                SqlDataReader dr = sqlCmd.ExecuteReader();
                while (dr.Read())
                {
                    id = Convert.ToString(dr.GetInt32(dr.GetOrdinal("id_user")));
                }

                connection.Close();

                addDep();
            }
            else
            {
                // редактирования пользователя
                // проверка на наличие какх либо записей
                updateAcc();
                addDep();
            }
            this.Close();
        }

        private void updatePass()
        {
            SqlConnection connection = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand command = connection.CreateCommand();
            command.CommandText = "UPDATE [users] SET [password] = @password "
                + "WHERE [id] = @id_user";
            connection.Open();
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@password", textBoxPass.Text);
            command.Parameters.AddWithValue("@id_user", id);
            command.ExecuteNonQuery();
        }

        private void updateAcc()
        {
            SqlConnection connection = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand command = connection.CreateCommand();
            command.CommandText = "UPDATE [users] SET [id_access] = @id_acc "
                + "WHERE [id] = @id_user";
            connection.Open();
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@id_acc", id_acc);
            command.Parameters.AddWithValue("@id_user", id);
            command.ExecuteNonQuery();
        }

        private void addDep()
        {
            SqlConnection connection = new SqlConnection(ProSetting.ConnetionString);
            try
            {

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "DELETE [users_dep] WHERE [id_users] = @id_user";
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@id_user", id);
                connection.Open();
                command.ExecuteNonQuery();

                command.CommandText = "INSERT INTO [users_dep] ([id_users],[dep]) "
                                            + "VALUES (@id_user, @dep)";

                for (int i = 0; i < listBox1.Items.Count; i++)
                {
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@id_user", id);
                    command.Parameters.AddWithValue("@dep", listBox1.Items[i].ToString());
                    command.ExecuteNonQuery();
                }

            }
            catch (SqlException)
            {
                MessageBox.Show("Ошибка добавления отделов.");
            }
            finally
            {
                connection.Close();
            }
        }

        private void buttonPass_Click(object sender, EventArgs e)
        {
            if (textBoxPass.Text.Length >= 6)
            {
                updatePass();
            }
            else
            {
                MessageBox.Show("Пароль меньше 6 символов.");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (accSelect == null || accSelect.IsDisposed)
            {
                accSelect = new Form_acc();
                accSelect.MdiParent = this.MdiParent;
                accSelect.FormClosing += (sender1, e1) =>
                {
                    if (accSelect.id_acc != null)
                    {
                        id_acc = accSelect.id_acc;
                        textBoxAcc.Text = accSelect.name_acc;
                    }
                };
                accSelect.Show();
            }
            else
            {
                accSelect.Activate();
            }
        }

        private void buttonAddDepTorg_Click(object sender, EventArgs e)
        {
            for (int i = 1; i <= 10; i++)
            {
                try
                {
                    depList.Add(i * 10, Convert.ToString(i * 10));
                }
                catch (ArgumentException)
                {
                    MessageBox.Show("Такая запись уже существует.");
                }
            }

            listBox1.Items.Clear();
            for (int i = 0; i < depList.Count; i++)
            {
                listBox1.Items.Add(depList.Values[i]);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = null;
            string sql = "SELECT [dep] FROM [dep] WHERE dep >100 ORDER BY dep";
            sqlCmd = new SqlCommand(sql, sqlConn);
            
            sqlCmd.CommandType = CommandType.Text;
            sqlConn.Open();
            using (SqlDataReader dr = sqlCmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    try
                    {
                        depList.Add(dr.GetInt32(dr.GetOrdinal("dep")), Convert.ToString(dr.GetInt32(dr.GetOrdinal("dep"))));
                    }
                    catch (ArgumentException)
                    {
                        MessageBox.Show("Такая запись уже существует.");
                    }
                }
            }
            sqlConn.Close();

            listBox1.Items.Clear();
            for (int i = 0; i < depList.Count; i++)
            {
                listBox1.Items.Add(depList.Values[i]);
            }
        }
    }
}

