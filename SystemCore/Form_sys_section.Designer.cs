﻿namespace SystemCore
{
    partial class Form_sys_section
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewSection = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSection)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewSection
            // 
            this.dataGridViewSection.AllowUserToAddRows = false;
            this.dataGridViewSection.AllowUserToDeleteRows = false;
            this.dataGridViewSection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSection.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSection.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.dep,
            this.stat,
            this.name});
            this.dataGridViewSection.Location = new System.Drawing.Point(12, 41);
            this.dataGridViewSection.Name = "dataGridViewSection";
            this.dataGridViewSection.ReadOnly = true;
            this.dataGridViewSection.RowHeadersVisible = false;
            this.dataGridViewSection.Size = new System.Drawing.Size(591, 467);
            this.dataGridViewSection.TabIndex = 0;
            this.dataGridViewSection.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewSection_CellDoubleClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Новая";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // dep
            // 
            this.dep.DataPropertyName = "dep";
            this.dep.HeaderText = "Отдел";
            this.dep.Name = "dep";
            this.dep.ReadOnly = true;
            // 
            // stat
            // 
            this.stat.DataPropertyName = "status";
            this.stat.HeaderText = "Порядок";
            this.stat.Name = "stat";
            this.stat.ReadOnly = true;
            // 
            // name
            // 
            this.name.DataPropertyName = "name";
            this.name.HeaderText = "Название";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Width = 200;
            // 
            // Form_sys_section
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(615, 520);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridViewSection);
            this.Name = "Form_sys_section";
            this.Text = "Form_section";
            this.Load += new System.EventHandler(this.Form_section_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewSection;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dep;
        private System.Windows.Forms.DataGridViewTextBoxColumn stat;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
    }
}