﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;
using Journal;

namespace SystemCore
{
    public partial class Form_sys_shift_type_edit : Form
    {
        Form_dep depSelect;
        Form_dinner dinnerForm;
        Form_work workForm;
        private string dinnerId, workId;
        private string id;

        private SortedList<int, string> depList = new SortedList<int, string>();

        public Form_sys_shift_type_edit(string id)
        {
            InitializeComponent();
            this.id = id;
        }

        private void buttonCnl_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            // dateTimePickerBegin.Value.ToShortTimeString()
            // написать функцию сохранению и храненку для сохранения новой смены.
            if (id == "")
            {
                // добавление нового пользователя
                SqlConnection connection = new SqlConnection(ProSetting.ConnetionString);
                SqlCommand sqlCmd = new SqlCommand("shift_create", connection);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@inpurt_name", textBoxShift.Text); // ели значение писаное из базы то через value
                sqlCmd.Parameters.AddWithValue("@inpurt_comm", textBoxComm.Text); // если значение записано хардкорно то через Item
                sqlCmd.Parameters.AddWithValue("@input_shift_time", Convert.ToInt32(checkBoxShift.Checked)); // если значение записано хардкорно то через Item
                sqlCmd.Parameters.AddWithValue("@input_shift_start", dateTimePickerBegin.Value.ToShortTimeString()); // если значение записано хардкорно то через Item
                sqlCmd.Parameters.AddWithValue("@input_shift_value", dateTimePickerValue.Value.ToShortTimeString()); // если значение записано хардкорно то через Item
                sqlCmd.Parameters.AddWithValue("@input_dinner_id", dinnerId); // если значение записано хардкорно то через Item
                sqlCmd.Parameters.AddWithValue("@input_shift_end", dateTimePickerEnd.Value.ToShortTimeString()); // если значение записано хардкорно то через Item
                sqlCmd.Parameters.AddWithValue("@input_id_work", workId); // если значение записано хардкорно то через Item

                connection.Open();
                try
                {
                    SqlDataReader dr = sqlCmd.ExecuteReader();
                    while (dr.Read())
                    {
                        id = Convert.ToString(dr.GetInt32(dr.GetOrdinal("id_shift")));
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Не все поля были заполнены.");
                }
                connection.Close();

                addDep();
            }
            else
            {
                // редактирования смены, дописать изменение название смены, комментариев и времени
                SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                SqlCommand sqlCmd = new SqlCommand();

                string sql = "UPDATE shedule_shift_type SET name = @inpurt_name, comment = @inpurt_comm, shift_part = @input_shift_part "
                + ", shift_start = @input_shift_start, shift_value = @input_shift_value, dinner_type =@dinner_type, shift_end = @input_shift_end, id_work = @id_work WHERE [id] = @id_shift";
                if (!checkBoxShift.Checked)
                {
                    sql = "UPDATE shedule_shift_type SET name = @inpurt_name, comment = @inpurt_comm, shift_part = @input_shift_part "
                            + ", shift_start = null, shift_value = null, dinner_type = @dinner_type, shift_end = null, id_work = @id_work WHERE [id] = @id_shift";
                }

                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = sql;
                sqlCmd.Connection = sqlConn;

                sqlCmd.Parameters.AddWithValue("@inpurt_name", textBoxShift.Text); // ели значение писаное из базы то через value
                sqlCmd.Parameters.AddWithValue("@inpurt_comm", textBoxComm.Text); // если значение записано хардкорно то через Item
                sqlCmd.Parameters.AddWithValue("@input_shift_part", Convert.ToInt32(checkBoxShift.Checked)); // если значение записано хардкорно то через Item
                if (checkBoxShift.Checked)
                {
                    sqlCmd.Parameters.AddWithValue("@input_shift_start", dateTimePickerBegin.Value.ToShortTimeString()); // если значение записано хардкорно то через Item
                    sqlCmd.Parameters.AddWithValue("@input_shift_value", dateTimePickerValue.Value.ToShortTimeString()); // если значение записано хардкорно то через Item
                    sqlCmd.Parameters.AddWithValue("@input_shift_end", dateTimePickerEnd.Value.ToShortTimeString()); // если значение записано хардкорно то через Item

                }

                sqlCmd.Parameters.AddWithValue("@id_shift", id);
                sqlCmd.Parameters.AddWithValue("@dinner_type", dinnerId);
                sqlCmd.Parameters.AddWithValue("@id_work", workId);

                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
                sqlConn.Close();
                addDep();
            }
            this.Close();
        }

        private void Form_sys_shift_type_edit_Load(object sender, EventArgs e)
        {

            // написать загрузку существующей смены
            if (id != "")
            {
                SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                SqlCommand sqlCmd = new SqlCommand();

                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Connection = sqlConn;
                string sql = "SELECT id, name, shift_start, shift_value, shift_part, comment, dinner_type, shift_end, id_work FROM shedule_shift_type WHERE id = @id_shift";
                sqlCmd.CommandText = sql;
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Parameters.AddWithValue("@id_shift", id);

                sqlConn.Open();

                SqlDataReader dr = sqlCmd.ExecuteReader();
                {
                    while (dr.Read())
                    {
                        // чтение данных из ридера 
                        id = dr.GetSqlInt32(dr.GetOrdinal("id")).ToString();
                        workId = dr.GetSqlInt32(dr.GetOrdinal("id_work")).ToString();
                        textBoxShift.Text = dr.GetString(dr.GetOrdinal("name"));
                        textBoxComm.Text = dr.GetString(dr.GetOrdinal("comment"));
                        checkBoxShift.Checked = Convert.ToBoolean(dr.GetInt32(dr.GetOrdinal("shift_part")));
                        if (checkBoxShift.Checked == true)
                        {
                            //MessageBox.Show( dr.GetTimeSpan(dr.GetOrdinal("shift_start")).ToString());
                            //dateTimePickerBegin.Value = dt;
                            dateTimePickerBegin.Value = new DateTime(1970, 1, 1).Add(dr.GetTimeSpan(dr.GetOrdinal("shift_start")));// DateTime.Now.Add(dr.GetTimeSpan(dr.GetOrdinal("shift_start")));
                            dateTimePickerValue.Value = new DateTime(1970, 1, 1).Add(dr.GetTimeSpan(dr.GetOrdinal("shift_value")));
                            dateTimePickerEnd.Value = new DateTime(1970, 1, 1).Add(dr.GetTimeSpan(dr.GetOrdinal("shift_end")));
                        }
                        dinnerId = dr.GetSqlInt32(dr.GetOrdinal("dinner_type")).ToString();
                    }
                }
                sqlConn.Close();

                sqlCmd.CommandType = CommandType.Text;
                sql = "SELECT dep FROM shedule_shift_type_dep WHERE id_shift = @id_shift ";
                sqlCmd.CommandText = sql;

                sqlConn.Open();

                dr = sqlCmd.ExecuteReader();
                while (dr.Read())
                {
                    depList.Add(dr.GetInt32(dr.GetOrdinal("dep")), Convert.ToString(dr.GetInt32(dr.GetOrdinal("dep"))));
                }
                sqlConn.Close();

                for (int i = 0; i < depList.Count; i++)
                {
                    listBox1.Items.Add(depList.Values[i]);
                }

                sqlCmd.CommandType = CommandType.Text;
                sql = "SELECT[id_type],[dinner_type_name] FROM [shedule_shift_dinner] WHERE id_type = @id_dinner ";
                sqlCmd.Parameters.Clear();
                sqlCmd.Parameters.AddWithValue("@id_dinner", dinnerId);
                sqlCmd.CommandText = sql;

                sqlConn.Open();
                dr = sqlCmd.ExecuteReader();
                while (dr.Read())
                {
                    textBoxDinner.Text = Convert.ToString(dr.GetString(dr.GetOrdinal("dinner_type_name")));
                }
                sqlConn.Close();

                sqlCmd.CommandType = CommandType.Text;
                sql = "SELECT[id],[work] FROM [shedule_shift_work] WHERE [id] = @id_work ";
                sqlCmd.Parameters.Clear();
                sqlCmd.Parameters.AddWithValue("@id_work", workId);
                sqlCmd.CommandText = sql;

                sqlConn.Open();
                dr = sqlCmd.ExecuteReader();
                while (dr.Read())
                {
                    textBoxWork.Text = Convert.ToString(dr.GetString(dr.GetOrdinal("work")));
                }
                sqlConn.Close();

            }
        }

        private void buttonAddDep_Click(object sender, EventArgs e)
        {
            if (depSelect == null || depSelect.IsDisposed)
            {
                depSelect = new Form_dep(0);
                depSelect.MdiParent = this.MdiParent;
                depSelect.FormClosing += (sender1, e1) =>
                {
                    if (depSelect.dep != null)
                    {
                        //listBox1.Items.Add(depSelect.dep);
                        try
                        {
                            depList.Add(Convert.ToInt32(depSelect.dep), depSelect.dep);
                        }
                        catch (ArgumentException)
                        {
                            MessageBox.Show("Такая запись уже существует.");
                        }

                        listBox1.Items.Clear();
                        for (int i = 0; i < depList.Count; i++)
                        {
                            listBox1.Items.Add(depList.Values[i]);
                        }
                    }
                };
                depSelect.Show();
            }
            else
            {
                depSelect.Activate();
            }
        }

        private void buttonDelDep_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxShift.Checked == true)
            {
                dateTimePickerBegin.Enabled = true;
                dateTimePickerValue.Enabled = true;
                dateTimePickerEnd.Enabled = true;
            }
            else
            {
                dateTimePickerBegin.Enabled = false;
                dateTimePickerValue.Enabled = false;
                dateTimePickerEnd.Enabled = false;
            }
        }

        private void buttonAddDepTorg_Click(object sender, EventArgs e)
        {
            for (int i = 1; i <= 10; i++)
            {
                try
                {
                    depList.Add(i * 10, Convert.ToString(i * 10));
                }
                catch (ArgumentException)
                {
                    MessageBox.Show("Такая запись уже существует.");
                }
            }

            listBox1.Items.Clear();
            for (int i = 0; i < depList.Count; i++)
            {
                listBox1.Items.Add(depList.Values[i]);
            }
        }

        private void addDep()
        {
            SqlConnection connection = new SqlConnection(ProSetting.ConnetionString);
            try
            {

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "DELETE [shedule_shift_type_dep] WHERE [id_shift] = @id_shift";
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@id_shift", id);
                connection.Open();
                command.ExecuteNonQuery();

                command.CommandText = "INSERT INTO [shedule_shift_type_dep] ([id_shift],[dep]) "
                                            + "VALUES (@id_shift, @dep)";

                for (int i = 0; i < listBox1.Items.Count; i++)
                {
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@id_shift", id);
                    command.Parameters.AddWithValue("@dep", listBox1.Items[i].ToString());
                    command.ExecuteNonQuery();
                }

            }
            catch (SqlException)
            {
                MessageBox.Show("Ошибка добавления отделов.");
            }
            finally
            {
                connection.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dinnerForm == null || dinnerForm.IsDisposed)
            {
                dinnerForm = new Form_dinner();
                dinnerForm.MdiParent = this.MdiParent;
                dinnerForm.FormClosing += (sender1, e1) =>
                {
                    if (dinnerForm.dinnerId != null)
                    {
                        dinnerId = dinnerForm.dinnerId;
                        textBoxDinner.Text = dinnerForm.dinnerName;
                    }
                };
                dinnerForm.Show();
            }
            else
            {
                dinnerForm.Activate();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = null;
            string sql = "SELECT [dep] FROM [dep] WHERE dep >100 ORDER BY dep";
            sqlCmd = new SqlCommand(sql, sqlConn);

            sqlCmd.CommandType = CommandType.Text;
            sqlConn.Open();
            using (SqlDataReader dr = sqlCmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    try
                    {
                        depList.Add(dr.GetInt32(dr.GetOrdinal("dep")), Convert.ToString(dr.GetInt32(dr.GetOrdinal("dep"))));
                    }
                    catch (ArgumentException)
                    {
                        MessageBox.Show("Такая запись уже существует.");
                    }
                }
            }
            sqlConn.Close();

            listBox1.Items.Clear();
            for (int i = 0; i < depList.Count; i++)
            {
                listBox1.Items.Add(depList.Values[i]);
            }
        }

        private void buttonWork_Click(object sender, EventArgs e)
        {
            if (dinnerForm == null || dinnerForm.IsDisposed)
            {
                workForm = new Form_work();
                workForm.MdiParent = this.MdiParent;
                workForm.FormClosing += (sender1, e1) =>
                {
                    if (workForm.id != "")
                    {
                        workId = workForm.id;
                        textBoxWork.Text = workForm.name;
                    }
                };
                workForm.Show();
            }
        }
    }
}
