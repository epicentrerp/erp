﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace SystemCore
{
    public partial class Form_sys_doc_blocked : Form
    {
        public Form_sys_doc_blocked()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();

            string sql = "DELETE FROM [docs_bloked] WHERE [id_doc]= @id_doc and [id_user] = @id_user";

            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = sql;
            sqlCmd.Connection = sqlConn;
            
            sqlCmd.Parameters.AddWithValue("@id_doc", dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            sqlCmd.Parameters.AddWithValue("@id_user", dataGridView1[1, dataGridView1.CurrentCell.RowIndex].Value.ToString());

            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();

            UpdateGrid();
        }

        private void Form_sys_doc_blocked_Load(object sender, EventArgs e)
        {
            UpdateGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UpdateGrid();
        }


        private void UpdateGrid()
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();

            string sql = "SELECT [id_doc],[id_user],[time],[name_ru],[comp],[doc_numder_full]"
                + " FROM [docs_blocked_view]";

            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = sql;
            sqlCmd.Connection = sqlConn;

            sqlConn.Open();
            dataAdapter.SelectCommand = sqlCmd;
            dataAdapter.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }
    }
}
