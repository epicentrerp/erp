﻿namespace SystemCore
{
    partial class Form_sys_login_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonEmp = new System.Windows.Forms.Button();
            this.textBoxEmp = new System.Windows.Forms.TextBox();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCencel = new System.Windows.Forms.Button();
            this.buttonDep = new System.Windows.Forms.Button();
            this.buttonDepCln = new System.Windows.Forms.Button();
            this.textBoxEmpId = new System.Windows.Forms.TextBox();
            this.textBoxPass = new System.Windows.Forms.TextBox();
            this.buttonPass = new System.Windows.Forms.Button();
            this.textBoxAcc = new System.Windows.Forms.TextBox();
            this.buttonAcc = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonAddDepTorg = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonEmp
            // 
            this.buttonEmp.Location = new System.Drawing.Point(419, 33);
            this.buttonEmp.Name = "buttonEmp";
            this.buttonEmp.Size = new System.Drawing.Size(30, 23);
            this.buttonEmp.TabIndex = 0;
            this.buttonEmp.Text = ".";
            this.buttonEmp.UseVisualStyleBackColor = true;
            this.buttonEmp.Click += new System.EventHandler(this.buttonEmp_Click);
            // 
            // textBoxEmp
            // 
            this.textBoxEmp.Enabled = false;
            this.textBoxEmp.Location = new System.Drawing.Point(71, 35);
            this.textBoxEmp.Name = "textBoxEmp";
            this.textBoxEmp.Size = new System.Drawing.Size(342, 20);
            this.textBoxEmp.TabIndex = 2;
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(293, 241);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 3;
            this.buttonOk.Text = "Ок";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCencel
            // 
            this.buttonCencel.Location = new System.Drawing.Point(374, 241);
            this.buttonCencel.Name = "buttonCencel";
            this.buttonCencel.Size = new System.Drawing.Size(75, 23);
            this.buttonCencel.TabIndex = 4;
            this.buttonCencel.Text = "Отмена";
            this.buttonCencel.UseVisualStyleBackColor = true;
            this.buttonCencel.Click += new System.EventHandler(this.buttonCencel_Click);
            // 
            // buttonDep
            // 
            this.buttonDep.Location = new System.Drawing.Point(198, 78);
            this.buttonDep.Name = "buttonDep";
            this.buttonDep.Size = new System.Drawing.Size(75, 23);
            this.buttonDep.TabIndex = 6;
            this.buttonDep.Text = "Доб Отд";
            this.buttonDep.UseVisualStyleBackColor = true;
            this.buttonDep.Click += new System.EventHandler(this.buttonDep_Click);
            // 
            // buttonDepCln
            // 
            this.buttonDepCln.Location = new System.Drawing.Point(198, 165);
            this.buttonDepCln.Name = "buttonDepCln";
            this.buttonDepCln.Size = new System.Drawing.Size(75, 23);
            this.buttonDepCln.TabIndex = 7;
            this.buttonDepCln.Text = "Удалить";
            this.buttonDepCln.UseVisualStyleBackColor = true;
            this.buttonDepCln.Click += new System.EventHandler(this.buttonDepCln_Click);
            // 
            // textBoxEmpId
            // 
            this.textBoxEmpId.Enabled = false;
            this.textBoxEmpId.Location = new System.Drawing.Point(12, 35);
            this.textBoxEmpId.Name = "textBoxEmpId";
            this.textBoxEmpId.Size = new System.Drawing.Size(53, 20);
            this.textBoxEmpId.TabIndex = 8;
            // 
            // textBoxPass
            // 
            this.textBoxPass.Location = new System.Drawing.Point(313, 139);
            this.textBoxPass.Name = "textBoxPass";
            this.textBoxPass.Size = new System.Drawing.Size(100, 20);
            this.textBoxPass.TabIndex = 9;
            // 
            // buttonPass
            // 
            this.buttonPass.Location = new System.Drawing.Point(313, 165);
            this.buttonPass.Name = "buttonPass";
            this.buttonPass.Size = new System.Drawing.Size(75, 23);
            this.buttonPass.TabIndex = 10;
            this.buttonPass.Text = "Сменить";
            this.buttonPass.UseVisualStyleBackColor = true;
            this.buttonPass.Click += new System.EventHandler(this.buttonPass_Click);
            // 
            // textBoxAcc
            // 
            this.textBoxAcc.Enabled = false;
            this.textBoxAcc.Location = new System.Drawing.Point(313, 78);
            this.textBoxAcc.Name = "textBoxAcc";
            this.textBoxAcc.Size = new System.Drawing.Size(100, 20);
            this.textBoxAcc.TabIndex = 11;
            // 
            // buttonAcc
            // 
            this.buttonAcc.Location = new System.Drawing.Point(419, 76);
            this.buttonAcc.Name = "buttonAcc";
            this.buttonAcc.Size = new System.Drawing.Size(30, 23);
            this.buttonAcc.TabIndex = 12;
            this.buttonAcc.Text = ".";
            this.buttonAcc.UseVisualStyleBackColor = true;
            this.buttonAcc.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 78);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(180, 186);
            this.listBox1.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "ИД и Фио сотрудника";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Отделы";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(310, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Пароль";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(310, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Доступ";
            // 
            // buttonAddDepTorg
            // 
            this.buttonAddDepTorg.Location = new System.Drawing.Point(198, 107);
            this.buttonAddDepTorg.Name = "buttonAddDepTorg";
            this.buttonAddDepTorg.Size = new System.Drawing.Size(75, 23);
            this.buttonAddDepTorg.TabIndex = 18;
            this.buttonAddDepTorg.Text = "Доб торг";
            this.buttonAddDepTorg.UseVisualStyleBackColor = true;
            this.buttonAddDepTorg.Click += new System.EventHandler(this.buttonAddDepTorg_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(198, 136);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 19;
            this.button1.Text = "Доб не тор";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Form_sys_login_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 275);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonAddDepTorg);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.buttonAcc);
            this.Controls.Add(this.textBoxAcc);
            this.Controls.Add(this.buttonPass);
            this.Controls.Add(this.textBoxPass);
            this.Controls.Add(this.textBoxEmpId);
            this.Controls.Add(this.buttonDepCln);
            this.Controls.Add(this.buttonDep);
            this.Controls.Add(this.buttonCencel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.textBoxEmp);
            this.Controls.Add(this.buttonEmp);
            this.Name = "Form_sys_login_edit";
            this.Text = "Сотрудник";
            this.Load += new System.EventHandler(this.Form_sys_login_edit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonEmp;
        private System.Windows.Forms.TextBox textBoxEmp;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCencel;
        private System.Windows.Forms.Button buttonDep;
        private System.Windows.Forms.Button buttonDepCln;
        private System.Windows.Forms.TextBox textBoxEmpId;
        private System.Windows.Forms.TextBox textBoxPass;
        private System.Windows.Forms.Button buttonPass;
        private System.Windows.Forms.TextBox textBoxAcc;
        private System.Windows.Forms.Button buttonAcc;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonAddDepTorg;
        private System.Windows.Forms.Button button1;
    }
}