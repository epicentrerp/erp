﻿namespace SystemCore
{
    partial class Form_sys_shift_type
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewShift = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dinner_type_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.work = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shift_start = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shift_value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shift_end = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShift)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewShift
            // 
            this.dataGridViewShift.AllowUserToAddRows = false;
            this.dataGridViewShift.AllowUserToDeleteRows = false;
            this.dataGridViewShift.AllowUserToResizeRows = false;
            this.dataGridViewShift.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewShift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewShift.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.name,
            this.comment,
            this.dinner_type_name,
            this.work,
            this.shift_start,
            this.shift_value,
            this.shift_end});
            this.dataGridViewShift.Location = new System.Drawing.Point(12, 52);
            this.dataGridViewShift.Name = "dataGridViewShift";
            this.dataGridViewShift.ReadOnly = true;
            this.dataGridViewShift.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewShift.RowHeadersVisible = false;
            this.dataGridViewShift.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewShift.Size = new System.Drawing.Size(928, 400);
            this.dataGridViewShift.TabIndex = 0;
            this.dataGridViewShift.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewShift_CellDoubleClick);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Width = 50;
            // 
            // name
            // 
            this.name.DataPropertyName = "name";
            this.name.HeaderText = "Название";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            // 
            // comment
            // 
            this.comment.DataPropertyName = "comment";
            this.comment.HeaderText = "Описание";
            this.comment.Name = "comment";
            this.comment.ReadOnly = true;
            this.comment.Width = 200;
            // 
            // dinner_type_name
            // 
            this.dinner_type_name.DataPropertyName = "dinner_type_name";
            this.dinner_type_name.HeaderText = "Питание";
            this.dinner_type_name.Name = "dinner_type_name";
            this.dinner_type_name.ReadOnly = true;
            // 
            // work
            // 
            this.work.DataPropertyName = "work";
            this.work.HeaderText = "Тип";
            this.work.Name = "work";
            this.work.ReadOnly = true;
            // 
            // shift_start
            // 
            this.shift_start.DataPropertyName = "shift_start";
            this.shift_start.HeaderText = "Старт";
            this.shift_start.Name = "shift_start";
            this.shift_start.ReadOnly = true;
            // 
            // shift_value
            // 
            this.shift_value.DataPropertyName = "shift_value";
            this.shift_value.HeaderText = "Длительность";
            this.shift_value.Name = "shift_value";
            this.shift_value.ReadOnly = true;
            // 
            // shift_end
            // 
            this.shift_end.DataPropertyName = "shift_end";
            this.shift_end.HeaderText = "Окончание";
            this.shift_end.Name = "shift_end";
            this.shift_end.ReadOnly = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Новый ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(93, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Редактировать";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 15000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form_sys_shift_type
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 464);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridViewShift);
            this.Name = "Form_sys_shift_type";
            this.Text = "Form_shift_type";
            this.Load += new System.EventHandler(this.Form_shift_type_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShift)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewShift;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn comment;
        private System.Windows.Forms.DataGridViewTextBoxColumn dinner_type_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn work;
        private System.Windows.Forms.DataGridViewTextBoxColumn shift_start;
        private System.Windows.Forms.DataGridViewTextBoxColumn shift_value;
        private System.Windows.Forms.DataGridViewTextBoxColumn shift_end;
    }
}