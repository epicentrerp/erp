﻿namespace SystemCore
{
    partial class Form_sys_dep
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonNew = new System.Windows.Forms.Button();
            this.dataGridViewDep = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dep_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDep)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonNew
            // 
            this.buttonNew.Location = new System.Drawing.Point(12, 12);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(75, 23);
            this.buttonNew.TabIndex = 0;
            this.buttonNew.Text = "Новый";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // dataGridViewDep
            // 
            this.dataGridViewDep.AllowUserToAddRows = false;
            this.dataGridViewDep.AllowUserToDeleteRows = false;
            this.dataGridViewDep.AllowUserToResizeRows = false;
            this.dataGridViewDep.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewDep.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDep.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.dep,
            this.dep_name});
            this.dataGridViewDep.Location = new System.Drawing.Point(12, 41);
            this.dataGridViewDep.Name = "dataGridViewDep";
            this.dataGridViewDep.ReadOnly = true;
            this.dataGridViewDep.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewDep.RowHeadersVisible = false;
            this.dataGridViewDep.Size = new System.Drawing.Size(863, 470);
            this.dataGridViewDep.TabIndex = 1;
            this.dataGridViewDep.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDep_CellDoubleClick);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // dep
            // 
            this.dep.DataPropertyName = "dep";
            this.dep.HeaderText = "Отдел";
            this.dep.Name = "dep";
            this.dep.ReadOnly = true;
            // 
            // dep_name
            // 
            this.dep_name.DataPropertyName = "dep_name";
            this.dep_name.HeaderText = "Название";
            this.dep_name.Name = "dep_name";
            this.dep_name.ReadOnly = true;
            this.dep_name.Width = 300;
            // 
            // Form_sys_dep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 523);
            this.Controls.Add(this.dataGridViewDep);
            this.Controls.Add(this.buttonNew);
            this.Name = "Form_sys_dep";
            this.Text = "Form_sys_dep";
            this.Load += new System.EventHandler(this.Form_sys_dep_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDep)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.DataGridView dataGridViewDep;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn dep;
        private System.Windows.Forms.DataGridViewTextBoxColumn dep_name;
    }
}