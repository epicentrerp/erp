﻿namespace Shedule
{
    partial class Form_dinner_order_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.labelFirst = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonDelEmpF = new System.Windows.Forms.Button();
            this.buttonAddEmpF = new System.Windows.Forms.Button();
            this.dataGridViewFirst = new System.Windows.Forms.DataGridView();
            this.idF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.depF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelSecond = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonDelEmpS = new System.Windows.Forms.Button();
            this.buttonAddEmpS = new System.Windows.Forms.Button();
            this.dataGridViewSecond = new System.Windows.Forms.DataGridView();
            this.idS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.depS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name_ruS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxDoc = new System.Windows.Forms.TextBox();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSecond)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 66);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.labelFirst);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.buttonDelEmpF);
            this.splitContainer1.Panel1.Controls.Add(this.buttonAddEmpF);
            this.splitContainer1.Panel1.Controls.Add(this.dataGridViewFirst);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.labelSecond);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.buttonDelEmpS);
            this.splitContainer1.Panel2.Controls.Add(this.buttonAddEmpS);
            this.splitContainer1.Panel2.Controls.Add(this.dataGridViewSecond);
            this.splitContainer1.Size = new System.Drawing.Size(653, 263);
            this.splitContainer1.SplitterDistance = 328;
            this.splitContainer1.TabIndex = 0;
            // 
            // labelFirst
            // 
            this.labelFirst.AutoSize = true;
            this.labelFirst.Location = new System.Drawing.Point(150, 11);
            this.labelFirst.Name = "labelFirst";
            this.labelFirst.Size = new System.Drawing.Size(0, 13);
            this.labelFirst.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(103, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Обеды";
            // 
            // buttonDelEmpF
            // 
            this.buttonDelEmpF.Location = new System.Drawing.Point(45, 6);
            this.buttonDelEmpF.Name = "buttonDelEmpF";
            this.buttonDelEmpF.Size = new System.Drawing.Size(36, 23);
            this.buttonDelEmpF.TabIndex = 2;
            this.buttonDelEmpF.Text = "-";
            this.buttonDelEmpF.UseVisualStyleBackColor = true;
            this.buttonDelEmpF.Click += new System.EventHandler(this.buttonDelEmpF_Click);
            // 
            // buttonAddEmpF
            // 
            this.buttonAddEmpF.Location = new System.Drawing.Point(3, 6);
            this.buttonAddEmpF.Name = "buttonAddEmpF";
            this.buttonAddEmpF.Size = new System.Drawing.Size(36, 23);
            this.buttonAddEmpF.TabIndex = 1;
            this.buttonAddEmpF.Text = "+";
            this.buttonAddEmpF.UseVisualStyleBackColor = true;
            this.buttonAddEmpF.Click += new System.EventHandler(this.buttonAddEmpF_Click);
            // 
            // dataGridViewFirst
            // 
            this.dataGridViewFirst.AllowUserToAddRows = false;
            this.dataGridViewFirst.AllowUserToDeleteRows = false;
            this.dataGridViewFirst.AllowUserToResizeRows = false;
            this.dataGridViewFirst.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewFirst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFirst.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idF,
            this.depF,
            this.Column1});
            this.dataGridViewFirst.Location = new System.Drawing.Point(3, 35);
            this.dataGridViewFirst.Name = "dataGridViewFirst";
            this.dataGridViewFirst.ReadOnly = true;
            this.dataGridViewFirst.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewFirst.RowHeadersVisible = false;
            this.dataGridViewFirst.Size = new System.Drawing.Size(322, 225);
            this.dataGridViewFirst.TabIndex = 0;
            // 
            // idF
            // 
            this.idF.DataPropertyName = "id";
            this.idF.HeaderText = "id";
            this.idF.Name = "idF";
            this.idF.ReadOnly = true;
            this.idF.Visible = false;
            // 
            // depF
            // 
            this.depF.DataPropertyName = "dep";
            this.depF.HeaderText = "Отдел";
            this.depF.Name = "depF";
            this.depF.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "name_ru";
            this.Column1.HeaderText = "ФИО";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 200;
            // 
            // labelSecond
            // 
            this.labelSecond.AutoSize = true;
            this.labelSecond.Location = new System.Drawing.Point(151, 11);
            this.labelSecond.Name = "labelSecond";
            this.labelSecond.Size = new System.Drawing.Size(0, 13);
            this.labelSecond.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ужины";
            // 
            // buttonDelEmpS
            // 
            this.buttonDelEmpS.Location = new System.Drawing.Point(45, 6);
            this.buttonDelEmpS.Name = "buttonDelEmpS";
            this.buttonDelEmpS.Size = new System.Drawing.Size(36, 23);
            this.buttonDelEmpS.TabIndex = 4;
            this.buttonDelEmpS.Text = "-";
            this.buttonDelEmpS.UseVisualStyleBackColor = true;
            this.buttonDelEmpS.Click += new System.EventHandler(this.buttonDelEmpS_Click);
            // 
            // buttonAddEmpS
            // 
            this.buttonAddEmpS.Location = new System.Drawing.Point(3, 6);
            this.buttonAddEmpS.Name = "buttonAddEmpS";
            this.buttonAddEmpS.Size = new System.Drawing.Size(36, 23);
            this.buttonAddEmpS.TabIndex = 3;
            this.buttonAddEmpS.Text = "+";
            this.buttonAddEmpS.UseVisualStyleBackColor = true;
            this.buttonAddEmpS.Click += new System.EventHandler(this.buttonAddEmpS_Click);
            // 
            // dataGridViewSecond
            // 
            this.dataGridViewSecond.AllowUserToAddRows = false;
            this.dataGridViewSecond.AllowUserToDeleteRows = false;
            this.dataGridViewSecond.AllowUserToResizeRows = false;
            this.dataGridViewSecond.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSecond.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSecond.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idS,
            this.depS,
            this.name_ruS});
            this.dataGridViewSecond.Location = new System.Drawing.Point(3, 35);
            this.dataGridViewSecond.Name = "dataGridViewSecond";
            this.dataGridViewSecond.ReadOnly = true;
            this.dataGridViewSecond.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewSecond.RowHeadersVisible = false;
            this.dataGridViewSecond.Size = new System.Drawing.Size(315, 225);
            this.dataGridViewSecond.TabIndex = 1;
            // 
            // idS
            // 
            this.idS.DataPropertyName = "id";
            this.idS.HeaderText = "idS";
            this.idS.Name = "idS";
            this.idS.ReadOnly = true;
            this.idS.Visible = false;
            // 
            // depS
            // 
            this.depS.DataPropertyName = "dep";
            this.depS.HeaderText = "Отдел";
            this.depS.Name = "depS";
            this.depS.ReadOnly = true;
            // 
            // name_ruS
            // 
            this.name_ruS.DataPropertyName = "name_ru";
            this.name_ruS.HeaderText = "ФИО";
            this.name_ruS.Name = "name_ruS";
            this.name_ruS.ReadOnly = true;
            this.name_ruS.Width = 200;
            // 
            // textBoxDoc
            // 
            this.textBoxDoc.Enabled = false;
            this.textBoxDoc.Location = new System.Drawing.Point(15, 12);
            this.textBoxDoc.Name = "textBoxDoc";
            this.textBoxDoc.Size = new System.Drawing.Size(122, 20);
            this.textBoxDoc.TabIndex = 2;
            // 
            // Form_dinner_order_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 341);
            this.Controls.Add(this.textBoxDoc);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form_dinner_order_edit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_dinner_order_edit_FormClosing);
            this.Load += new System.EventHandler(this.Form_dinner_order_edit_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSecond)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dataGridViewFirst;
        private System.Windows.Forms.DataGridView dataGridViewSecond;
        private System.Windows.Forms.TextBox textBoxDoc;
        private System.Windows.Forms.Button buttonDelEmpF;
        private System.Windows.Forms.Button buttonAddEmpF;
        private System.Windows.Forms.Button buttonDelEmpS;
        private System.Windows.Forms.Button buttonAddEmpS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idF;
        private System.Windows.Forms.DataGridViewTextBoxColumn depF;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idS;
        private System.Windows.Forms.DataGridViewTextBoxColumn depS;
        private System.Windows.Forms.DataGridViewTextBoxColumn name_ruS;
        private System.Windows.Forms.Label labelFirst;
        private System.Windows.Forms.Label labelSecond;
    }
}