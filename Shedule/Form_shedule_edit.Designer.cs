﻿namespace Shedule
{
    partial class Form_shedule_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.dataGridView_stat = new System.Windows.Forms.DataGridView();
            this.shift = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.s31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewShift = new System.Windows.Forms.DataGridView();
            this.idShift = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name_shift = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView_shedule = new System.Windows.Forms.DataGridView();
            this.delEmp = new System.Windows.Forms.Button();
            this.addEmp = new System.Windows.Forms.Button();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.buttonTimeSheet = new System.Windows.Forms.Button();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.section = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.work = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.week = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.section_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_stat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShift)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_shedule)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 44);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView_shedule);
            this.splitContainer1.Panel2.Controls.Add(this.delEmp);
            this.splitContainer1.Panel2.Controls.Add(this.addEmp);
            this.splitContainer1.Size = new System.Drawing.Size(1243, 577);
            this.splitContainer1.SplitterDistance = 160;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridView_stat);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dataGridViewShift);
            this.splitContainer2.Size = new System.Drawing.Size(1243, 160);
            this.splitContainer2.SplitterDistance = 997;
            this.splitContainer2.TabIndex = 0;
            // 
            // dataGridView_stat
            // 
            this.dataGridView_stat.AllowUserToAddRows = false;
            this.dataGridView_stat.AllowUserToDeleteRows = false;
            this.dataGridView_stat.AllowUserToResizeRows = false;
            this.dataGridView_stat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_stat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_stat.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.shift,
            this.rec,
            this.s1,
            this.s2,
            this.s3,
            this.s4,
            this.s5,
            this.s6,
            this.s7,
            this.s8,
            this.s9,
            this.s10,
            this.s11,
            this.s12,
            this.s13,
            this.s14,
            this.s15,
            this.s16,
            this.s17,
            this.s18,
            this.s19,
            this.s20,
            this.s21,
            this.s22,
            this.s23,
            this.s24,
            this.s25,
            this.s26,
            this.s27,
            this.s28,
            this.s29,
            this.s30,
            this.s31});
            this.dataGridView_stat.Location = new System.Drawing.Point(12, 3);
            this.dataGridView_stat.Name = "dataGridView_stat";
            this.dataGridView_stat.ReadOnly = true;
            this.dataGridView_stat.RowHeadersVisible = false;
            this.dataGridView_stat.Size = new System.Drawing.Size(982, 154);
            this.dataGridView_stat.TabIndex = 0;
            // 
            // shift
            // 
            this.shift.DataPropertyName = "shift";
            this.shift.HeaderText = "Смена";
            this.shift.Name = "shift";
            this.shift.ReadOnly = true;
            this.shift.Width = 50;
            // 
            // rec
            // 
            this.rec.DataPropertyName = "rec";
            this.rec.HeaderText = "rec";
            this.rec.Name = "rec";
            this.rec.ReadOnly = true;
            this.rec.Visible = false;
            // 
            // s1
            // 
            this.s1.DataPropertyName = "1";
            this.s1.HeaderText = "1";
            this.s1.Name = "s1";
            this.s1.ReadOnly = true;
            this.s1.Width = 30;
            // 
            // s2
            // 
            this.s2.DataPropertyName = "2";
            this.s2.HeaderText = "2";
            this.s2.Name = "s2";
            this.s2.ReadOnly = true;
            this.s2.Width = 30;
            // 
            // s3
            // 
            this.s3.DataPropertyName = "3";
            this.s3.HeaderText = "3";
            this.s3.Name = "s3";
            this.s3.ReadOnly = true;
            this.s3.Width = 30;
            // 
            // s4
            // 
            this.s4.DataPropertyName = "4";
            this.s4.HeaderText = "4";
            this.s4.Name = "s4";
            this.s4.ReadOnly = true;
            this.s4.Width = 30;
            // 
            // s5
            // 
            this.s5.DataPropertyName = "5";
            this.s5.HeaderText = "5";
            this.s5.Name = "s5";
            this.s5.ReadOnly = true;
            this.s5.Width = 30;
            // 
            // s6
            // 
            this.s6.DataPropertyName = "6";
            this.s6.HeaderText = "6";
            this.s6.Name = "s6";
            this.s6.ReadOnly = true;
            this.s6.Width = 30;
            // 
            // s7
            // 
            this.s7.DataPropertyName = "7";
            this.s7.HeaderText = "7";
            this.s7.Name = "s7";
            this.s7.ReadOnly = true;
            this.s7.Width = 30;
            // 
            // s8
            // 
            this.s8.DataPropertyName = "8";
            this.s8.HeaderText = "8";
            this.s8.Name = "s8";
            this.s8.ReadOnly = true;
            this.s8.Width = 30;
            // 
            // s9
            // 
            this.s9.DataPropertyName = "9";
            this.s9.HeaderText = "9";
            this.s9.Name = "s9";
            this.s9.ReadOnly = true;
            this.s9.Width = 30;
            // 
            // s10
            // 
            this.s10.DataPropertyName = "10";
            this.s10.HeaderText = "10";
            this.s10.Name = "s10";
            this.s10.ReadOnly = true;
            this.s10.Width = 30;
            // 
            // s11
            // 
            this.s11.DataPropertyName = "11";
            this.s11.HeaderText = "11";
            this.s11.Name = "s11";
            this.s11.ReadOnly = true;
            this.s11.Width = 30;
            // 
            // s12
            // 
            this.s12.DataPropertyName = "12";
            this.s12.HeaderText = "12";
            this.s12.Name = "s12";
            this.s12.ReadOnly = true;
            this.s12.Width = 30;
            // 
            // s13
            // 
            this.s13.DataPropertyName = "13";
            this.s13.HeaderText = "13";
            this.s13.Name = "s13";
            this.s13.ReadOnly = true;
            this.s13.Width = 30;
            // 
            // s14
            // 
            this.s14.DataPropertyName = "14";
            this.s14.HeaderText = "14";
            this.s14.Name = "s14";
            this.s14.ReadOnly = true;
            this.s14.Width = 30;
            // 
            // s15
            // 
            this.s15.DataPropertyName = "15";
            this.s15.HeaderText = "15";
            this.s15.Name = "s15";
            this.s15.ReadOnly = true;
            this.s15.Width = 30;
            // 
            // s16
            // 
            this.s16.DataPropertyName = "16";
            this.s16.HeaderText = "16";
            this.s16.Name = "s16";
            this.s16.ReadOnly = true;
            this.s16.Width = 30;
            // 
            // s17
            // 
            this.s17.DataPropertyName = "17";
            this.s17.HeaderText = "17";
            this.s17.Name = "s17";
            this.s17.ReadOnly = true;
            this.s17.Width = 30;
            // 
            // s18
            // 
            this.s18.DataPropertyName = "18";
            this.s18.HeaderText = "18";
            this.s18.Name = "s18";
            this.s18.ReadOnly = true;
            this.s18.Width = 30;
            // 
            // s19
            // 
            this.s19.DataPropertyName = "19";
            this.s19.HeaderText = "19";
            this.s19.Name = "s19";
            this.s19.ReadOnly = true;
            this.s19.Width = 30;
            // 
            // s20
            // 
            this.s20.DataPropertyName = "20";
            this.s20.HeaderText = "20";
            this.s20.Name = "s20";
            this.s20.ReadOnly = true;
            this.s20.Width = 30;
            // 
            // s21
            // 
            this.s21.DataPropertyName = "21";
            this.s21.HeaderText = "21";
            this.s21.Name = "s21";
            this.s21.ReadOnly = true;
            this.s21.Width = 30;
            // 
            // s22
            // 
            this.s22.DataPropertyName = "22";
            this.s22.HeaderText = "22";
            this.s22.Name = "s22";
            this.s22.ReadOnly = true;
            this.s22.Width = 30;
            // 
            // s23
            // 
            this.s23.DataPropertyName = "23";
            this.s23.HeaderText = "23";
            this.s23.Name = "s23";
            this.s23.ReadOnly = true;
            this.s23.Width = 30;
            // 
            // s24
            // 
            this.s24.DataPropertyName = "24";
            this.s24.HeaderText = "24";
            this.s24.Name = "s24";
            this.s24.ReadOnly = true;
            this.s24.Width = 30;
            // 
            // s25
            // 
            this.s25.DataPropertyName = "25";
            this.s25.HeaderText = "25";
            this.s25.Name = "s25";
            this.s25.ReadOnly = true;
            this.s25.Width = 30;
            // 
            // s26
            // 
            this.s26.DataPropertyName = "26";
            this.s26.HeaderText = "26";
            this.s26.Name = "s26";
            this.s26.ReadOnly = true;
            this.s26.Width = 30;
            // 
            // s27
            // 
            this.s27.DataPropertyName = "27";
            this.s27.HeaderText = "27";
            this.s27.Name = "s27";
            this.s27.ReadOnly = true;
            this.s27.Width = 30;
            // 
            // s28
            // 
            this.s28.DataPropertyName = "28";
            this.s28.HeaderText = "28";
            this.s28.Name = "s28";
            this.s28.ReadOnly = true;
            this.s28.Width = 30;
            // 
            // s29
            // 
            this.s29.DataPropertyName = "29";
            this.s29.HeaderText = "29";
            this.s29.Name = "s29";
            this.s29.ReadOnly = true;
            this.s29.Width = 30;
            // 
            // s30
            // 
            this.s30.DataPropertyName = "30";
            this.s30.HeaderText = "30";
            this.s30.Name = "s30";
            this.s30.ReadOnly = true;
            this.s30.Width = 30;
            // 
            // s31
            // 
            this.s31.DataPropertyName = "31";
            this.s31.HeaderText = "31";
            this.s31.Name = "s31";
            this.s31.ReadOnly = true;
            this.s31.Width = 30;
            // 
            // dataGridViewShift
            // 
            this.dataGridViewShift.AllowUserToAddRows = false;
            this.dataGridViewShift.AllowUserToDeleteRows = false;
            this.dataGridViewShift.AllowUserToResizeRows = false;
            this.dataGridViewShift.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewShift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewShift.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idShift,
            this.name_shift});
            this.dataGridViewShift.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewShift.Name = "dataGridViewShift";
            this.dataGridViewShift.ReadOnly = true;
            this.dataGridViewShift.RowHeadersVisible = false;
            this.dataGridViewShift.Size = new System.Drawing.Size(227, 154);
            this.dataGridViewShift.TabIndex = 1;
            // 
            // idShift
            // 
            this.idShift.DataPropertyName = "id";
            this.idShift.HeaderText = "Column1";
            this.idShift.Name = "idShift";
            this.idShift.ReadOnly = true;
            this.idShift.Visible = false;
            this.idShift.Width = 150;
            // 
            // name_shift
            // 
            this.name_shift.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.name_shift.DataPropertyName = "name";
            this.name_shift.HeaderText = "Смена";
            this.name_shift.Name = "name_shift";
            this.name_shift.ReadOnly = true;
            // 
            // dataGridView_shedule
            // 
            this.dataGridView_shedule.AllowUserToAddRows = false;
            this.dataGridView_shedule.AllowUserToDeleteRows = false;
            this.dataGridView_shedule.AllowUserToResizeRows = false;
            this.dataGridView_shedule.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_shedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_shedule.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.name,
            this.section,
            this.d1,
            this.d2,
            this.d3,
            this.d4,
            this.d5,
            this.d6,
            this.d7,
            this.d8,
            this.d9,
            this.d10,
            this.d11,
            this.d12,
            this.d13,
            this.d14,
            this.d15,
            this.d16,
            this.d17,
            this.d18,
            this.d19,
            this.d20,
            this.d21,
            this.d22,
            this.d23,
            this.d24,
            this.d25,
            this.d26,
            this.d27,
            this.d28,
            this.d29,
            this.d30,
            this.d31,
            this.work,
            this.week,
            this.chour,
            this.section_id});
            this.dataGridView_shedule.Location = new System.Drawing.Point(12, 32);
            this.dataGridView_shedule.MultiSelect = false;
            this.dataGridView_shedule.Name = "dataGridView_shedule";
            this.dataGridView_shedule.ReadOnly = true;
            this.dataGridView_shedule.RowHeadersVisible = false;
            this.dataGridView_shedule.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView_shedule.Size = new System.Drawing.Size(1219, 369);
            this.dataGridView_shedule.TabIndex = 2;
            this.dataGridView_shedule.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // delEmp
            // 
            this.delEmp.Location = new System.Drawing.Point(47, 3);
            this.delEmp.Name = "delEmp";
            this.delEmp.Size = new System.Drawing.Size(29, 23);
            this.delEmp.TabIndex = 1;
            this.delEmp.Text = "-";
            this.delEmp.UseVisualStyleBackColor = true;
            this.delEmp.Click += new System.EventHandler(this.delEmp_Click);
            // 
            // addEmp
            // 
            this.addEmp.Location = new System.Drawing.Point(12, 3);
            this.addEmp.Name = "addEmp";
            this.addEmp.Size = new System.Drawing.Size(29, 23);
            this.addEmp.TabIndex = 0;
            this.addEmp.Text = "+";
            this.addEmp.UseVisualStyleBackColor = true;
            this.addEmp.Click += new System.EventHandler(this.addEmp_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.Location = new System.Drawing.Point(12, 12);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(75, 23);
            this.buttonPrint.TabIndex = 1;
            this.buttonPrint.Text = "Печать";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // buttonTimeSheet
            // 
            this.buttonTimeSheet.Location = new System.Drawing.Point(93, 12);
            this.buttonTimeSheet.Name = "buttonTimeSheet";
            this.buttonTimeSheet.Size = new System.Drawing.Size(75, 23);
            this.buttonTimeSheet.TabIndex = 2;
            this.buttonTimeSheet.Text = "Табель";
            this.buttonTimeSheet.UseVisualStyleBackColor = true;
            this.buttonTimeSheet.Click += new System.EventHandler(this.buttonTimeSheet_Click);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.Frozen = true;
            this.id.HeaderText = "";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Width = 30;
            // 
            // name
            // 
            this.name.DataPropertyName = "name";
            this.name.Frozen = true;
            this.name.HeaderText = "ФИО";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Width = 150;
            // 
            // section
            // 
            this.section.DataPropertyName = "section";
            this.section.Frozen = true;
            this.section.HeaderText = "Секция";
            this.section.Name = "section";
            this.section.ReadOnly = true;
            this.section.Width = 150;
            // 
            // d1
            // 
            this.d1.DataPropertyName = "1";
            this.d1.HeaderText = "1";
            this.d1.Name = "d1";
            this.d1.ReadOnly = true;
            this.d1.Width = 30;
            // 
            // d2
            // 
            this.d2.DataPropertyName = "2";
            this.d2.HeaderText = "2";
            this.d2.Name = "d2";
            this.d2.ReadOnly = true;
            this.d2.Width = 30;
            // 
            // d3
            // 
            this.d3.DataPropertyName = "3";
            this.d3.HeaderText = "3";
            this.d3.Name = "d3";
            this.d3.ReadOnly = true;
            this.d3.Width = 30;
            // 
            // d4
            // 
            this.d4.DataPropertyName = "4";
            this.d4.HeaderText = "4";
            this.d4.Name = "d4";
            this.d4.ReadOnly = true;
            this.d4.Width = 30;
            // 
            // d5
            // 
            this.d5.DataPropertyName = "5";
            this.d5.HeaderText = "5";
            this.d5.Name = "d5";
            this.d5.ReadOnly = true;
            this.d5.Width = 30;
            // 
            // d6
            // 
            this.d6.DataPropertyName = "6";
            this.d6.HeaderText = "6";
            this.d6.Name = "d6";
            this.d6.ReadOnly = true;
            this.d6.Width = 30;
            // 
            // d7
            // 
            this.d7.DataPropertyName = "7";
            this.d7.HeaderText = "7";
            this.d7.Name = "d7";
            this.d7.ReadOnly = true;
            this.d7.Width = 30;
            // 
            // d8
            // 
            this.d8.DataPropertyName = "8";
            this.d8.HeaderText = "8";
            this.d8.Name = "d8";
            this.d8.ReadOnly = true;
            this.d8.Width = 30;
            // 
            // d9
            // 
            this.d9.DataPropertyName = "9";
            this.d9.HeaderText = "9";
            this.d9.Name = "d9";
            this.d9.ReadOnly = true;
            this.d9.Width = 30;
            // 
            // d10
            // 
            this.d10.DataPropertyName = "10";
            this.d10.HeaderText = "10";
            this.d10.Name = "d10";
            this.d10.ReadOnly = true;
            this.d10.Width = 30;
            // 
            // d11
            // 
            this.d11.DataPropertyName = "11";
            this.d11.HeaderText = "11";
            this.d11.Name = "d11";
            this.d11.ReadOnly = true;
            this.d11.Width = 30;
            // 
            // d12
            // 
            this.d12.DataPropertyName = "12";
            this.d12.HeaderText = "12";
            this.d12.Name = "d12";
            this.d12.ReadOnly = true;
            this.d12.Width = 30;
            // 
            // d13
            // 
            this.d13.DataPropertyName = "13";
            this.d13.HeaderText = "13";
            this.d13.Name = "d13";
            this.d13.ReadOnly = true;
            this.d13.Width = 30;
            // 
            // d14
            // 
            this.d14.DataPropertyName = "14";
            this.d14.HeaderText = "14";
            this.d14.Name = "d14";
            this.d14.ReadOnly = true;
            this.d14.Width = 30;
            // 
            // d15
            // 
            this.d15.DataPropertyName = "15";
            this.d15.HeaderText = "15";
            this.d15.Name = "d15";
            this.d15.ReadOnly = true;
            this.d15.Width = 30;
            // 
            // d16
            // 
            this.d16.DataPropertyName = "16";
            this.d16.HeaderText = "16";
            this.d16.Name = "d16";
            this.d16.ReadOnly = true;
            this.d16.Width = 30;
            // 
            // d17
            // 
            this.d17.DataPropertyName = "17";
            this.d17.HeaderText = "17";
            this.d17.Name = "d17";
            this.d17.ReadOnly = true;
            this.d17.Width = 30;
            // 
            // d18
            // 
            this.d18.DataPropertyName = "18";
            this.d18.HeaderText = "18";
            this.d18.Name = "d18";
            this.d18.ReadOnly = true;
            this.d18.Width = 30;
            // 
            // d19
            // 
            this.d19.DataPropertyName = "19";
            this.d19.HeaderText = "19";
            this.d19.Name = "d19";
            this.d19.ReadOnly = true;
            this.d19.Width = 30;
            // 
            // d20
            // 
            this.d20.DataPropertyName = "20";
            this.d20.HeaderText = "20";
            this.d20.Name = "d20";
            this.d20.ReadOnly = true;
            this.d20.Width = 30;
            // 
            // d21
            // 
            this.d21.DataPropertyName = "21";
            this.d21.HeaderText = "21";
            this.d21.Name = "d21";
            this.d21.ReadOnly = true;
            this.d21.Width = 30;
            // 
            // d22
            // 
            this.d22.DataPropertyName = "22";
            this.d22.HeaderText = "22";
            this.d22.Name = "d22";
            this.d22.ReadOnly = true;
            this.d22.Width = 30;
            // 
            // d23
            // 
            this.d23.DataPropertyName = "23";
            this.d23.HeaderText = "23";
            this.d23.Name = "d23";
            this.d23.ReadOnly = true;
            this.d23.Width = 30;
            // 
            // d24
            // 
            this.d24.DataPropertyName = "24";
            this.d24.HeaderText = "24";
            this.d24.Name = "d24";
            this.d24.ReadOnly = true;
            this.d24.Width = 30;
            // 
            // d25
            // 
            this.d25.DataPropertyName = "25";
            this.d25.HeaderText = "25";
            this.d25.Name = "d25";
            this.d25.ReadOnly = true;
            this.d25.Width = 30;
            // 
            // d26
            // 
            this.d26.DataPropertyName = "26";
            this.d26.HeaderText = "26";
            this.d26.Name = "d26";
            this.d26.ReadOnly = true;
            this.d26.Width = 30;
            // 
            // d27
            // 
            this.d27.DataPropertyName = "27";
            this.d27.HeaderText = "27";
            this.d27.Name = "d27";
            this.d27.ReadOnly = true;
            this.d27.Width = 30;
            // 
            // d28
            // 
            this.d28.DataPropertyName = "28";
            this.d28.HeaderText = "28";
            this.d28.Name = "d28";
            this.d28.ReadOnly = true;
            this.d28.Width = 30;
            // 
            // d29
            // 
            this.d29.DataPropertyName = "29";
            this.d29.HeaderText = "29";
            this.d29.Name = "d29";
            this.d29.ReadOnly = true;
            this.d29.Width = 30;
            // 
            // d30
            // 
            this.d30.DataPropertyName = "30";
            this.d30.HeaderText = "30";
            this.d30.Name = "d30";
            this.d30.ReadOnly = true;
            this.d30.Width = 30;
            // 
            // d31
            // 
            this.d31.DataPropertyName = "31";
            this.d31.HeaderText = "31";
            this.d31.Name = "d31";
            this.d31.ReadOnly = true;
            this.d31.Width = 30;
            // 
            // work
            // 
            this.work.DataPropertyName = "work";
            this.work.HeaderText = "Раб";
            this.work.Name = "work";
            this.work.ReadOnly = true;
            this.work.Width = 30;
            // 
            // week
            // 
            this.week.DataPropertyName = "week";
            this.week.HeaderText = "Вых";
            this.week.Name = "week";
            this.week.ReadOnly = true;
            this.week.Width = 30;
            // 
            // chour
            // 
            this.chour.DataPropertyName = "chour";
            this.chour.HeaderText = "Часы";
            this.chour.Name = "chour";
            this.chour.ReadOnly = true;
            this.chour.Width = 45;
            // 
            // section_id
            // 
            this.section_id.DataPropertyName = "section_id";
            this.section_id.HeaderText = "section_id";
            this.section_id.Name = "section_id";
            this.section_id.ReadOnly = true;
            this.section_id.Visible = false;
            // 
            // Form_shedule_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1243, 621);
            this.Controls.Add(this.buttonTimeSheet);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form_shedule_edit";
            this.Text = "Расписание";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_shedule_edit_FormClosing);
            this.Load += new System.EventHandler(this.Form_shedule_edit_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_stat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShift)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_shedule)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView dataGridView_stat;
        private System.Windows.Forms.DataGridView dataGridViewShift;
        private System.Windows.Forms.DataGridView dataGridView_shedule;
        private System.Windows.Forms.Button delEmp;
        private System.Windows.Forms.Button addEmp;
        private System.Windows.Forms.DataGridViewTextBoxColumn shift;
        private System.Windows.Forms.DataGridViewTextBoxColumn rec;
        private System.Windows.Forms.DataGridViewTextBoxColumn s1;
        private System.Windows.Forms.DataGridViewTextBoxColumn s2;
        private System.Windows.Forms.DataGridViewTextBoxColumn s3;
        private System.Windows.Forms.DataGridViewTextBoxColumn s4;
        private System.Windows.Forms.DataGridViewTextBoxColumn s5;
        private System.Windows.Forms.DataGridViewTextBoxColumn s6;
        private System.Windows.Forms.DataGridViewTextBoxColumn s7;
        private System.Windows.Forms.DataGridViewTextBoxColumn s8;
        private System.Windows.Forms.DataGridViewTextBoxColumn s9;
        private System.Windows.Forms.DataGridViewTextBoxColumn s10;
        private System.Windows.Forms.DataGridViewTextBoxColumn s11;
        private System.Windows.Forms.DataGridViewTextBoxColumn s12;
        private System.Windows.Forms.DataGridViewTextBoxColumn s13;
        private System.Windows.Forms.DataGridViewTextBoxColumn s14;
        private System.Windows.Forms.DataGridViewTextBoxColumn s15;
        private System.Windows.Forms.DataGridViewTextBoxColumn s16;
        private System.Windows.Forms.DataGridViewTextBoxColumn s17;
        private System.Windows.Forms.DataGridViewTextBoxColumn s18;
        private System.Windows.Forms.DataGridViewTextBoxColumn s19;
        private System.Windows.Forms.DataGridViewTextBoxColumn s20;
        private System.Windows.Forms.DataGridViewTextBoxColumn s21;
        private System.Windows.Forms.DataGridViewTextBoxColumn s22;
        private System.Windows.Forms.DataGridViewTextBoxColumn s23;
        private System.Windows.Forms.DataGridViewTextBoxColumn s24;
        private System.Windows.Forms.DataGridViewTextBoxColumn s25;
        private System.Windows.Forms.DataGridViewTextBoxColumn s26;
        private System.Windows.Forms.DataGridViewTextBoxColumn s27;
        private System.Windows.Forms.DataGridViewTextBoxColumn s28;
        private System.Windows.Forms.DataGridViewTextBoxColumn s29;
        private System.Windows.Forms.DataGridViewTextBoxColumn s30;
        private System.Windows.Forms.DataGridViewTextBoxColumn s31;
        private System.Windows.Forms.DataGridViewTextBoxColumn idShift;
        private System.Windows.Forms.DataGridViewTextBoxColumn name_shift;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.Button buttonTimeSheet;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn section;
        private System.Windows.Forms.DataGridViewTextBoxColumn d1;
        private System.Windows.Forms.DataGridViewTextBoxColumn d2;
        private System.Windows.Forms.DataGridViewTextBoxColumn d3;
        private System.Windows.Forms.DataGridViewTextBoxColumn d4;
        private System.Windows.Forms.DataGridViewTextBoxColumn d5;
        private System.Windows.Forms.DataGridViewTextBoxColumn d6;
        private System.Windows.Forms.DataGridViewTextBoxColumn d7;
        private System.Windows.Forms.DataGridViewTextBoxColumn d8;
        private System.Windows.Forms.DataGridViewTextBoxColumn d9;
        private System.Windows.Forms.DataGridViewTextBoxColumn d10;
        private System.Windows.Forms.DataGridViewTextBoxColumn d11;
        private System.Windows.Forms.DataGridViewTextBoxColumn d12;
        private System.Windows.Forms.DataGridViewTextBoxColumn d13;
        private System.Windows.Forms.DataGridViewTextBoxColumn d14;
        private System.Windows.Forms.DataGridViewTextBoxColumn d15;
        private System.Windows.Forms.DataGridViewTextBoxColumn d16;
        private System.Windows.Forms.DataGridViewTextBoxColumn d17;
        private System.Windows.Forms.DataGridViewTextBoxColumn d18;
        private System.Windows.Forms.DataGridViewTextBoxColumn d19;
        private System.Windows.Forms.DataGridViewTextBoxColumn d20;
        private System.Windows.Forms.DataGridViewTextBoxColumn d21;
        private System.Windows.Forms.DataGridViewTextBoxColumn d22;
        private System.Windows.Forms.DataGridViewTextBoxColumn d23;
        private System.Windows.Forms.DataGridViewTextBoxColumn d24;
        private System.Windows.Forms.DataGridViewTextBoxColumn d25;
        private System.Windows.Forms.DataGridViewTextBoxColumn d26;
        private System.Windows.Forms.DataGridViewTextBoxColumn d27;
        private System.Windows.Forms.DataGridViewTextBoxColumn d28;
        private System.Windows.Forms.DataGridViewTextBoxColumn d29;
        private System.Windows.Forms.DataGridViewTextBoxColumn d30;
        private System.Windows.Forms.DataGridViewTextBoxColumn d31;
        private System.Windows.Forms.DataGridViewTextBoxColumn work;
        private System.Windows.Forms.DataGridViewTextBoxColumn week;
        private System.Windows.Forms.DataGridViewTextBoxColumn chour;
        private System.Windows.Forms.DataGridViewTextBoxColumn section_id;
    }
}