﻿namespace Shedule
{
    partial class Form_shedule_chenge
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.согласоватьДирToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.согласоватьДирToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonDep = new System.Windows.Forms.Button();
            this.textBoxDep = new System.Windows.Forms.TextBox();
            this.buttonDepCln = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridViewChengeShift = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.doc_numder_full = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date_create = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time_create = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name_ru = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.perrent_doc_numder_full = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emp_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reasons = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.историяДокументаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewChengeShift)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.согласоватьДирToolStripMenuItem,
            this.согласоватьДирToolStripMenuItem1,
            this.историяДокументаToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(183, 92);
            // 
            // согласоватьДирToolStripMenuItem
            // 
            this.согласоватьДирToolStripMenuItem.Enabled = false;
            this.согласоватьДирToolStripMenuItem.Name = "согласоватьДирToolStripMenuItem";
            this.согласоватьДирToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.согласоватьДирToolStripMenuItem.Text = "Отменить Дир";
            this.согласоватьДирToolStripMenuItem.Click += new System.EventHandler(this.согласоватьДирToolStripMenuItem_Click_1);
            // 
            // согласоватьДирToolStripMenuItem1
            // 
            this.согласоватьДирToolStripMenuItem1.Enabled = false;
            this.согласоватьДирToolStripMenuItem1.Name = "согласоватьДирToolStripMenuItem1";
            this.согласоватьДирToolStripMenuItem1.Size = new System.Drawing.Size(182, 22);
            this.согласоватьДирToolStripMenuItem1.Text = "Согласовать Дир";
            this.согласоватьДирToolStripMenuItem1.Click += new System.EventHandler(this.согласоватьДирToolStripMenuItem1_Click);
            // 
            // buttonDep
            // 
            this.buttonDep.Location = new System.Drawing.Point(175, 10);
            this.buttonDep.Name = "buttonDep";
            this.buttonDep.Size = new System.Drawing.Size(23, 23);
            this.buttonDep.TabIndex = 2;
            this.buttonDep.Text = ".";
            this.buttonDep.UseVisualStyleBackColor = true;
            this.buttonDep.Click += new System.EventHandler(this.buttonDep_Click);
            // 
            // textBoxDep
            // 
            this.textBoxDep.Enabled = false;
            this.textBoxDep.Location = new System.Drawing.Point(69, 12);
            this.textBoxDep.Name = "textBoxDep";
            this.textBoxDep.Size = new System.Drawing.Size(100, 20);
            this.textBoxDep.TabIndex = 3;
            // 
            // buttonDepCln
            // 
            this.buttonDepCln.Location = new System.Drawing.Point(204, 10);
            this.buttonDepCln.Name = "buttonDepCln";
            this.buttonDepCln.Size = new System.Drawing.Size(39, 23);
            this.buttonDepCln.TabIndex = 4;
            this.buttonDepCln.Text = "Все";
            this.buttonDepCln.UseVisualStyleBackColor = true;
            this.buttonDepCln.Click += new System.EventHandler(this.buttonDepCln_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 15000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Отдел";
            // 
            // dataGridViewChengeShift
            // 
            this.dataGridViewChengeShift.AllowUserToAddRows = false;
            this.dataGridViewChengeShift.AllowUserToDeleteRows = false;
            this.dataGridViewChengeShift.AllowUserToResizeRows = false;
            this.dataGridViewChengeShift.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewChengeShift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewChengeShift.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.status,
            this.dep,
            this.doc_numder_full,
            this.date_create,
            this.time_create,
            this.name_ru,
            this.perrent_doc_numder_full,
            this.emp_name,
            this.reasons});
            this.dataGridViewChengeShift.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridViewChengeShift.Location = new System.Drawing.Point(12, 51);
            this.dataGridViewChengeShift.Name = "dataGridViewChengeShift";
            this.dataGridViewChengeShift.ReadOnly = true;
            this.dataGridViewChengeShift.RowHeadersVisible = false;
            this.dataGridViewChengeShift.Size = new System.Drawing.Size(1061, 448);
            this.dataGridViewChengeShift.TabIndex = 7;
            this.dataGridViewChengeShift.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick_1);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // status
            // 
            this.status.DataPropertyName = "status";
            this.status.HeaderText = "Статус";
            this.status.Name = "status";
            this.status.ReadOnly = true;
            this.status.Width = 50;
            // 
            // dep
            // 
            this.dep.DataPropertyName = "dep";
            this.dep.HeaderText = "Отдел";
            this.dep.Name = "dep";
            this.dep.ReadOnly = true;
            // 
            // doc_numder_full
            // 
            this.doc_numder_full.DataPropertyName = "doc_numder_full";
            this.doc_numder_full.HeaderText = "Документ";
            this.doc_numder_full.Name = "doc_numder_full";
            this.doc_numder_full.ReadOnly = true;
            // 
            // date_create
            // 
            this.date_create.DataPropertyName = "date_create";
            this.date_create.HeaderText = "Дата";
            this.date_create.Name = "date_create";
            this.date_create.ReadOnly = true;
            // 
            // time_create
            // 
            this.time_create.DataPropertyName = "time_create";
            this.time_create.HeaderText = "Время";
            this.time_create.Name = "time_create";
            this.time_create.ReadOnly = true;
            // 
            // name_ru
            // 
            this.name_ru.DataPropertyName = "name_ru";
            this.name_ru.HeaderText = "Создал";
            this.name_ru.Name = "name_ru";
            this.name_ru.ReadOnly = true;
            this.name_ru.Width = 200;
            // 
            // perrent_doc_numder_full
            // 
            this.perrent_doc_numder_full.DataPropertyName = "perrent_doc_numder_full";
            this.perrent_doc_numder_full.HeaderText = "Основа";
            this.perrent_doc_numder_full.Name = "perrent_doc_numder_full";
            this.perrent_doc_numder_full.ReadOnly = true;
            // 
            // emp_name
            // 
            this.emp_name.DataPropertyName = "emp_name";
            this.emp_name.HeaderText = "Сотрудник";
            this.emp_name.Name = "emp_name";
            this.emp_name.ReadOnly = true;
            this.emp_name.Width = 200;
            // 
            // reasons
            // 
            this.reasons.DataPropertyName = "reasons";
            this.reasons.HeaderText = "Причина";
            this.reasons.Name = "reasons";
            this.reasons.ReadOnly = true;
            this.reasons.Width = 150;
            // 
            // историяДокументаToolStripMenuItem
            // 
            this.историяДокументаToolStripMenuItem.Name = "историяДокументаToolStripMenuItem";
            this.историяДокументаToolStripMenuItem.Size = new System.Drawing.Size(182, 22);
            this.историяДокументаToolStripMenuItem.Text = "История документа";
            this.историяДокументаToolStripMenuItem.Click += new System.EventHandler(this.историяДокументаToolStripMenuItem_Click);
            // 
            // Form_shedule_chenge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1085, 511);
            this.Controls.Add(this.dataGridViewChengeShift);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonDepCln);
            this.Controls.Add(this.textBoxDep);
            this.Controls.Add(this.buttonDep);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(500, 320);
            this.Name = "Form_shedule_chenge";
            this.Text = "Журнал изменений";
            this.Load += new System.EventHandler(this.Form_chenge_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewChengeShift)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonDep;
        private System.Windows.Forms.TextBox textBoxDep;
        private System.Windows.Forms.Button buttonDepCln;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem согласоватьДирToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem согласоватьДирToolStripMenuItem1;
        private System.Windows.Forms.DataGridViewTextBoxColumn depDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datechengeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timechangeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameruDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn daycountDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn reasonsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentsDataGridViewTextBoxColumn;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewChengeShift;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewCheckBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn dep;
        private System.Windows.Forms.DataGridViewTextBoxColumn doc_numder_full;
        private System.Windows.Forms.DataGridViewTextBoxColumn date_create;
        private System.Windows.Forms.DataGridViewTextBoxColumn time_create;
        private System.Windows.Forms.DataGridViewTextBoxColumn name_ru;
        private System.Windows.Forms.DataGridViewTextBoxColumn perrent_doc_numder_full;
        private System.Windows.Forms.DataGridViewTextBoxColumn emp_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn reasons;
        private System.Windows.Forms.ToolStripMenuItem историяДокументаToolStripMenuItem;
    }
}

