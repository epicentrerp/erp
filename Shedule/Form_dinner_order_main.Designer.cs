﻿namespace Shedule
{
    partial class Form_dinner_order_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridViewOrder = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.doc_numder_full = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date_create = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time_create = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name_ru = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.админПровестиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.админСнятьПроведениToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.печатьПерваяСменаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.печатьУжиныToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonNew = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.историяДокументаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrder)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewOrder
            // 
            this.dataGridViewOrder.AllowUserToAddRows = false;
            this.dataGridViewOrder.AllowUserToDeleteRows = false;
            this.dataGridViewOrder.AllowUserToResizeRows = false;
            this.dataGridViewOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.status,
            this.doc_numder_full,
            this.date_create,
            this.time_create,
            this.name_ru});
            this.dataGridViewOrder.ContextMenuStrip = this.contextMenuStrip1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewOrder.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewOrder.Location = new System.Drawing.Point(12, 48);
            this.dataGridViewOrder.Name = "dataGridViewOrder";
            this.dataGridViewOrder.ReadOnly = true;
            this.dataGridViewOrder.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewOrder.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewOrder.RowHeadersVisible = false;
            this.dataGridViewOrder.Size = new System.Drawing.Size(748, 365);
            this.dataGridViewOrder.TabIndex = 0;
            this.dataGridViewOrder.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewOrder_CellDoubleClick);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // status
            // 
            this.status.DataPropertyName = "status_nach";
            this.status.HeaderText = "Статус";
            this.status.Name = "status";
            this.status.ReadOnly = true;
            // 
            // doc_numder_full
            // 
            this.doc_numder_full.DataPropertyName = "doc_numder_full";
            this.doc_numder_full.HeaderText = "Номер";
            this.doc_numder_full.Name = "doc_numder_full";
            this.doc_numder_full.ReadOnly = true;
            // 
            // date_create
            // 
            this.date_create.DataPropertyName = "date_create";
            this.date_create.HeaderText = "Дата";
            this.date_create.Name = "date_create";
            this.date_create.ReadOnly = true;
            // 
            // time_create
            // 
            this.time_create.DataPropertyName = "time_create";
            this.time_create.HeaderText = "Время";
            this.time_create.Name = "time_create";
            this.time_create.ReadOnly = true;
            // 
            // name_ru
            // 
            this.name_ru.DataPropertyName = "name_ru";
            this.name_ru.HeaderText = "ФИО";
            this.name_ru.Name = "name_ru";
            this.name_ru.ReadOnly = true;
            this.name_ru.Width = 200;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.админПровестиToolStripMenuItem,
            this.админСнятьПроведениToolStripMenuItem,
            this.toolStripSeparator2,
            this.печатьПерваяСменаToolStripMenuItem,
            this.печатьУжиныToolStripMenuItem,
            this.историяДокументаToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(209, 142);
            // 
            // админПровестиToolStripMenuItem
            // 
            this.админПровестиToolStripMenuItem.Name = "админПровестиToolStripMenuItem";
            this.админПровестиToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.админПровестиToolStripMenuItem.Text = "Админ Провести";
            this.админПровестиToolStripMenuItem.Click += new System.EventHandler(this.админПровестиToolStripMenuItem_Click);
            // 
            // админСнятьПроведениToolStripMenuItem
            // 
            this.админСнятьПроведениToolStripMenuItem.Name = "админСнятьПроведениToolStripMenuItem";
            this.админСнятьПроведениToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.админСнятьПроведениToolStripMenuItem.Text = "Админ Снять проведени";
            this.админСнятьПроведениToolStripMenuItem.Click += new System.EventHandler(this.админСнятьПроведениToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(205, 6);
            // 
            // печатьПерваяСменаToolStripMenuItem
            // 
            this.печатьПерваяСменаToolStripMenuItem.Name = "печатьПерваяСменаToolStripMenuItem";
            this.печатьПерваяСменаToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.печатьПерваяСменаToolStripMenuItem.Text = "Печать Обеды";
            this.печатьПерваяСменаToolStripMenuItem.Click += new System.EventHandler(this.печатьПерваяСменаToolStripMenuItem_Click);
            // 
            // печатьУжиныToolStripMenuItem
            // 
            this.печатьУжиныToolStripMenuItem.Name = "печатьУжиныToolStripMenuItem";
            this.печатьУжиныToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.печатьУжиныToolStripMenuItem.Text = "Печать Ужины";
            this.печатьУжиныToolStripMenuItem.Click += new System.EventHandler(this.печатьУжиныToolStripMenuItem_Click);
            // 
            // buttonNew
            // 
            this.buttonNew.Location = new System.Drawing.Point(12, 12);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(75, 23);
            this.buttonNew.TabIndex = 1;
            this.buttonNew.Text = "Новый";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 15000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // историяДокументаToolStripMenuItem
            // 
            this.историяДокументаToolStripMenuItem.Name = "историяДокументаToolStripMenuItem";
            this.историяДокументаToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.историяДокументаToolStripMenuItem.Text = "История документа";
            this.историяДокументаToolStripMenuItem.Click += new System.EventHandler(this.историяДокументаToolStripMenuItem_Click);
            // 
            // Form_dinner_order_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(772, 425);
            this.Controls.Add(this.buttonNew);
            this.Controls.Add(this.dataGridViewOrder);
            this.Name = "Form_dinner_order_main";
            this.Text = "Заказ питания";
            this.Load += new System.EventHandler(this.Form_dinner_order_main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrder)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewOrder;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem админПровестиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem админСнятьПроведениToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem печатьПерваяСменаToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem печатьУжиныToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewCheckBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn doc_numder_full;
        private System.Windows.Forms.DataGridViewTextBoxColumn date_create;
        private System.Windows.Forms.DataGridViewTextBoxColumn time_create;
        private System.Windows.Forms.DataGridViewTextBoxColumn name_ru;
        private System.Windows.Forms.ToolStripMenuItem историяДокументаToolStripMenuItem;
    }
}