﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Report;

namespace Shedule
{
    public partial class Form_dinner_order_main : Form
    {
        public Form_dinner_order_main()
        {
            InitializeComponent();
        }

        private void Form_dinner_order_main_Load(object sender, EventArgs e)
        {
            UpdateDinner();
            if (ProSetting.Access != 0)
            {
                админСнятьПроведениToolStripMenuItem.Visible = false;
            }
        }

        private void UpdateDinner()
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();
            string sql;

            sql = "SELECT [id],[status_nach],[doc_numder_full],[date_create],[time_create],[name_ru] "
             + " FROM [docs_view] "
             + " WHERE [id_type] = 2 "
             + " ORDER BY id";

            sqlCmd.Connection = sqlConn;
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = sql;

            sqlConn.Open();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = sqlCmd;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            dataGridViewOrder.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }

        private int old_x, old_y;

        private void UpdateOrderGrid()
        {
            if (dataGridViewOrder.RowCount != 0)
            {
                old_y = dataGridViewOrder.CurrentCell.RowIndex;
                old_x = dataGridViewOrder.CurrentCell.ColumnIndex;
            }

            UpdateDinner();

            if (dataGridViewOrder.RowCount >= old_y)
            {
                if (old_x == 0)
                {
                    if (dataGridViewOrder.RowCount != 0)
                        dataGridViewOrder.CurrentCell = dataGridViewOrder[1, 0];
                }
                else
                    dataGridViewOrder.CurrentCell = dataGridViewOrder[old_x, old_y];
            }
            else
            {
                if (dataGridViewOrder.RowCount != 0 && old_y != 0)
                {
                    dataGridViewOrder.CurrentCell = dataGridViewOrder[old_x, dataGridViewOrder.RowCount - 1];
                }
            }            
        }

        private void buttonNew_Click(object sender, EventArgs e)
        {
            int id_doc = 0;

            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();

            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "dinner_order_create";
            sqlCmd.Connection = sqlConn;

             sqlCmd.Parameters.AddWithValue("@input_user", ProSetting.User);

            sqlConn.Open();
            SqlDataReader dr = sqlCmd.ExecuteReader();
            while (dr.Read())
            {
                id_doc = dr.GetInt32(dr.GetOrdinal("doc_id"));
            }
            sqlConn.Close();

            if (id_doc == 0)
                MessageBox.Show("Заказ на питание уже создан.");
            else
            {
                Form_dinner_order_edit dinnerOrder = new Form_dinner_order_edit(id_doc);
                dinnerOrder.MdiParent = this.MdiParent;
                dinnerOrder.FormClosing += (sender1, e1) =>
                {
                    UpdateOrderGrid();
                };
                dinnerOrder.Show();
            }
        }

 
        private void editSelectShedule()
        {
            int id_doc = Convert.ToInt32(dataGridViewOrder[0, dataGridViewOrder.CurrentCell.RowIndex].Value.ToString());

            string blocked = "";

            using (var sqlConn = new SqlConnection(ProSetting.ConnetionString))
            {
                var sqlCmd = new SqlCommand("blocked_docs", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@input_doc", id_doc); // ели значение писаное из базы то через value
                sqlCmd.Parameters.AddWithValue("@input_id_user", ProSetting.User); // если значение записано хардкорно то через Item
                sqlCmd.Parameters.AddWithValue("@input_move", 0);

                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        blocked = dr.GetString(dr.GetOrdinal("rez"));
                    }
                }
                sqlConn.Close();
            }

            if (blocked == "0")
            {
                Form_dinner_order_edit dinnerOrder = new Form_dinner_order_edit(id_doc);
                dinnerOrder.MdiParent = this.MdiParent;
                dinnerOrder.Show();
            }
            else
            {
                MessageBox.Show(blocked);
            }
        }

        private void dataGridViewOrder_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editSelectShedule();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            UpdateOrderGrid();
        }

        private void админПровестиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand("docs_chenge_status", sqlConn);

            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@input_id_doc", Convert.ToInt32(dataGridViewOrder[0, dataGridViewOrder.CurrentCell.RowIndex].Value.ToString()));
            sqlCmd.Parameters.AddWithValue("@input_chenge", 0);
            sqlCmd.Parameters.AddWithValue("@input_how_change", 0);
            sqlCmd.Parameters.AddWithValue("@input_id_emp", ProSetting.User); // пока не используется

            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();
            UpdateOrderGrid();
        }

        private void админСнятьПроведениToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand("docs_chenge_status", sqlConn);

            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@input_id_doc", Convert.ToInt32(dataGridViewOrder[0, dataGridViewOrder.CurrentCell.RowIndex].Value.ToString()));
            sqlCmd.Parameters.AddWithValue("@input_chenge", 1);
            sqlCmd.Parameters.AddWithValue("@input_how_change", 0);
            sqlCmd.Parameters.AddWithValue("@input_id_emp", ProSetting.User); // пока не используется

            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();
            UpdateOrderGrid();
        }

        private void печатьПерваяСменаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridViewOrder[1, dataGridViewOrder.CurrentCell.RowIndex].Value.ToString() == "1")
            {
                Form_rep_order_by_ass orderRep = new Form_rep_order_by_ass(0, dataGridViewOrder[0, dataGridViewOrder.CurrentCell.RowIndex].Value.ToString());
                orderRep.MdiParent = this.MdiParent;
                orderRep.Show();
            }
            else
            {
                MessageBox.Show("Зпи не утверждено, печать невозможна");
            }
        }

        private void печатьУжиныToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridViewOrder[1, dataGridViewOrder.CurrentCell.RowIndex].Value.ToString() == "1")
            {
                Form_rep_order_by_ass orderRep = new Form_rep_order_by_ass(1, dataGridViewOrder[0, dataGridViewOrder.CurrentCell.RowIndex].Value.ToString());
                orderRep.MdiParent = this.MdiParent;
                orderRep.Show();
            }
            else
            {
                MessageBox.Show("Зпи не утверждено, печать невозможна");
            }
        }

        private void историяДокументаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_rep_history editShedule = new Form_rep_history(dataGridViewOrder[0, dataGridViewOrder.CurrentCell.RowIndex].Value.ToString());
            editShedule.MdiParent = this.MdiParent;
            editShedule.Show();
        }
    }
}
