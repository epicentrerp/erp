﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Journal;

namespace Shedule
{
    public partial class Form_dinner_order_edit : Form
    {
        int id_doc, statusNach;

        Form_emp AddEmp;

        public Form_dinner_order_edit(int id_doc)
        {
            InitializeComponent();
            this.id_doc = id_doc;
        }

        private void Form_dinner_order_edit_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            using (var sqlConn = new SqlConnection(ProSetting.ConnetionString))
            {
                var sqlCmd = new SqlCommand("SELECT [status_nach], doc_numder_full, shedule_day FROM [docs_view] WHERE [id] = @id_doc ", sqlConn);
                sqlCmd.CommandType = CommandType.Text;

                sqlCmd.Parameters.AddWithValue("@id_doc", id_doc); // 

                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        statusNach = dr.GetInt32(dr.GetOrdinal("status_nach"));
                        this.Text = dr.GetString(dr.GetOrdinal("doc_numder_full"));
                        textBoxDoc.Text = dr.GetInt32(dr.GetOrdinal("shedule_day")).ToString(); 
                    }
                }
            }

            UpdateEmpGrid();
        }

        int old_y, old_x, old_y_s, old_x_s;

        private void UpdateEmpGrid()
        {
             if (dataGridViewFirst.RowCount != 0)
            {
                old_y = dataGridViewFirst.CurrentCell.RowIndex;
                old_x = dataGridViewFirst.CurrentCell.ColumnIndex;
            }

            if (dataGridViewSecond.RowCount != 0)
            {
                old_y_s = dataGridViewSecond.CurrentCell.RowIndex;
                old_x_s = dataGridViewSecond.CurrentCell.ColumnIndex;
            }

            UpdateEmp();
            countDinner();

            if (dataGridViewFirst.RowCount > old_y)
            {
                if (old_x == 0)
                    dataGridViewFirst.CurrentCell = dataGridViewFirst[1, 0];
                else
                    dataGridViewFirst.CurrentCell = dataGridViewFirst[old_x, old_y];
            }
            else
            {
                if (dataGridViewFirst.RowCount != 0 && old_y != 0)
                {
                    dataGridViewFirst.CurrentCell = dataGridViewFirst[old_x, dataGridViewFirst.RowCount - 1];
                }
            }

            if (dataGridViewSecond.RowCount > old_y_s)
            {
                if (old_x_s == 0)
                    dataGridViewSecond.CurrentCell = dataGridViewSecond[1, 0];
                else
                    dataGridViewSecond.CurrentCell = dataGridViewSecond[old_x, old_y_s];
            }
            else
            {
                if (dataGridViewSecond.RowCount != 0 && old_y_s != 0)
                {
                    dataGridViewSecond.CurrentCell = dataGridViewSecond[old_x_s, dataGridViewSecond.RowCount - 1];
                }
            }

        }

        private void UpdateEmp()
        {
            SqlConnection sqlConn1 = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd1 = new SqlCommand();
            SqlDataAdapter dataAdapter1 = new SqlDataAdapter();
            DataSet dataSet = new DataSet();

            string shift = "SELECT [id],[dep],[name_ru] FROM [dinner_order_det_view] "+
                " WHERE dinner_type = 1 and id_doc = @id_doc ORDER BY dep, name_ru";
            DataSet dataSet1 = new DataSet();
            sqlCmd1.Parameters.Clear();
            sqlCmd1.CommandText = shift;
            sqlCmd1.Connection = sqlConn1;
            sqlCmd1.Parameters.AddWithValue("@id_doc", id_doc);

            sqlConn1.Open();
            dataAdapter1.SelectCommand = sqlCmd1;
            dataAdapter1.Fill(dataSet1);
            dataGridViewFirst.DataSource = dataSet1.Tables[0].DefaultView;
            sqlConn1.Close();

             shift = "SELECT [id],[dep],[name_ru] FROM [dinner_order_det_view] " +
                    " WHERE dinner_type = 2 and id_doc = @id_doc ORDER BY dep, name_ru";
            DataSet dataSet2 = new DataSet();
            sqlCmd1.Parameters.Clear();
            sqlCmd1.CommandText = shift;
            sqlCmd1.Parameters.AddWithValue("@id_doc", id_doc);

            sqlConn1.Open();
            dataAdapter1.SelectCommand = sqlCmd1;
            dataAdapter1.Fill(dataSet2);
            dataGridViewSecond.DataSource = dataSet2.Tables[0].DefaultView;
            sqlConn1.Close();
        }

        private void buttonDelEmpF_Click(object sender, EventArgs e)
        {
            if (statusNach == 0)
            {
                SqlConnection sqlConn1 = new SqlConnection(ProSetting.ConnetionString);
                SqlCommand sqlCmd1 = new SqlCommand();

                string shift = "DELETE FROM [balu].[dbo].[dinner_order_det] "
                                + "WHERE [id_doc] = @id_doc and id_emp = @id_emp and dinner_type = 1 ";
                sqlCmd1.Parameters.Clear();

                sqlCmd1.Parameters.AddWithValue("@id_doc", id_doc);
                sqlCmd1.Parameters.AddWithValue("@id_emp", dataGridViewFirst[0, dataGridViewFirst.CurrentCell.RowIndex].Value.ToString());
                sqlCmd1.CommandText = shift;
                sqlCmd1.Connection = sqlConn1;
              
                sqlConn1.Open();
                sqlCmd1.ExecuteNonQuery();
                sqlConn1.Close();

                UpdateEmpGrid();
            }
        }

        private void buttonDelEmpS_Click(object sender, EventArgs e)
        {
            if (statusNach == 0)
            {
                SqlConnection sqlConn1 = new SqlConnection(ProSetting.ConnetionString);
                SqlCommand sqlCmd1 = new SqlCommand();

                string shift = "DELETE FROM [dinner_order_det] "
                                + "WHERE [id_doc] = @id_doc and id_emp = @id_emp and dinner_type = 2 ";

                sqlCmd1.Parameters.Clear();

                sqlCmd1.Parameters.AddWithValue("@id_doc", id_doc);
                sqlCmd1.Parameters.AddWithValue("@id_emp", dataGridViewSecond[0, dataGridViewSecond.CurrentCell.RowIndex].Value.ToString());
                sqlCmd1.CommandText = shift;
                sqlCmd1.Connection = sqlConn1;
                sqlConn1.Open();
                sqlCmd1.ExecuteNonQuery();
                sqlConn1.Close();

                UpdateEmpGrid();
            }
        }

        private void buttonAddEmpS_Click(object sender, EventArgs e)
        {
            if (statusNach == 0)
            {
                if (AddEmp == null || AddEmp.IsDisposed)
                {
                    AddEmp = new Form_emp("");
                    AddEmp.MdiParent = this.MdiParent;
                    AddEmp.FormClosing += (sender1, e1) =>
                    {
                        if (AddEmp.id != null)
                        {
                            AddEmpType(2, Convert.ToInt32(AddEmp.id));
                            UpdateEmpGrid();
                        }
                    };
                    AddEmp.Show();
                }
                else
                {
                    AddEmp.Activate();
                }
            }
        }


        private void AddEmpType(int type, int id_emp)
        {
            SqlConnection sqlConn1 = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd1 = new SqlCommand();

            string shift = "INSERT INTO [dinner_order_det] ([id_doc],[id_emp],[dinner_type]) "
                            + " values ( @id_doc, @id_emp, @id_type)";

            sqlCmd1.Parameters.Clear();

            sqlCmd1.Parameters.AddWithValue("@id_doc", id_doc);
            sqlCmd1.Parameters.AddWithValue("@id_emp", id_emp);
            sqlCmd1.Parameters.AddWithValue("@id_type", type);
            sqlCmd1.CommandText = shift;
            sqlCmd1.Connection = sqlConn1;

            sqlConn1.Open();
                sqlCmd1.ExecuteNonQuery();
            sqlConn1.Close();
        }

        private void buttonAddEmpF_Click(object sender, EventArgs e)
        {
            if (statusNach == 0)
            {
                if (AddEmp == null || AddEmp.IsDisposed)
                {
                    AddEmp = new Form_emp("");
                    AddEmp.MdiParent = this.MdiParent;
                    AddEmp.FormClosing += (sender1, e1) =>
                    {
                        if (AddEmp.id != "")
                        {
                            AddEmpType(1, Convert.ToInt32(AddEmp.id));
                            UpdateEmpGrid();
                        }
                    };
                    AddEmp.Show();
                }
                else
                {
                    AddEmp.Activate();
                }
            }
        }

        private void Form_dinner_order_edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            SqlConnection connection = new SqlConnection(ProSetting.ConnetionString);
            try
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "DELETE FROM  [docs_bloked] WHERE id_doc = @id_doc and id_user = @id_user";
                connection.Open();

                command.Parameters.Clear();
                command.Parameters.AddWithValue("@id_doc", id_doc);
                command.Parameters.AddWithValue("@id_user", ProSetting.User);
                command.ExecuteNonQuery();
            }
            catch (SqlException)
            {

            }
            finally
            {
                connection.Close();
            }
        }

        private void countDinner()
        {
            labelFirst.Text = dataGridViewFirst.RowCount.ToString();
            labelSecond.Text = dataGridViewSecond.RowCount.ToString();
        }
    }
}
