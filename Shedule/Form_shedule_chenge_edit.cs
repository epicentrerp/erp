﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Journal;

namespace Shedule
{
    public partial class Form_shedule_chenge_edit : Form
    {
        private int[] shiftArr = new int[31];
        private Int32  id_shedule, id_doc, id_emp;
        private string id_rez ="";
        Form_reason reasonsSelect;
        Form_emp empSelect;

        public Form_shedule_chenge_edit(int id_shedule, int id_doc)
        {
            InitializeComponent();
            this.id_shedule = id_shedule;
            this.id_doc = id_doc;
        }
        
        private void button5_Click(object sender, EventArgs e)
        {
            reasonSelect();      
        }

        private void Form_chenge_edit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Insert)
            {
                if (textBoxEmp.Text == "")
                {
                    fioSelect();
                }
                else if (textBoxRes.Text == "")
                {
                    reasonSelect();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fioSelect();
        }

        private void fioSelect()
        {
            if (empSelect == null || empSelect.IsDisposed)
            {
                empSelect = new Form_emp(textBoxDep.Text);
                empSelect.MdiParent = this.MdiParent;
                empSelect.FormClosing += (sender1, e1) =>
                    {
                        if (empSelect.id != "")
                        {
                            id_emp = Convert.ToInt32(empSelect.id);
                            textBoxEmp.Text = empSelect.name;
                            //загрузка расписания сотрудника
                            ExistEmp();
                            dataGridViewShiftToCheng.Rows.Clear();
                        }
                    };
                empSelect.Show();
            }
            else
            {
                empSelect.Activate();
            }
        }

        private int existEmp;
        public void ExistEmp()
        {
            
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "SELECT Count ([id_shift]) as id_shift FROM [shedule_shift] WHERE [id_doc] = @id_doc and [id_emp] = @id_emp and [day]= 1";
            sqlCmd.Connection = sqlConn;

            sqlCmd.Parameters.AddWithValue("@id_doc", id_shedule); // ели значение писаное из базы то через value
            sqlCmd.Parameters.AddWithValue("@id_emp", id_emp); // если значение записано хардкорно то через Item
            SqlDataReader dr = null;
            sqlConn.Open();
            try
            {
                 dr = sqlCmd.ExecuteReader();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
            while (dr.Read())
            {
                existEmp = dr.GetInt32(dr.GetOrdinal("id_shift"));
            }
            if (existEmp == 0)
            {
                for (int i = 0; i < 31; i++)
                {
                    shiftArr[i] = Convert.ToInt32(dataGridViewShift[0, 0].Value.ToString());
                }

                string nameShift = dataGridViewShift[1, dataGridViewShift.CurrentCell.RowIndex].Value.ToString();
                dataGridViewShedule.Rows.Add("",
                     nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift);
            }
            else
            {
                LoadShedule();
            }
        }

        // загрузка распсиания заданного сотрудника
        private void LoadShedule()
        {
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            DataSet ds = new DataSet();
            con.ConnectionString = ProSetting.ConnetionString;
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "[shedule_chenge_view]";
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@inpit_id_emp", id_emp);
            cmd.Parameters.AddWithValue("@input_id_shedule", id_shedule);
            dataAdapter.SelectCommand = cmd;

            con.Open();
            try
            {
                
                dataAdapter.Fill(ds);
                dataGridViewShedule.DataSource = ds.Tables[0].DefaultView;
            }
            catch (SqlException)
            {
                MessageBox.Show("Возникла ошибка загрузки распсиания!");
            }
            finally
            {
                con.Close();
            }
        }

        private void reasonSelect()
        {
            if (reasonsSelect == null || reasonsSelect.IsDisposed)
            {
                reasonsSelect = new Form_reason();
                reasonsSelect.MdiParent = this.MdiParent;
                reasonsSelect.FormClosing += (sender1, e1) =>
                    {
                        if (reasonsSelect.name_reason != "")
                        {
                            textBoxRes.Text = reasonsSelect.name_reason;
                            id_rez = reasonsSelect.id_reason;
                        }
                    };
                reasonsSelect.Show();
            }
            else
            {
                reasonsSelect.Activate();
            }
        }

        // поиск допустимо ли редактировать смены в этот день
        private bool Find()
        {
            bool ret = false;

            for (int i = 0; i < dataGridViewShiftToCheng.RowCount; i++)
                if (dataGridViewShiftToCheng[0, i].FormattedValue.ToString().StartsWith(
                    dataGridViewShedule.Columns[dataGridViewShedule.CurrentCell.ColumnIndex].HeaderText.ToString())
                    )
                {
                    ret = true;
                }
            return ret;
        }

        private string oldIdShift()
        {
            string id_shift="";

            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();

            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "SELECT [id_shift] FROM [shedule_shift] WHERE [id_doc] = @id_doc and [id_emp] = @id_emp and [day]= @day";
            sqlCmd.Connection = sqlConn;

            sqlCmd.Parameters.AddWithValue("@id_doc", id_shedule); // ели значение писаное из базы то через value
            sqlCmd.Parameters.AddWithValue("@id_emp", id_emp); // если значение записано хардкорно то через Item
            sqlCmd.Parameters.AddWithValue("@day", dataGridViewShedule.CurrentCell.ColumnIndex);

            sqlConn.Open();
            try
            {
                SqlDataReader dr = sqlCmd.ExecuteReader();
                while (dr.Read())
                {
                    id_shift = dr.GetInt32(dr.GetOrdinal("id_shift")).ToString();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
            sqlConn.Close();

            return id_shift;
        }

        // подсчет количества измененных дней
        private void dayCount(bool chg)
        {
            if (chg == true)
            {
                labelDayCount.Text = Convert.ToString(Convert.ToInt32(labelDayCount.Text) + 1);
            }
            else
            {
                labelDayCount.Text = Convert.ToString(Convert.ToInt32(labelDayCount.Text) - 1);
            }
        }

        private void Form_chenge_edit_Load(object sender, EventArgs e)
        {
            SqlConnection sqlConn1 = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd1 = new SqlCommand();
            SqlDataAdapter dataAdapter1 = new SqlDataAdapter();
            DataSet dataSet = new DataSet();

            int id_for_shift = 0;
            if (id_doc != 0)
                id_for_shift = id_doc;
            else
                id_for_shift = id_shedule;
            //заполнение грида со сменами
            string shift = "SELECT [id], name as short_name, ([shedule_shift_type].[name]+' '+comment+' '+ISNULL(Convert(varchar,[shift_start]),'')+' '+ISNULL(Convert(varchar,[shift_end]),'')) as [name] "
                    + " FROM [shedule_shift_type] inner join [shedule_shift_type_dep]"
                    + " ON [shedule_shift_type].id = [shedule_shift_type_dep].[id_shift] "
                    + " WHERE [shedule_shift_type_dep].dep = (SELECT dep FROM docs WHERE id = @id_doc) ORDER BY name";
            sqlCmd1.Parameters.Clear();
            sqlCmd1.CommandText = shift;
            sqlCmd1.Connection = sqlConn1;
            sqlCmd1.Parameters.AddWithValue("@id_doc", id_for_shift);

            sqlConn1.Open();
            try
            {
                dataAdapter1.SelectCommand = sqlCmd1;
                dataAdapter1.Fill(dataSet);
                dataGridViewShift.DataSource = dataSet.Tables[0].DefaultView;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
            sqlConn1.Close();

            if (id_doc != 0)
            {
                //дописать пункт с расписанием и с данными пользователя
                buttonEmp.Enabled = false;
                buttonDelShift.Enabled = false;
                buttonRes.Enabled = false;
                textBoxComment.Enabled = false;
 
                // заполнение полей формы
                string sql = "SELECT [id],[status],[docs_view].[dep],[doc_numder_full],[date_create],[time_create],[name_ru],perrent_doc_numder_full, emp_name, doc_comment, reasons, perrent_id, id_emp"
                + " FROM [docs_view] "
                + " WHERE [id] = @id_doc";
                sqlCmd1.Parameters.Clear();
                sqlCmd1.CommandType = CommandType.Text;
                sqlCmd1.Parameters.AddWithValue("@id_doc", id_doc);
                sqlCmd1.CommandText = sql;

                sqlConn1.Open();

                try
                {
                    SqlDataReader dr = sqlCmd1.ExecuteReader();
                    while (dr.Read())
                    {
                        textBoxDep.Text = dr.GetInt32(dr.GetOrdinal("dep")).ToString();
                        textBoxEmp.Text = dr.GetString(dr.GetOrdinal("emp_name")).ToString();
                        textBoxComment.Text = dr.GetString(dr.GetOrdinal("doc_comment")).ToString();
                        textBoxShedule.Text = dr.GetString(dr.GetOrdinal("doc_numder_full")).ToString();
                        textBoxRes.Text = dr.GetString(dr.GetOrdinal("reasons")).ToString();
                        id_shedule = Convert.ToInt32(dr.GetInt64(dr.GetOrdinal("perrent_id")));
                        id_emp = dr.GetInt32(dr.GetOrdinal("id_emp"));
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                sqlConn1.Close();

                LoadShedule();

                // смены которые были изменены в распсианиии
                shift = "SELECT [day],[old_shift],[new_shift] FROM [shedule_shift_chenge_det_view] "
                + " WHERE id_shedule = @id_doc ORDER BY day";

                sqlCmd1.Parameters.Clear();
                sqlCmd1.CommandText = shift;
                sqlCmd1.Connection = sqlConn1;
                sqlCmd1.Parameters.AddWithValue("@id_doc", id_doc);
                DataSet dataSet1 = new DataSet();

                sqlConn1.Open();
                try
                {

                    dataAdapter1.SelectCommand = sqlCmd1;
                    dataAdapter1.Fill(dataSet1);
                    dataGridViewShiftToCheng.DataSource = dataSet1.Tables[0].DefaultView;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                sqlConn1.Close();

                labelDayCount.Text = dataGridViewShiftToCheng.RowCount.ToString();
            }
            else
            {
                try
                {
                    string sql = "SELECT [dep], doc_numder_full"
                                 + " FROM [docs_view]"
                                 + " WHERE [id] = @id_doc";
                    sqlCmd1.Parameters.Clear();
                    sqlCmd1.CommandType = CommandType.Text;
                    sqlCmd1.CommandText = sql;
                    sqlCmd1.Parameters.AddWithValue("@id_doc", id_shedule);

                    sqlConn1.Open();
                    try
                    {
                        SqlDataReader dr = sqlCmd1.ExecuteReader();
                        while (dr.Read())
                        {
                            textBoxDep.Text = dr.GetInt32(dr.GetOrdinal("dep")).ToString();
                            textBoxShedule.Text = dr.GetString(dr.GetOrdinal("doc_numder_full"));
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    sqlConn1.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

        }

        private void dataGridViewShedule_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
          if (id_doc == 0)
            {
                if (existEmp == 0)
                {
                    if (dataGridViewShedule.CurrentCell.ColumnIndex > 0)
                    {

                        // дописать дни недели и редактирование нижней строки.
                        shiftArr[dataGridViewShedule.CurrentCell.ColumnIndex - 1] = Convert.ToInt32(dataGridViewShift[0, dataGridViewShift.CurrentCell.RowIndex].Value.ToString());
                        dataGridViewShedule[dataGridViewShedule.CurrentCell.ColumnIndex, 0].Value = dataGridViewShift[1, dataGridViewShift.CurrentCell.RowIndex].Value.ToString();
                    }
                }
                else
                {
                    if (dataGridViewShedule.CurrentCell.RowIndex > 0
                        && Convert.ToInt32(DateTime.Today.Day.ToString()) <= Convert.ToInt32(dataGridViewShedule.Columns[dataGridViewShedule.CurrentCell.ColumnIndex].HeaderText.ToString()))
                    {
                        if (Find() != true)
                        {
                            // переписать для редактирования и массива

                            dataGridViewShiftToCheng.Rows.Add(
                                dataGridViewShedule.Columns[dataGridViewShedule.CurrentCell.ColumnIndex].HeaderText.ToString(), // day
                                dataGridViewShedule[dataGridViewShedule.CurrentCell.ColumnIndex, 1].Value.ToString(), // стара смена
                                //засписать айди старой смены 
                                oldIdShift(),// айди старой смены
                                dataGridViewShift[1, dataGridViewShift.CurrentCell.RowIndex].Value.ToString(),// новая смена
                                dataGridViewShift[0, dataGridViewShift.CurrentCell.RowIndex].Value.ToString()); // айди новой смены
                            dayCount(true);
                        }
                    }
                    else
                    {
                        MessageBox.Show("В этом дне уже сделали изменения. Смотри список изменений.");
                    }
                }
            }
        }

        private void buttonОк_Click(object sender, EventArgs e)
        {
            // сохранение расписание
            if (id_doc == 0)
            {
                // проверка заполноности всех полей
                if ( id_emp != 0 & id_rez != "")
                {
                    

                    if (existEmp == 0)
                    {
                        //запись строки в журнал изменения расписания
                        SqlConnection connection = new SqlConnection(ProSetting.ConnetionString);
                        try
                        {
                            SqlCommand command = connection.CreateCommand();
                            command.CommandText = "INSERT INTO [shedule_shift] ([id_doc],[id_emp],[day],[id_shift])"
                                                    + "VALUES (@id_doc, @id_emp, @day, @id_shift)";
                            connection.Open();
                            for (int i = 0; i < 31; i++)
                            {
                                command.Parameters.Clear();
                                command.Parameters.AddWithValue("@id_doc", id_shedule);
                                command.Parameters.AddWithValue("@id_emp", id_emp);// 
                                command.Parameters.AddWithValue("@day", i + 1);
                                command.Parameters.AddWithValue("@id_shift", shiftArr[i].ToString());
                                command.ExecuteNonQuery();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Не все поля заполнены.");
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                    else
                    {
                        if (dataGridViewShiftToCheng.RowCount != 0)
                        {
                            // если сотрудник существует в расписании
                            string doc_number = "", auto = "";
                            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                            SqlCommand sqlCmd = new SqlCommand();

                            sqlCmd.Connection = sqlConn;
                            sqlCmd.CommandType = CommandType.StoredProcedure;
                            sqlCmd.CommandText = "shedule_chenge_create";


                            sqlCmd.Parameters.AddWithValue("@input_id_emp", id_emp);
                            sqlCmd.Parameters.AddWithValue("@input_day_count", labelDayCount.Text);
                            sqlCmd.Parameters.AddWithValue("@input_id_reason", id_rez);
                            sqlCmd.Parameters.AddWithValue("@input_comment", textBoxComment.Text);
                            sqlCmd.Parameters.AddWithValue("@input_id_user", ProSetting.User);
                            sqlCmd.Parameters.AddWithValue("@input_perrent_id", id_shedule);

                            sqlConn.Open();

                            SqlDataReader dr = sqlCmd.ExecuteReader();
                            while (dr.Read())
                            {
                                doc_number = dr.GetInt32(dr.GetOrdinal("id_doc")).ToString();
                            }
                            sqlConn.Close();
                            // запись изменных смен в базу
                            string sql = "INSERT INTO [shedule_shift_chenge_det] ([id_shedule],[old_shift],[new_shift],[day]) "
                                         + " VALUES (@id_shedule, @old_shift, @new_shift, @day)";
                            sqlCmd.CommandType = CommandType.Text;
                            sqlCmd.CommandText = sql;

                            sqlConn.Open();
                            for (int i = 0; i < dataGridViewShiftToCheng.RowCount; i++)
                            {
                                sqlCmd.Parameters.Clear();
                                sqlCmd.Parameters.AddWithValue("@id_shedule", doc_number);
                                sqlCmd.Parameters.AddWithValue("@old_shift", dataGridViewShiftToCheng[2, i].Value.ToString());
                                sqlCmd.Parameters.AddWithValue("@new_shift", dataGridViewShiftToCheng[4, i].Value.ToString());
                                sqlCmd.Parameters.AddWithValue("@day", dataGridViewShiftToCheng[0, i].Value.ToString());
                                try
                                {
                                    sqlCmd.ExecuteNonQuery();
                                }
                                catch (SqlException ex)
                                {
                                    MessageBox.Show(ex.ToString());
                                }
                            }
                            sqlConn.Close();

                            sqlCmd.CommandType = CommandType.Text;
                            sqlCmd.CommandText = "(SELECT value FROM sysseting WHERE param= 'autochenge')";

                            sqlCmd.Parameters.Clear();

                            sqlConn.Open();
                            try
                            {
                                dr = sqlCmd.ExecuteReader();
                            }
                            catch (SqlException ex)
                            {
                                MessageBox.Show(ex.ToString());
                            }
                            while (dr.Read())
                            {
                                auto = dr.GetString(dr.GetOrdinal("value"));
                            }

                            dr.Close();

                            if (auto == "1")
                            {
                                try
                                {
                                    // подтверждение изменения
                                    sqlCmd.CommandType = CommandType.StoredProcedure;
                                    sqlCmd.CommandText = "[shedule_cheng_submit]";
                                    sqlCmd.Parameters.Clear();
                                    sqlCmd.Parameters.AddWithValue("@input_id_doc", doc_number);
                                    sqlCmd.Parameters.AddWithValue("@input_new_old", 1); // если документ утверждается то 1 если разпроводиться то 0
                                    sqlCmd.Parameters.AddWithValue("@input_user", ProSetting.User); 
                                    sqlCmd.ExecuteNonQuery();
                                }
                                catch (SqlException ex)
                                {
                                    MessageBox.Show(ex.ToString());
                                }
                            }
                            sqlConn.Close();


                        }
                    }
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Не все поля были заполнены!");
                }
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonDelShift_Click(object sender, EventArgs e)
        {
            if (dataGridViewShiftToCheng.RowCount != 0)
            {
                dataGridViewShiftToCheng.Rows.RemoveAt(dataGridViewShiftToCheng.CurrentCell.RowIndex);
                dayCount(false);
            }
        }
    }
}