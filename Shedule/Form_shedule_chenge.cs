﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Journal;
using Report;

namespace Shedule
{
    public partial class Form_shedule_chenge : Form
    {
        public Form_shedule_chenge()
        {
            InitializeComponent();
        }

        DateTime dateBegin, dateEnd;
        Form_dep depSelect;
        //Form_period period;

        private void Form_chenge_Load(object sender, EventArgs e)
        {
            if (ProSetting.Access == 0 || ProSetting.Access == 1)
            {
                согласоватьДирToolStripMenuItem.Enabled = true;
                согласоватьДирToolStripMenuItem1.Enabled = true;
            }

            // получаем дату первого дня и последнего дня месяца
            int yr = DateTime.Today.Year;
            int mth = DateTime.Today.Month;
            dateBegin = new DateTime(yr, mth, 1);
            if (mth != 12 )
                dateEnd = new DateTime(yr, mth + 1, 1).AddDays(-1);
            else
                dateEnd = new DateTime(yr+1, 1, 1).AddDays(-1);
            
            UpdateChenge();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //MessageBox.Show(this.dataGridViewChengeShift[0, this.dataGridViewChengeShift.CurrentCell.RowIndex].Value.ToString());

        }
        
        int old_y, old_x;

        private void UpdateChengeGrid()
        {
            // запоминание данных положения курсора
            
            if (dataGridViewChengeShift.RowCount != 0)
            {
                old_y = dataGridViewChengeShift.CurrentCell.RowIndex;
                old_x = dataGridViewChengeShift.CurrentCell.ColumnIndex;
            }
            // обновление данных грида
            UpdateChenge();

            //восстановление положения курсора
            if (dataGridViewChengeShift.RowCount > old_y && old_y != 0)
            {
                dataGridViewChengeShift.CurrentCell = dataGridViewChengeShift[old_x, old_y];
            }
            else
            {
                if (old_y != 0 && dataGridViewChengeShift.RowCount != 0)
                {
                    dataGridViewChengeShift.CurrentCell = dataGridViewChengeShift[old_x, dataGridViewChengeShift.RowCount - 1];
                }
            }
        }

        private void UpdateChenge()
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            string sql;

            if (textBoxDep.Text != "")
            {
                sql = "SELECT [id],[status],[docs_view].[dep],[doc_numder_full],[date_create],[time_create],[name_ru],perrent_doc_numder_full, emp_name, reasons"
                + " FROM [docs_view] inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
                + " on [docs_view].dep = dep_table.dep"
                + " WHERE [id_type] = 1 and docs_view.dep = @dep "
                + " and date_create >= @Begin and date_create <= @End "
                + " ORDER BY id";
                sqlCmd.Parameters.AddWithValue("@dep", textBoxDep.Text);
            }
            else
            {
                sql = "SELECT [id],[status],[docs_view].[dep],[doc_numder_full],[date_create],[time_create],[name_ru],perrent_doc_numder_full, emp_name,reasons "
                + " FROM [docs_view] inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
                + " on [docs_view].dep = dep_table.dep"
                + " WHERE [id_type] = 1 "
                + " and date_create >= @Begin and date_create <= @End "
                + " ORDER BY id";
            }

            sqlCmd.Connection = sqlConn;
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = sql;
            sqlCmd.Parameters.AddWithValue("@id_user", ProSetting.User);
            sqlCmd.Parameters.AddWithValue("@Begin", dateBegin);
            sqlCmd.Parameters.AddWithValue("@End", dateEnd);


            sqlConn.Open();
            dataAdapter.SelectCommand = sqlCmd;
            dataAdapter.Fill(ds);
            dataGridViewChengeShift.DataSource = ds.Tables[0].DefaultView;
            dataGridViewChengeShift.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }
        
        private void согласоватьДирToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SubmitChenge(1);
            UpdateChengeGrid();
        }

        private void SubmitChenge(int sub)
        {
            int status = 0;
            SqlConnection sqlConn1 = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd1 = new SqlCommand();
            // проверка проведен ли документ диром
            // заполнение полей формы
            string sql = "SELECT [id],[status] "
            + " FROM [docs_view] "
            + " WHERE [id] = @id_doc";
            sqlCmd1.Connection = sqlConn1;
            sqlCmd1.Parameters.Clear();
            sqlCmd1.CommandType = CommandType.Text;
            sqlCmd1.Parameters.AddWithValue("@id_doc", dataGridViewChengeShift[0, dataGridViewChengeShift.CurrentCell.RowIndex].Value.ToString());
            sqlCmd1.CommandText = sql;

            sqlConn1.Open();

            SqlDataReader dr = sqlCmd1.ExecuteReader();
            while (dr.Read())
            {
                status = dr.GetInt32(dr.GetOrdinal("status"));
            }

            if ((status == 0 & sub == 1) | (status == 1 & sub == 0))
            {
                SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                SqlCommand sqlCmd = new SqlCommand();

                sqlCmd.Connection = sqlConn;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandText = "shedule_cheng_submit";
                sqlCmd.Parameters.Clear();
                sqlCmd.Parameters.AddWithValue("@input_id_doc", dataGridViewChengeShift[0, dataGridViewChengeShift.CurrentCell.RowIndex].Value.ToString());
                sqlCmd.Parameters.AddWithValue("@input_new_old", sub); // если документ утверждается то 1 если разпроводиться то 0
                sqlCmd.Parameters.AddWithValue("@input_user", ProSetting.User);
                sqlConn.Open();
                sqlCmd.ExecuteNonQuery();
                sqlConn.Close();
            }
            else
            {
                MessageBox.Show("Нарушен порядок утверждения.");
            }
        }

        private void согласоватьДирToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            SubmitChenge(0);
            UpdateChengeGrid();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            UpdateChengeGrid();
        }

        private void buttonDepCln_Click(object sender, EventArgs e)
        {
            textBoxDep.Clear();
            UpdateChengeGrid();
        }

        private void buttonDep_Click(object sender, EventArgs e)
        {
            if (depSelect == null || depSelect.IsDisposed)
            {
                depSelect = new Form_dep(ProSetting.User);
                depSelect.Owner = this;
                depSelect.FormClosing += (sender1, e1) =>
                {
                    textBoxDep.Text = depSelect.dep;
                    UpdateChengeGrid();
                };
                depSelect.Show();
            }
            else
            {
                depSelect.Activate();
            }
        }

        private void dataGridView1_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            Form_shedule_chenge_edit edit_chenge = new Form_shedule_chenge_edit(0, Convert.ToInt32(dataGridViewChengeShift[0, dataGridViewChengeShift.CurrentCell.RowIndex].Value.ToString()));
            edit_chenge.MdiParent = this.MdiParent;
            edit_chenge.Show();
        }

        private void историяДокументаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_rep_history editShedule = new Form_rep_history(dataGridViewChengeShift[0, dataGridViewChengeShift.CurrentCell.RowIndex].Value.ToString());
            editShedule.MdiParent = this.MdiParent;
            editShedule.Show();
        }
    }
}