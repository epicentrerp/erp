﻿namespace Shedule
{
    partial class Form_shedule_chenge_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewShedule = new System.Windows.Forms.DataGridView();
            this.oreder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewShiftToCheng = new System.Windows.Forms.DataGridView();
            this.day = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.old_shedule = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_old = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.new_shedule = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_new = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonEmp = new System.Windows.Forms.Button();
            this.textBoxEmp = new System.Windows.Forms.TextBox();
            this.buttonDelShift = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.buttonОк = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxRes = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonRes = new System.Windows.Forms.Button();
            this.textBoxComment = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxDep = new System.Windows.Forms.TextBox();
            this.splitContainerShedule = new System.Windows.Forms.SplitContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dataGridViewShift = new System.Windows.Forms.DataGridView();
            this.id_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.short_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxShedule = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.labelDayCount = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShiftToCheng)).BeginInit();
            this.splitContainerShedule.Panel1.SuspendLayout();
            this.splitContainerShedule.Panel2.SuspendLayout();
            this.splitContainerShedule.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShift)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewShedule
            // 
            this.dataGridViewShedule.AllowUserToAddRows = false;
            this.dataGridViewShedule.AllowUserToDeleteRows = false;
            this.dataGridViewShedule.AllowUserToResizeColumns = false;
            this.dataGridViewShedule.AllowUserToResizeRows = false;
            this.dataGridViewShedule.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewShedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewShedule.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.oreder,
            this.d1,
            this.d2,
            this.d3,
            this.d4,
            this.d5,
            this.d6,
            this.d7,
            this.d8,
            this.d9,
            this.d10,
            this.d11,
            this.d12,
            this.d13,
            this.d14,
            this.d15,
            this.d16,
            this.d17,
            this.d18,
            this.d19,
            this.d20,
            this.d21,
            this.d22,
            this.d23,
            this.d24,
            this.d25,
            this.d26,
            this.d27,
            this.d28,
            this.d29,
            this.d30,
            this.d31});
            this.dataGridViewShedule.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewShedule.MultiSelect = false;
            this.dataGridViewShedule.Name = "dataGridViewShedule";
            this.dataGridViewShedule.ReadOnly = true;
            this.dataGridViewShedule.RowHeadersVisible = false;
            this.dataGridViewShedule.Size = new System.Drawing.Size(950, 105);
            this.dataGridViewShedule.TabIndex = 0;
            this.dataGridViewShedule.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewShedule_CellDoubleClick);
            // 
            // oreder
            // 
            this.oreder.DataPropertyName = "order";
            this.oreder.HeaderText = "oreder";
            this.oreder.Name = "oreder";
            this.oreder.ReadOnly = true;
            this.oreder.Visible = false;
            // 
            // d1
            // 
            this.d1.DataPropertyName = "1";
            this.d1.HeaderText = "1";
            this.d1.Name = "d1";
            this.d1.ReadOnly = true;
            this.d1.Width = 30;
            // 
            // d2
            // 
            this.d2.DataPropertyName = "2";
            this.d2.HeaderText = "2";
            this.d2.Name = "d2";
            this.d2.ReadOnly = true;
            this.d2.Width = 30;
            // 
            // d3
            // 
            this.d3.DataPropertyName = "3";
            this.d3.HeaderText = "3";
            this.d3.Name = "d3";
            this.d3.ReadOnly = true;
            this.d3.Width = 30;
            // 
            // d4
            // 
            this.d4.DataPropertyName = "4";
            this.d4.HeaderText = "4";
            this.d4.Name = "d4";
            this.d4.ReadOnly = true;
            this.d4.Width = 30;
            // 
            // d5
            // 
            this.d5.DataPropertyName = "5";
            this.d5.HeaderText = "5";
            this.d5.Name = "d5";
            this.d5.ReadOnly = true;
            this.d5.Width = 30;
            // 
            // d6
            // 
            this.d6.DataPropertyName = "6";
            this.d6.HeaderText = "6";
            this.d6.Name = "d6";
            this.d6.ReadOnly = true;
            this.d6.Width = 30;
            // 
            // d7
            // 
            this.d7.DataPropertyName = "7";
            this.d7.HeaderText = "7";
            this.d7.Name = "d7";
            this.d7.ReadOnly = true;
            this.d7.Width = 30;
            // 
            // d8
            // 
            this.d8.DataPropertyName = "8";
            this.d8.HeaderText = "8";
            this.d8.Name = "d8";
            this.d8.ReadOnly = true;
            this.d8.Width = 30;
            // 
            // d9
            // 
            this.d9.DataPropertyName = "9";
            this.d9.HeaderText = "9";
            this.d9.Name = "d9";
            this.d9.ReadOnly = true;
            this.d9.Width = 30;
            // 
            // d10
            // 
            this.d10.DataPropertyName = "10";
            this.d10.HeaderText = "10";
            this.d10.Name = "d10";
            this.d10.ReadOnly = true;
            this.d10.Width = 30;
            // 
            // d11
            // 
            this.d11.DataPropertyName = "11";
            this.d11.HeaderText = "11";
            this.d11.Name = "d11";
            this.d11.ReadOnly = true;
            this.d11.Width = 30;
            // 
            // d12
            // 
            this.d12.DataPropertyName = "12";
            this.d12.HeaderText = "12";
            this.d12.Name = "d12";
            this.d12.ReadOnly = true;
            this.d12.Width = 30;
            // 
            // d13
            // 
            this.d13.DataPropertyName = "13";
            this.d13.HeaderText = "13";
            this.d13.Name = "d13";
            this.d13.ReadOnly = true;
            this.d13.Width = 30;
            // 
            // d14
            // 
            this.d14.DataPropertyName = "14";
            this.d14.HeaderText = "14";
            this.d14.Name = "d14";
            this.d14.ReadOnly = true;
            this.d14.Width = 30;
            // 
            // d15
            // 
            this.d15.DataPropertyName = "15";
            this.d15.HeaderText = "15";
            this.d15.Name = "d15";
            this.d15.ReadOnly = true;
            this.d15.Width = 30;
            // 
            // d16
            // 
            this.d16.DataPropertyName = "16";
            this.d16.HeaderText = "16";
            this.d16.Name = "d16";
            this.d16.ReadOnly = true;
            this.d16.Width = 30;
            // 
            // d17
            // 
            this.d17.DataPropertyName = "17";
            this.d17.HeaderText = "17";
            this.d17.Name = "d17";
            this.d17.ReadOnly = true;
            this.d17.Width = 30;
            // 
            // d18
            // 
            this.d18.DataPropertyName = "18";
            this.d18.HeaderText = "18";
            this.d18.Name = "d18";
            this.d18.ReadOnly = true;
            this.d18.Width = 30;
            // 
            // d19
            // 
            this.d19.DataPropertyName = "19";
            this.d19.HeaderText = "19";
            this.d19.Name = "d19";
            this.d19.ReadOnly = true;
            this.d19.Width = 30;
            // 
            // d20
            // 
            this.d20.DataPropertyName = "20";
            this.d20.HeaderText = "20";
            this.d20.Name = "d20";
            this.d20.ReadOnly = true;
            this.d20.Width = 30;
            // 
            // d21
            // 
            this.d21.DataPropertyName = "21";
            this.d21.HeaderText = "21";
            this.d21.Name = "d21";
            this.d21.ReadOnly = true;
            this.d21.Width = 30;
            // 
            // d22
            // 
            this.d22.DataPropertyName = "22";
            this.d22.HeaderText = "22";
            this.d22.Name = "d22";
            this.d22.ReadOnly = true;
            this.d22.Width = 30;
            // 
            // d23
            // 
            this.d23.DataPropertyName = "23";
            this.d23.HeaderText = "23";
            this.d23.Name = "d23";
            this.d23.ReadOnly = true;
            this.d23.Width = 30;
            // 
            // d24
            // 
            this.d24.DataPropertyName = "24";
            this.d24.HeaderText = "24";
            this.d24.Name = "d24";
            this.d24.ReadOnly = true;
            this.d24.Width = 30;
            // 
            // d25
            // 
            this.d25.DataPropertyName = "25";
            this.d25.HeaderText = "25";
            this.d25.Name = "d25";
            this.d25.ReadOnly = true;
            this.d25.Width = 30;
            // 
            // d26
            // 
            this.d26.DataPropertyName = "26";
            this.d26.HeaderText = "26";
            this.d26.Name = "d26";
            this.d26.ReadOnly = true;
            this.d26.Width = 30;
            // 
            // d27
            // 
            this.d27.DataPropertyName = "27";
            this.d27.HeaderText = "27";
            this.d27.Name = "d27";
            this.d27.ReadOnly = true;
            this.d27.Width = 30;
            // 
            // d28
            // 
            this.d28.DataPropertyName = "28";
            this.d28.HeaderText = "28";
            this.d28.Name = "d28";
            this.d28.ReadOnly = true;
            this.d28.Width = 30;
            // 
            // d29
            // 
            this.d29.DataPropertyName = "29";
            this.d29.HeaderText = "29";
            this.d29.Name = "d29";
            this.d29.ReadOnly = true;
            this.d29.Width = 30;
            // 
            // d30
            // 
            this.d30.DataPropertyName = "30";
            this.d30.HeaderText = "30";
            this.d30.Name = "d30";
            this.d30.ReadOnly = true;
            this.d30.Width = 30;
            // 
            // d31
            // 
            this.d31.DataPropertyName = "31";
            this.d31.HeaderText = "31";
            this.d31.Name = "d31";
            this.d31.ReadOnly = true;
            this.d31.Width = 30;
            // 
            // dataGridViewShiftToCheng
            // 
            this.dataGridViewShiftToCheng.AllowUserToAddRows = false;
            this.dataGridViewShiftToCheng.AllowUserToDeleteRows = false;
            this.dataGridViewShiftToCheng.AllowUserToResizeRows = false;
            this.dataGridViewShiftToCheng.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewShiftToCheng.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewShiftToCheng.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.day,
            this.old_shedule,
            this.id_old,
            this.new_shedule,
            this.id_new});
            this.dataGridViewShiftToCheng.Location = new System.Drawing.Point(3, 0);
            this.dataGridViewShiftToCheng.MultiSelect = false;
            this.dataGridViewShiftToCheng.Name = "dataGridViewShiftToCheng";
            this.dataGridViewShiftToCheng.ReadOnly = true;
            this.dataGridViewShiftToCheng.RowHeadersVisible = false;
            this.dataGridViewShiftToCheng.Size = new System.Drawing.Size(726, 220);
            this.dataGridViewShiftToCheng.TabIndex = 1;
            // 
            // day
            // 
            this.day.DataPropertyName = "day";
            this.day.HeaderText = "День";
            this.day.Name = "day";
            this.day.ReadOnly = true;
            // 
            // old_shedule
            // 
            this.old_shedule.DataPropertyName = "old_shift";
            this.old_shedule.HeaderText = "Старая";
            this.old_shedule.Name = "old_shedule";
            this.old_shedule.ReadOnly = true;
            // 
            // id_old
            // 
            this.id_old.HeaderText = "old";
            this.id_old.Name = "id_old";
            this.id_old.ReadOnly = true;
            this.id_old.Visible = false;
            // 
            // new_shedule
            // 
            this.new_shedule.DataPropertyName = "new_shift";
            this.new_shedule.HeaderText = "Новая";
            this.new_shedule.Name = "new_shedule";
            this.new_shedule.ReadOnly = true;
            // 
            // id_new
            // 
            this.id_new.HeaderText = "new";
            this.id_new.Name = "id_new";
            this.id_new.ReadOnly = true;
            this.id_new.Visible = false;
            // 
            // buttonEmp
            // 
            this.buttonEmp.Location = new System.Drawing.Point(517, 8);
            this.buttonEmp.Name = "buttonEmp";
            this.buttonEmp.Size = new System.Drawing.Size(27, 20);
            this.buttonEmp.TabIndex = 2;
            this.buttonEmp.Text = ".";
            this.buttonEmp.UseVisualStyleBackColor = true;
            this.buttonEmp.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxEmp
            // 
            this.textBoxEmp.Enabled = false;
            this.textBoxEmp.Location = new System.Drawing.Point(358, 8);
            this.textBoxEmp.Name = "textBoxEmp";
            this.textBoxEmp.Size = new System.Drawing.Size(150, 20);
            this.textBoxEmp.TabIndex = 3;
            // 
            // buttonDelShift
            // 
            this.buttonDelShift.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonDelShift.Location = new System.Drawing.Point(15, 439);
            this.buttonDelShift.Name = "buttonDelShift";
            this.buttonDelShift.Size = new System.Drawing.Size(75, 23);
            this.buttonDelShift.TabIndex = 5;
            this.buttonDelShift.Text = "Удалить";
            this.buttonDelShift.UseVisualStyleBackColor = true;
            this.buttonDelShift.Click += new System.EventHandler(this.buttonDelShift_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.Location = new System.Drawing.Point(896, 439);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 6;
            this.buttonClose.Text = "Отмена";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonОк
            // 
            this.buttonОк.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonОк.Location = new System.Drawing.Point(815, 439);
            this.buttonОк.Name = "buttonОк";
            this.buttonОк.Size = new System.Drawing.Size(75, 23);
            this.buttonОк.TabIndex = 7;
            this.buttonОк.Text = "Ок";
            this.buttonОк.UseVisualStyleBackColor = true;
            this.buttonОк.Click += new System.EventHandler(this.buttonОк_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(107, 444);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Количество измененных дней -";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(278, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "ФИО";
            // 
            // textBoxRes
            // 
            this.textBoxRes.Enabled = false;
            this.textBoxRes.Location = new System.Drawing.Point(358, 34);
            this.textBoxRes.Name = "textBoxRes";
            this.textBoxRes.Size = new System.Drawing.Size(150, 20);
            this.textBoxRes.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(278, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Причина";
            // 
            // buttonRes
            // 
            this.buttonRes.Location = new System.Drawing.Point(517, 34);
            this.buttonRes.Name = "buttonRes";
            this.buttonRes.Size = new System.Drawing.Size(27, 20);
            this.buttonRes.TabIndex = 13;
            this.buttonRes.Text = ".";
            this.buttonRes.UseVisualStyleBackColor = true;
            this.buttonRes.Click += new System.EventHandler(this.button5_Click);
            // 
            // textBoxComment
            // 
            this.textBoxComment.Location = new System.Drawing.Point(95, 64);
            this.textBoxComment.MaxLength = 50;
            this.textBoxComment.Name = "textBoxComment";
            this.textBoxComment.Size = new System.Drawing.Size(374, 20);
            this.textBoxComment.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Комментарии";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Отдел";
            // 
            // textBoxDep
            // 
            this.textBoxDep.Enabled = false;
            this.textBoxDep.Location = new System.Drawing.Point(95, 8);
            this.textBoxDep.Name = "textBoxDep";
            this.textBoxDep.Size = new System.Drawing.Size(38, 20);
            this.textBoxDep.TabIndex = 18;
            // 
            // splitContainerShedule
            // 
            this.splitContainerShedule.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerShedule.Location = new System.Drawing.Point(15, 90);
            this.splitContainerShedule.Name = "splitContainerShedule";
            this.splitContainerShedule.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerShedule.Panel1
            // 
            this.splitContainerShedule.Panel1.Controls.Add(this.dataGridViewShedule);
            // 
            // splitContainerShedule.Panel2
            // 
            this.splitContainerShedule.Panel2.Controls.Add(this.splitContainer1);
            this.splitContainerShedule.Size = new System.Drawing.Size(956, 343);
            this.splitContainerShedule.SplitterDistance = 110;
            this.splitContainerShedule.TabIndex = 21;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dataGridViewShiftToCheng);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridViewShift);
            this.splitContainer1.Size = new System.Drawing.Size(953, 223);
            this.splitContainer1.SplitterDistance = 732;
            this.splitContainer1.TabIndex = 2;
            // 
            // dataGridViewShift
            // 
            this.dataGridViewShift.AllowUserToAddRows = false;
            this.dataGridViewShift.AllowUserToDeleteRows = false;
            this.dataGridViewShift.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewShift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewShift.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_type,
            this.short_name,
            this.name_type});
            this.dataGridViewShift.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewShift.Name = "dataGridViewShift";
            this.dataGridViewShift.ReadOnly = true;
            this.dataGridViewShift.RowHeadersVisible = false;
            this.dataGridViewShift.Size = new System.Drawing.Size(211, 217);
            this.dataGridViewShift.TabIndex = 0;
            // 
            // id_type
            // 
            this.id_type.DataPropertyName = "id";
            this.id_type.HeaderText = "id_type";
            this.id_type.Name = "id_type";
            this.id_type.ReadOnly = true;
            this.id_type.Visible = false;
            // 
            // short_name
            // 
            this.short_name.DataPropertyName = "short_name";
            this.short_name.HeaderText = "short_name";
            this.short_name.Name = "short_name";
            this.short_name.ReadOnly = true;
            this.short_name.Visible = false;
            // 
            // name_type
            // 
            this.name_type.DataPropertyName = "name";
            this.name_type.HeaderText = "Название";
            this.name_type.Name = "name_type";
            this.name_type.ReadOnly = true;
            this.name_type.Width = 200;
            // 
            // textBoxShedule
            // 
            this.textBoxShedule.Enabled = false;
            this.textBoxShedule.Location = new System.Drawing.Point(95, 34);
            this.textBoxShedule.Name = "textBoxShedule";
            this.textBoxShedule.Size = new System.Drawing.Size(100, 20);
            this.textBoxShedule.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "График";
            // 
            // labelDayCount
            // 
            this.labelDayCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelDayCount.AutoSize = true;
            this.labelDayCount.Location = new System.Drawing.Point(278, 444);
            this.labelDayCount.Name = "labelDayCount";
            this.labelDayCount.Size = new System.Drawing.Size(13, 13);
            this.labelDayCount.TabIndex = 24;
            this.labelDayCount.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(560, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(401, 64);
            this.label7.TabIndex = 25;
            this.label7.Text = "Редактировать возможно лишь последющие дни.\r\n\r\nПример:\r\nЕсли сегодня 5-е, то реда" +
                "ктирование доступно с 6-го чилса.";
            // 
            // Form_shedule_chenge_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 474);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.labelDayCount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxShedule);
            this.Controls.Add(this.splitContainerShedule);
            this.Controls.Add(this.textBoxDep);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxComment);
            this.Controls.Add(this.buttonRes);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxRes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonОк);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonDelShift);
            this.Controls.Add(this.textBoxEmp);
            this.Controls.Add(this.buttonEmp);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(500, 320);
            this.Name = "Form_shedule_chenge_edit";
            this.Load += new System.EventHandler(this.Form_chenge_edit_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form_chenge_edit_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShiftToCheng)).EndInit();
            this.splitContainerShedule.Panel1.ResumeLayout(false);
            this.splitContainerShedule.Panel2.ResumeLayout(false);
            this.splitContainerShedule.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShift)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewShedule;
        private System.Windows.Forms.DataGridView dataGridViewShiftToCheng;
        private System.Windows.Forms.Button buttonEmp;
        private System.Windows.Forms.Button buttonDelShift;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Button buttonОк;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonRes;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dayDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn newDataGridViewTextBoxColumn;
        public System.Windows.Forms.TextBox textBoxComment;
        public System.Windows.Forms.TextBox textBoxDep;
        private System.Windows.Forms.SplitContainer splitContainerShedule;
        private System.Windows.Forms.TextBox textBoxShedule;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelDayCount;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dataGridViewShift;
        private System.Windows.Forms.TextBox textBoxEmp;
        private System.Windows.Forms.TextBox textBoxRes;
        private System.Windows.Forms.DataGridViewTextBoxColumn oreder;
        private System.Windows.Forms.DataGridViewTextBoxColumn d1;
        private System.Windows.Forms.DataGridViewTextBoxColumn d2;
        private System.Windows.Forms.DataGridViewTextBoxColumn d3;
        private System.Windows.Forms.DataGridViewTextBoxColumn d4;
        private System.Windows.Forms.DataGridViewTextBoxColumn d5;
        private System.Windows.Forms.DataGridViewTextBoxColumn d6;
        private System.Windows.Forms.DataGridViewTextBoxColumn d7;
        private System.Windows.Forms.DataGridViewTextBoxColumn d8;
        private System.Windows.Forms.DataGridViewTextBoxColumn d9;
        private System.Windows.Forms.DataGridViewTextBoxColumn d10;
        private System.Windows.Forms.DataGridViewTextBoxColumn d11;
        private System.Windows.Forms.DataGridViewTextBoxColumn d12;
        private System.Windows.Forms.DataGridViewTextBoxColumn d13;
        private System.Windows.Forms.DataGridViewTextBoxColumn d14;
        private System.Windows.Forms.DataGridViewTextBoxColumn d15;
        private System.Windows.Forms.DataGridViewTextBoxColumn d16;
        private System.Windows.Forms.DataGridViewTextBoxColumn d17;
        private System.Windows.Forms.DataGridViewTextBoxColumn d18;
        private System.Windows.Forms.DataGridViewTextBoxColumn d19;
        private System.Windows.Forms.DataGridViewTextBoxColumn d20;
        private System.Windows.Forms.DataGridViewTextBoxColumn d21;
        private System.Windows.Forms.DataGridViewTextBoxColumn d22;
        private System.Windows.Forms.DataGridViewTextBoxColumn d23;
        private System.Windows.Forms.DataGridViewTextBoxColumn d24;
        private System.Windows.Forms.DataGridViewTextBoxColumn d25;
        private System.Windows.Forms.DataGridViewTextBoxColumn d26;
        private System.Windows.Forms.DataGridViewTextBoxColumn d27;
        private System.Windows.Forms.DataGridViewTextBoxColumn d28;
        private System.Windows.Forms.DataGridViewTextBoxColumn d29;
        private System.Windows.Forms.DataGridViewTextBoxColumn d30;
        private System.Windows.Forms.DataGridViewTextBoxColumn d31;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn day;
        private System.Windows.Forms.DataGridViewTextBoxColumn old_shedule;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_old;
        private System.Windows.Forms.DataGridViewTextBoxColumn new_shedule;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_new;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_type;
        private System.Windows.Forms.DataGridViewTextBoxColumn short_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn name_type;
    }
}