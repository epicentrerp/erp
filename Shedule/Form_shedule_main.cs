﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Report;
using Journal;

namespace Shedule
{
    public partial class Form_shedule_main : Form
    {
        Form_dep depSelect;

        private string mBegin, yBegin, mEnd;


        public Form_shedule_main()
        {
            InitializeComponent();
        }

        private void newShedule_Click(object sender, EventArgs e)
        {
            Form_shedule_new newShedule = new Form_shedule_new();
            newShedule.MdiParent = this.MdiParent;
            newShedule.FormClosing += (sender1, e1) =>
            {
                UpdateSheduleGrid();
            };
            newShedule.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UpdateSheduleGrid();
        }

        private void editShedule_Click(object sender, EventArgs e)
        {
            editSelectShedule();
        }

        private void Form_shedule_main_Load(object sender, EventArgs e)
        {
            numericUpDownYearBegin.Value = DateTime.Now.Year;
            numericUpDownMonthBegin.Value = DateTime.Now.Month;
            numericUpDownMonthEnd.Value = DateTime.Now.Month;

            mBegin = numericUpDownMonthBegin.Value.ToString();
            yBegin = numericUpDownYearBegin.Value.ToString();
            mEnd = numericUpDownMonthEnd.Value.ToString();
            
            UpdateShedule();
            accessLevel();
        }
        
        private int old_x, old_y;
        
        private void UpdateSheduleGrid()
        {
            if (dataGridViewShedule.RowCount != 0)
            {
                old_y = dataGridViewShedule.CurrentCell.RowIndex;
                old_x = dataGridViewShedule.CurrentCell.ColumnIndex;
            }

            UpdateShedule();

            if (dataGridViewShedule.RowCount >= old_y)
            {
                if (old_x == 0)
                {
                    if (dataGridViewShedule.RowCount != 0)
                        dataGridViewShedule.CurrentCell = dataGridViewShedule[1, 0];
                }
                else
                    dataGridViewShedule.CurrentCell = dataGridViewShedule[old_x, old_y];
            }
            else
            {
                if (dataGridViewShedule.RowCount != 0 && old_y != 0)
                {
                    dataGridViewShedule.CurrentCell = dataGridViewShedule[old_x, dataGridViewShedule.RowCount - 1];
                }
            }            
        }

        private void UpdateShedule()
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();
            string sql;

            if (textBoxDep.Text != "")
            {
                sql = "SELECT [id],[status],[status_nach],[docs_view].[dep],[doc_numder_full],[date_create],[time_create],[name_ru],[shedule_date] "
                + " FROM [docs_view] inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
                + " on [docs_view].dep = dep_table.dep"
                + " WHERE [id_type] = 0 and docs_view.dep = @dep "
                + " and shedule_month >= @mBegin and shedule_month <= @mEnd and shedule_year >= @yBegin "
                + " ORDER BY id";
                sqlCmd.Parameters.AddWithValue("@dep", textBoxDep.Text);
                //dbo.docs.shedule_month, dbo.docs.shedule_year
            }
            else
            {
                sql = "SELECT [id],[status],[status_nach],[docs_view].[dep],[doc_numder_full],[date_create],[time_create],[name_ru],[shedule_date] "
                + " FROM [docs_view] inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
                + " on [docs_view].dep = dep_table.dep"
                + " WHERE [id_type] = 0 "
                + " and shedule_month >= @mBegin and shedule_month <= @mEnd and shedule_year = @yBegin "
                + " ORDER BY id";
            }

            sqlCmd.Connection = sqlConn;
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = sql;
            sqlCmd.Parameters.AddWithValue("@id_user", ProSetting.User);
            sqlCmd.Parameters.AddWithValue("@mBegin", mBegin);
            sqlCmd.Parameters.AddWithValue("@yBegin", yBegin);
            sqlCmd.Parameters.AddWithValue("@mEnd", mEnd);

            sqlConn.Open();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = sqlCmd;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            dataGridViewShedule.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }

        private void утвердитьНачальникомToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand("docs_chenge_status", sqlConn);

            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@input_id_doc", Convert.ToInt32(dataGridViewShedule[0, dataGridViewShedule.CurrentCell.RowIndex].Value.ToString()));
            sqlCmd.Parameters.AddWithValue("@input_chenge", 0);
            sqlCmd.Parameters.AddWithValue("@input_how_change", 0);
            sqlCmd.Parameters.AddWithValue("@input_id_emp", ProSetting.User); // пока не используется

            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();
            UpdateSheduleGrid();
        }

        private void начToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand("docs_chenge_status", sqlConn);

            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@input_id_doc", dataGridViewShedule[0, dataGridViewShedule.CurrentCell.RowIndex].Value.ToString());
            sqlCmd.Parameters.AddWithValue("@input_chenge", 1);
            sqlCmd.Parameters.AddWithValue("@input_how_change", 0);
            sqlCmd.Parameters.AddWithValue("@input_id_emp", ProSetting.User);// пока не используется

            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();
            UpdateSheduleGrid();
        }

        private void дирToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand("docs_chenge_status", sqlConn);

            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@input_id_doc", dataGridViewShedule[0, dataGridViewShedule.CurrentCell.RowIndex].Value.ToString());
            sqlCmd.Parameters.AddWithValue("@input_chenge", 0);
            sqlCmd.Parameters.AddWithValue("@input_how_change", 1);
            sqlCmd.Parameters.AddWithValue("@input_id_emp", ProSetting.User);// пока не используется

            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();
            UpdateSheduleGrid();
        }

        private void дирСнятьПроведениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand("docs_chenge_status", sqlConn);

            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@input_id_doc", dataGridViewShedule[0, dataGridViewShedule.CurrentCell.RowIndex].Value.ToString());
            sqlCmd.Parameters.AddWithValue("@input_chenge", 1);
            sqlCmd.Parameters.AddWithValue("@input_how_change", 1);
            sqlCmd.Parameters.AddWithValue("@input_id_emp", ProSetting.User);// пока не используется

            sqlConn.Open();
            sqlCmd.ExecuteNonQuery();
            sqlConn.Close();
            UpdateSheduleGrid();
        }

        private void accessLevel()
        {
            начToolStripMenuItem.Enabled = true;
            утвердитьНачальникомToolStripMenuItem.Enabled = true;

            if (ProSetting.Access == 0)
            {
                дирToolStripMenuItem.Enabled = true;
                дирСнятьПроведениеToolStripMenuItem.Enabled = true;

            }
            if (ProSetting.Access == 1)
            {
                дирToolStripMenuItem.Enabled = true;
                дирСнятьПроведениеToolStripMenuItem.Enabled = true;
            }
        }

        private void editSelectShedule()
        {
            int id_doc = Convert.ToInt32(dataGridViewShedule[0, dataGridViewShedule.CurrentCell.RowIndex].Value.ToString());

            string blocked = "";

            using (var sqlConn = new SqlConnection(ProSetting.ConnetionString))
            {
                var sqlCmd = new SqlCommand("blocked_docs", sqlConn);
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@input_doc", id_doc); // ели значение писаное из базы то через value
                sqlCmd.Parameters.AddWithValue("@input_id_user", ProSetting.User); // если значение записано хардкорно то через Item
                sqlCmd.Parameters.AddWithValue("@input_move", 0);

                sqlConn.Open();
                using (SqlDataReader dr = sqlCmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        blocked = dr.GetString(dr.GetOrdinal("rez"));
                    }
                }
                sqlConn.Close();
            }

            if (blocked == "0")
            {
                Form_shedule_edit editShedule = new Form_shedule_edit(id_doc);
                editShedule.MdiParent = this.MdiParent;
                editShedule.Show();
            }
            else
            {
                MessageBox.Show(blocked);
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            editSelectShedule();
        }

        private void печатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_rep_shedule editShedule = new Form_rep_shedule(Convert.ToInt32(dataGridViewShedule[0, dataGridViewShedule.CurrentCell.RowIndex].Value.ToString()), true);
            editShedule.MdiParent = this.MdiParent;
            editShedule.FormClosing += (sender1, e1) =>
            {
                UpdateSheduleGrid();
            };
            editShedule.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            UpdateSheduleGrid();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (depSelect == null || depSelect.IsDisposed)
            {
                depSelect = new Form_dep(ProSetting.User);
                depSelect.MdiParent = this.MdiParent;
                depSelect.FormClosing += (sender1, e1) =>
                {
                    if (depSelect.dep != null)
                    {
                        textBoxDep.Text = depSelect.dep;
                        UpdateSheduleGrid();
                    }
                };
                depSelect.Show();
            }
            else
            {
                depSelect.Activate();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBoxDep.Clear();
            UpdateSheduleGrid();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            mBegin = numericUpDownMonthBegin.Value.ToString();
            yBegin = numericUpDownYearBegin.Value.ToString();
            mEnd = numericUpDownMonthEnd.Value.ToString();
            UpdateSheduleGrid();
        }

        private void созданиеИзгToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int status = 0;
            SqlConnection sqlConn1 = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd1 = new SqlCommand();
            // проверка проведен ли документ диром
            // заполнение полей формы
            string sql = "SELECT [id],[status] "
            + " FROM [docs_view] "
            + " WHERE [id] = @id_doc";
            sqlCmd1.Connection = sqlConn1;
            sqlCmd1.Parameters.Clear();
            sqlCmd1.CommandType = CommandType.Text;
            sqlCmd1.Parameters.AddWithValue("@id_doc", dataGridViewShedule[0, dataGridViewShedule.CurrentCell.RowIndex].Value.ToString());
            sqlCmd1.CommandText = sql;

            sqlConn1.Open();

            SqlDataReader dr = sqlCmd1.ExecuteReader();
            while (dr.Read())
            {
                status = dr.GetInt32(dr.GetOrdinal("status"));
            }
            sqlConn1.Close();
            if (status == 1)
            {
                Form_shedule_chenge_edit edit_chenge = new Form_shedule_chenge_edit(Convert.ToInt32(dataGridViewShedule[0, dataGridViewShedule.CurrentCell.RowIndex].Value.ToString()), 0);
                edit_chenge.MdiParent = this.MdiParent;
                edit_chenge.Show();
            }
            else
            {
                MessageBox.Show("График не утвержден директором.");
            }
        }

        private void печатьТабельToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_rep_shedule editShedule = new Form_rep_shedule(Convert.ToInt32(dataGridViewShedule[0, dataGridViewShedule.CurrentCell.RowIndex].Value.ToString()), false);
            editShedule.MdiParent = this.MdiParent;
            editShedule.FormClosing += (sender1, e1) =>
            {
                UpdateSheduleGrid();
            };
            editShedule.Show();
        }

        private void toolStripSeparator1_Click(object sender, EventArgs e)
        {

        }

        private void историяДокументаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_rep_history editShedule = new Form_rep_history(dataGridViewShedule[0, dataGridViewShedule.CurrentCell.RowIndex].Value.ToString());
            editShedule.MdiParent = this.MdiParent;
            editShedule.Show();
        }
    }
}
