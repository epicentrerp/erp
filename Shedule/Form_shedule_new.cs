﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Shedule
{
    public partial class Form_shedule_new : Form
    {

        //private string conString = @"Data Source=192.168.40.200;Initial Catalog=balu;Persist Security Info=True;User ID=admin;Password=njgjkm;";

        public Form_shedule_new()
        {
            InitializeComponent();
        }

        private void Form_shedule_new_Load(object sender, EventArgs e)
        {
            SqlConnection dbConn = new SqlConnection(ProSetting.ConnetionString);
            string logins = "SELECT dep FROM [users_dep] "
                            + " WHERE id_users = " + ProSetting.User
                            + " ORDER by dep";
            SqlDataAdapter dataAdapter = new SqlDataAdapter(logins, dbConn);

            DataSet dataSet = new DataSet();
            dbConn.Open();
            dataAdapter.Fill(dataSet);
            dbConn.Close();

            comboBox1.DataSource = dataSet.Tables[0];
            comboBox1.DisplayMember = "dep";
            comboBox1.ValueMember = "dep";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id_doc = 0;

            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();

            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.CommandText = "shedule_create";
            sqlCmd.Connection = sqlConn;

            sqlCmd.Parameters.AddWithValue("@inpit_dep", comboBox1.SelectedValue.ToString()); // ели значение писаное из базы то через value
            sqlCmd.Parameters.AddWithValue("@input_monthe", comboBox2.SelectedItem.ToString()); // если значение записано хардкорно то через Item
            sqlCmd.Parameters.AddWithValue("@input_year", comboBox3.SelectedItem.ToString());
            sqlCmd.Parameters.AddWithValue("@input_user", ProSetting.User);

            sqlConn.Open();
            SqlDataReader dr = sqlCmd.ExecuteReader();
            while (dr.Read())
            {
                id_doc = dr.GetInt32(dr.GetOrdinal("doc_id"));
            }
            sqlConn.Close();

            if (id_doc == 0)
                MessageBox.Show("Распсиание для данного отдела уже состалвено уже создано.");
            else
            {
                Form_shedule_edit editShedule = new Form_shedule_edit(id_doc);
                editShedule.MdiParent = this.MdiParent;
                editShedule.Show();
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
