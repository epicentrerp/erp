﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Report;

namespace Shedule
{
    public partial class Form_shedule_edit : Form
    {
        //private string conString = @"Data Source=192.168.40.200;Initial Catalog=balu;Persist Security Info=True;User ID=admin;Password=njgjkm;";
        Form_shedule_add_emp shedAddEmp;
        private int id_doc;
        private int statusNach, year, month;
        private int old_x, old_y, old_x_s, old_y_s;

        public Form_shedule_edit(int id_doc)
        {
            InitializeComponent();
            this.id_doc = id_doc;
        }

        private void Form_shedule_edit_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            con.ConnectionString = ProSetting.ConnetionString;
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "[shedule_view]";
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@input_shedule", id_doc);

            con.Open();
            try
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = cmd;
                DataSet ds = new DataSet();
                dataAdapter.Fill(ds);
                dataGridView_shedule.DataSource = ds.Tables[0].DefaultView;
            }
            catch (SqlException)
            {
                MessageBox.Show("Возникла ошибка загрузки распсиания!");
            }
            finally
            {
                con.Close();
            }

            con.ConnectionString = ProSetting.ConnetionString;
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "[shedule_view_count]";
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@input_shedule", id_doc);

            con.Open();
            try
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = cmd;
                DataSet ds = new DataSet();
                dataAdapter.Fill(ds);
                dataGridView_stat.DataSource = ds.Tables[0].DefaultView;
            }
            catch (SqlException)
            {
                MessageBox.Show("Возникла ошибка загрузки распсиания!");
            }
            finally
            {
                con.Close();
            }

            ColorGrid();

            using (var sqlConn = new SqlConnection(ProSetting.ConnetionString))
            {
                var sqlCmd = new SqlCommand("SELECT [status_nach],doc_numder_full, shedule_year, shedule_month FROM [docs_view] WHERE [id] = @id_doc ", sqlConn);
                sqlCmd.CommandType = CommandType.Text;

                sqlCmd.Parameters.AddWithValue("@id_doc", id_doc); // 

                sqlConn.Open();
                try
                {
                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            statusNach = dr.GetInt32(dr.GetOrdinal("status_nach"));
                            this.Text = dr.GetString(dr.GetOrdinal("doc_numder_full"));
                            year = dr.GetInt32(dr.GetOrdinal("shedule_year"));
                            month = dr.GetInt32(dr.GetOrdinal("shedule_month"));
                        }
                    }
                }
                catch (SqlException)
                {
                    MessageBox.Show("Возникла ошибка загрузки!");
                }
                finally
                {
                    sqlConn.Close();
                }
            }
            // дописать загрузку смен согласно отделу

            SqlConnection sqlConn1 = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd1 = new SqlCommand();
            SqlDataAdapter dataAdapter1 = new SqlDataAdapter();
            DataSet dataSet = new DataSet();

            string shift = "SELECT [id],([shedule_shift_type].[name]+' '+comment+' '+ISNULL(Convert(varchar,[shift_start]),'')+' '+ISNULL(Convert(varchar,[shift_end]),'')) as [name]"
                            + " FROM [shedule_shift_type] inner join [shedule_shift_type_dep]"
                            + " ON [shedule_shift_type].id = [shedule_shift_type_dep].[id_shift] "
                            + " WHERE [shedule_shift_type_dep].dep = (SELECT dep FROM docs WHERE id = @id_doc) ORDER BY name";
            DataSet dataSet1 = new DataSet();
            sqlCmd1.Parameters.Clear();
            sqlCmd1.CommandText = shift;
            sqlCmd1.Connection = sqlConn1;
            sqlCmd1.Parameters.AddWithValue("@id_doc", id_doc);

            sqlConn1.Open();
            try
            {
                dataAdapter1.SelectCommand = sqlCmd1;
                dataAdapter1.Fill(dataSet1);
                dataGridViewShift.DataSource = dataSet1.Tables[0].DefaultView;
            }
            catch (SqlException)
            {
                MessageBox.Show("Возникла ошибка загрузки!");
            }
            finally
            {
                sqlConn1.Close();
            }

            dayMonthCount();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // запрещаем редактировать первую строку таблицы, первые две колонки где хранится фио и айди, плюс последнюю строку количества выходных
            if (statusNach == 0 )
            {/*
                if (dataGridView_shedule.CurrentCell.RowIndex > 0 && dataGridView_shedule.CurrentCell.ColumnIndex > 1 && dataGridView_shedule.CurrentCell.ColumnIndex < dataGridView_shedule.ColumnCount - 2)
                {
                    // получаем расположение ячейки, в которой необходимо переместить комбо бокс
                    Point pCell = dataGridView_shedule.GetCellDisplayRectangle(dataGridView_shedule.CurrentCell.ColumnIndex, dataGridView_shedule.CurrentCell.RowIndex, true).Location;
                    Point pGrid = dataGridView_shedule.Location;
                    // перемещаем сомбо бокс на необходимую позицию
                    comboBox_chengeShift.Location = new Point(pCell.X + pGrid.X, pCell.Y + pGrid.Y);
                    comboBox_chengeShift.Visible = true;
                    comboBox_chengeShift.DroppedDown = true;
                }*/
                // дописать возможность сохранения измненеия и обновления грида
                if (dataGridViewShift.CurrentCell.RowIndex > -1 & dataGridViewShift.CurrentCell.ColumnIndex > 0 & dataGridView_shedule.CurrentCell.ColumnIndex > 2 & dataGridView_shedule.CurrentCell.ColumnIndex < dataGridView_shedule.ColumnCount - 3)
                {
                    SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                    SqlCommand sqlCmd = new SqlCommand("shedule_chenge_shift", sqlConn);

                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@input_shedule", id_doc); // ели значение писаное из базы то через value
                    sqlCmd.Parameters.AddWithValue("@input_id_emp", dataGridView_shedule[0, dataGridView_shedule.CurrentCell.RowIndex].Value.ToString()); // если значение записано хардкорно то через Item
                    sqlCmd.Parameters.AddWithValue("@input_day", dataGridView_shedule.Columns[dataGridView_shedule.CurrentCell.ColumnIndex].HeaderText.ToString());
                    sqlCmd.Parameters.AddWithValue("@input_new_shift", dataGridViewShift[0, dataGridViewShift.CurrentCell.RowIndex].Value.ToString());

                    sqlConn.Open();
                    sqlCmd.ExecuteNonQuery();
                    sqlConn.Close();
                    /*
                    comboBox_chengeShift.Visible = false;
                    comboBox_chengeShift.SelectedIndex = 0;
                    */
                }
                UpdateShedule();
            }
        }

        private void UpdateShedule()
        {
            if (dataGridView_shedule.RowCount != 0)
            {
                old_y = dataGridView_shedule.CurrentCell.RowIndex;
                old_x = dataGridView_shedule.CurrentCell.ColumnIndex;
            }

            if (dataGridView_stat.RowCount != 0)
            {
                old_y_s = dataGridView_stat.CurrentCell.RowIndex;
                old_x_s = dataGridView_stat.CurrentCell.ColumnIndex;
            }
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            con.ConnectionString = ProSetting.ConnetionString;
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "[shedule_view]";
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@input_shedule", id_doc);

            con.Open();
            try
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = cmd;
                DataSet ds = new DataSet();
                dataAdapter.Fill(ds);
                dataGridView_shedule.DataSource = ds.Tables[0].DefaultView;
            }
            catch (SqlException)
            {
                MessageBox.Show("Возникла ошибка загрузки распсиания!");
            }
            finally
            {
                con.Close();
            }

            con.ConnectionString = ProSetting.ConnetionString;
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "[shedule_view_count]";
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@input_shedule", id_doc);

            con.Open();
            try
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = cmd;
                DataSet ds = new DataSet();
                dataAdapter.Fill(ds);
                dataGridView_stat.DataSource = ds.Tables[0].DefaultView;
            }
            catch (SqlException)
            {
                MessageBox.Show("Возникла ошибка загрузки распсиания!");
            }
            finally
            {
                con.Close();
            }
            
            if (dataGridView_shedule.RowCount > old_y)
            {
                dataGridView_shedule.CurrentCell = dataGridView_shedule[old_x, old_y];
            }
            else
            {
                if (dataGridView_shedule.RowCount != 0)
                {
                    dataGridView_shedule.CurrentCell = dataGridView_shedule[old_x, dataGridView_shedule.RowCount - 1];
                }
            }

            if (dataGridView_stat.RowCount > old_y_s)
            {
                dataGridView_stat.CurrentCell = dataGridView_stat[old_x_s, old_y_s];
            }
            else
            {
                if (dataGridView_stat.RowCount != 0)
                {
                    dataGridView_stat.CurrentCell = dataGridView_stat[old_x_s, dataGridView_stat.RowCount - 1];
                }
            }

        }

        private void delEmp_Click(object sender, EventArgs e)
        {
            if (statusNach == 0)
            {
                if (dataGridView_shedule.CurrentCell.RowIndex > 0)
                {
                    // дописать возможность сохранения измненеия и обновления грида
                    SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                    SqlCommand sqlCmd = new SqlCommand("shedule_emp_del", sqlConn);

                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@input_shedule", id_doc); // ели значение писаное из базы то через value
                    sqlCmd.Parameters.AddWithValue("@input_id_emp", dataGridView_shedule[0, dataGridView_shedule.CurrentCell.RowIndex].Value.ToString()); // если значение записано хардкорно то через Item

                    sqlConn.Open();
                    sqlCmd.ExecuteNonQuery();
                    sqlConn.Close();

                    UpdateShedule();
                }
            }
            else
            {
                if (ProSetting.Access == 0 )
                {
                    // дописать возможность сохранения измненеия и обновления грида
                    SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                    SqlCommand sqlCmd = new SqlCommand("shedule_emp_del", sqlConn);

                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@input_shedule", id_doc); // ели значение писаное из базы то через value
                    sqlCmd.Parameters.AddWithValue("@input_id_emp", dataGridView_shedule[0, dataGridView_shedule.CurrentCell.RowIndex].Value.ToString()); // если значение записано хардкорно то через Item

                    sqlConn.Open();
                    sqlCmd.ExecuteNonQuery();
                    sqlConn.Close();

                    UpdateShedule();
                }
            }
        }

        private void Form_shedule_edit_FormClosing(object sender, FormClosingEventArgs e)
        {

            SqlConnection connection = new SqlConnection(ProSetting.ConnetionString);
            try
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "DELETE FROM  [docs_bloked] WHERE id_doc = @id_doc and id_user = @id_user";
                connection.Open();
                
                command.Parameters.Clear();
                command.Parameters.AddWithValue("@id_doc", id_doc);
                command.Parameters.AddWithValue("@id_user", ProSetting.User);
                command.ExecuteNonQuery();
            }
            catch (SqlException)
            {

            }
            finally
            {
                connection.Close();
            }
        }

        private void ColorGrid()
        {
            for (int i = 2; i < dataGridView_shedule.ColumnCount- 3; i++)
            {
                if (dataGridView_shedule[i, 0].FormattedValue.ToString().Contains("Сб".Trim()) || dataGridView_shedule[i, 0].FormattedValue.ToString().Contains("Вс".Trim()))
                {
                    dataGridView_shedule.Columns[i].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
            
            for (int i = 2; i < dataGridView_stat.ColumnCount; i++)
            {
                if (dataGridView_stat[i, 0].FormattedValue.ToString().Contains("Вс".Trim()) || dataGridView_stat[i, 0].FormattedValue.ToString().Contains("Сб".Trim()))
               {
                    this.dataGridView_stat.Columns[i].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }

            for (int i = 35; i < dataGridView_shedule.ColumnCount; i++)
            {
                this.dataGridView_shedule.Columns[i].DefaultCellStyle.BackColor = Color.SkyBlue;
            }

            for (int i = 1; i < 2; i++)
            {
                this.dataGridView_shedule.Columns[i].DefaultCellStyle.BackColor = Color.SkyBlue;
            }
        }

        private void dayMonthCount()
        {
            int days = DateTime.DaysInMonth(year, month);

            if (days == 30)
            {
                dataGridView_shedule.Columns[34].Visible = false;
                dataGridView_stat.Columns[32].Visible = false;
            }
            if (days == 29)
            {
                dataGridView_shedule.Columns[34].Visible = false;
                dataGridView_stat.Columns[32].Visible = false;
                dataGridView_shedule.Columns[33].Visible = false;
                dataGridView_stat.Columns[31].Visible = false;
            }
            if (days == 28)
            {
                dataGridView_shedule.Columns[34].Visible = false;
                dataGridView_stat.Columns[32].Visible = false;
                dataGridView_shedule.Columns[33].Visible = false;
                dataGridView_stat.Columns[31].Visible = false;
                dataGridView_shedule.Columns[32].Visible = false;
                dataGridView_stat.Columns[30].Visible = false;
            }
        }

        private void addEmp_Click(object sender, EventArgs e)
        {
            if (statusNach == 0)
            {
                if (shedAddEmp == null || shedAddEmp.IsDisposed)
                {
                    shedAddEmp = new Form_shedule_add_emp(id_doc);

                    shedAddEmp.MdiParent = this.MdiParent;
                    shedAddEmp.FormClosing += (sender1, e1) =>
                    {
                        UpdateShedule();

                    };
                    shedAddEmp.Show();
                }
                else
                {
                    shedAddEmp.Activate();
                }
            }
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            Form_rep_shedule editShedule = new Form_rep_shedule(Convert.ToInt32(id_doc), true);
            editShedule.MdiParent = this.MdiParent;
            editShedule.Show();
        }

        private void buttonTimeSheet_Click(object sender, EventArgs e)
        {
            Form_rep_shedule editShedule = new Form_rep_shedule(Convert.ToInt32(id_doc), false);
            editShedule.MdiParent = this.MdiParent;
            editShedule.Show();
        }
    }
}
