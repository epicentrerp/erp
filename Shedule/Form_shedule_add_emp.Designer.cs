﻿namespace Shedule
{
    partial class Form_shedule_add_emp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGridView_addEmp = new System.Windows.Forms.DataGridView();
            this.id_emp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name_ru = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView_sheduleEmp = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewShift = new System.Windows.Forms.DataGridView();
            this.id_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.short_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.type_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_addEmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_sheduleEmp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShift)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(844, 288);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Сохранить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(925, 288);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Отмена";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // dataGridView_addEmp
            // 
            this.dataGridView_addEmp.AllowUserToAddRows = false;
            this.dataGridView_addEmp.AllowUserToDeleteRows = false;
            this.dataGridView_addEmp.AllowUserToResizeRows = false;
            this.dataGridView_addEmp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_addEmp.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_addEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_addEmp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_emp,
            this.dep,
            this.name_ru});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_addEmp.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_addEmp.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_addEmp.Name = "dataGridView_addEmp";
            this.dataGridView_addEmp.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_addEmp.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_addEmp.RowHeadersVisible = false;
            this.dataGridView_addEmp.Size = new System.Drawing.Size(554, 114);
            this.dataGridView_addEmp.TabIndex = 2;
            this.dataGridView_addEmp.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_addEmp_CellDoubleClick);
            // 
            // id_emp
            // 
            this.id_emp.DataPropertyName = "id";
            this.id_emp.HeaderText = "id_emp";
            this.id_emp.Name = "id_emp";
            this.id_emp.ReadOnly = true;
            this.id_emp.Visible = false;
            // 
            // dep
            // 
            this.dep.DataPropertyName = "dep";
            this.dep.HeaderText = "Отдел";
            this.dep.Name = "dep";
            this.dep.ReadOnly = true;
            // 
            // name_ru
            // 
            this.name_ru.DataPropertyName = "name_ru";
            this.name_ru.HeaderText = "ФИО";
            this.name_ru.Name = "name_ru";
            this.name_ru.ReadOnly = true;
            this.name_ru.Width = 200;
            // 
            // dataGridView_sheduleEmp
            // 
            this.dataGridView_sheduleEmp.AllowUserToAddRows = false;
            this.dataGridView_sheduleEmp.AllowUserToDeleteRows = false;
            this.dataGridView_sheduleEmp.AllowUserToResizeRows = false;
            this.dataGridView_sheduleEmp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_sheduleEmp.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView_sheduleEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_sheduleEmp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.name,
            this.d1,
            this.d2,
            this.d3,
            this.d4,
            this.d5,
            this.d6,
            this.d7,
            this.d8,
            this.d9,
            this.d10,
            this.d11,
            this.d12,
            this.d13,
            this.d14,
            this.d15,
            this.d16,
            this.d17,
            this.d18,
            this.d19,
            this.d20,
            this.d21,
            this.d22,
            this.d23,
            this.d24,
            this.d25,
            this.d26,
            this.d27,
            this.d28,
            this.d29,
            this.d30,
            this.d31});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_sheduleEmp.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView_sheduleEmp.Location = new System.Drawing.Point(6, 3);
            this.dataGridView_sheduleEmp.Name = "dataGridView_sheduleEmp";
            this.dataGridView_sheduleEmp.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_sheduleEmp.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView_sheduleEmp.RowHeadersVisible = false;
            this.dataGridView_sheduleEmp.Size = new System.Drawing.Size(976, 134);
            this.dataGridView_sheduleEmp.TabIndex = 3;
            this.dataGridView_sheduleEmp.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_sheduleEmp_CellDoubleClick);
            // 
            // id
            // 
            this.id.Frozen = true;
            this.id.HeaderText = "";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Width = 30;
            // 
            // name
            // 
            this.name.Frozen = true;
            this.name.HeaderText = "ФИО";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Width = 150;
            // 
            // d1
            // 
            this.d1.HeaderText = "1";
            this.d1.Name = "d1";
            this.d1.ReadOnly = true;
            this.d1.Width = 25;
            // 
            // d2
            // 
            this.d2.HeaderText = "2";
            this.d2.Name = "d2";
            this.d2.ReadOnly = true;
            this.d2.Width = 25;
            // 
            // d3
            // 
            this.d3.HeaderText = "3";
            this.d3.Name = "d3";
            this.d3.ReadOnly = true;
            this.d3.Width = 25;
            // 
            // d4
            // 
            this.d4.HeaderText = "4";
            this.d4.Name = "d4";
            this.d4.ReadOnly = true;
            this.d4.Width = 25;
            // 
            // d5
            // 
            this.d5.HeaderText = "5";
            this.d5.Name = "d5";
            this.d5.ReadOnly = true;
            this.d5.Width = 25;
            // 
            // d6
            // 
            this.d6.HeaderText = "6";
            this.d6.Name = "d6";
            this.d6.ReadOnly = true;
            this.d6.Width = 25;
            // 
            // d7
            // 
            this.d7.HeaderText = "7";
            this.d7.Name = "d7";
            this.d7.ReadOnly = true;
            this.d7.Width = 25;
            // 
            // d8
            // 
            this.d8.HeaderText = "8";
            this.d8.Name = "d8";
            this.d8.ReadOnly = true;
            this.d8.Width = 25;
            // 
            // d9
            // 
            this.d9.HeaderText = "9";
            this.d9.Name = "d9";
            this.d9.ReadOnly = true;
            this.d9.Width = 25;
            // 
            // d10
            // 
            this.d10.HeaderText = "10";
            this.d10.Name = "d10";
            this.d10.ReadOnly = true;
            this.d10.Width = 25;
            // 
            // d11
            // 
            this.d11.HeaderText = "11";
            this.d11.Name = "d11";
            this.d11.ReadOnly = true;
            this.d11.Width = 25;
            // 
            // d12
            // 
            this.d12.HeaderText = "12";
            this.d12.Name = "d12";
            this.d12.ReadOnly = true;
            this.d12.Width = 25;
            // 
            // d13
            // 
            this.d13.HeaderText = "13";
            this.d13.Name = "d13";
            this.d13.ReadOnly = true;
            this.d13.Width = 25;
            // 
            // d14
            // 
            this.d14.HeaderText = "14";
            this.d14.Name = "d14";
            this.d14.ReadOnly = true;
            this.d14.Width = 25;
            // 
            // d15
            // 
            this.d15.HeaderText = "15";
            this.d15.Name = "d15";
            this.d15.ReadOnly = true;
            this.d15.Width = 25;
            // 
            // d16
            // 
            this.d16.HeaderText = "16";
            this.d16.Name = "d16";
            this.d16.ReadOnly = true;
            this.d16.Width = 25;
            // 
            // d17
            // 
            this.d17.HeaderText = "17";
            this.d17.Name = "d17";
            this.d17.ReadOnly = true;
            this.d17.Width = 25;
            // 
            // d18
            // 
            this.d18.HeaderText = "18";
            this.d18.Name = "d18";
            this.d18.ReadOnly = true;
            this.d18.Width = 25;
            // 
            // d19
            // 
            this.d19.HeaderText = "19";
            this.d19.Name = "d19";
            this.d19.ReadOnly = true;
            this.d19.Width = 25;
            // 
            // d20
            // 
            this.d20.HeaderText = "20";
            this.d20.Name = "d20";
            this.d20.ReadOnly = true;
            this.d20.Width = 25;
            // 
            // d21
            // 
            this.d21.HeaderText = "21";
            this.d21.Name = "d21";
            this.d21.ReadOnly = true;
            this.d21.Width = 25;
            // 
            // d22
            // 
            this.d22.HeaderText = "22";
            this.d22.Name = "d22";
            this.d22.ReadOnly = true;
            this.d22.Width = 25;
            // 
            // d23
            // 
            this.d23.HeaderText = "23";
            this.d23.Name = "d23";
            this.d23.ReadOnly = true;
            this.d23.Width = 25;
            // 
            // d24
            // 
            this.d24.HeaderText = "24";
            this.d24.Name = "d24";
            this.d24.ReadOnly = true;
            this.d24.Width = 25;
            // 
            // d25
            // 
            this.d25.HeaderText = "25";
            this.d25.Name = "d25";
            this.d25.ReadOnly = true;
            this.d25.Width = 25;
            // 
            // d26
            // 
            this.d26.HeaderText = "26";
            this.d26.Name = "d26";
            this.d26.ReadOnly = true;
            this.d26.Width = 25;
            // 
            // d27
            // 
            this.d27.HeaderText = "27";
            this.d27.Name = "d27";
            this.d27.ReadOnly = true;
            this.d27.Width = 25;
            // 
            // d28
            // 
            this.d28.HeaderText = "28";
            this.d28.Name = "d28";
            this.d28.ReadOnly = true;
            this.d28.Width = 25;
            // 
            // d29
            // 
            this.d29.HeaderText = "29";
            this.d29.Name = "d29";
            this.d29.ReadOnly = true;
            this.d29.Width = 25;
            // 
            // d30
            // 
            this.d30.HeaderText = "30";
            this.d30.Name = "d30";
            this.d30.ReadOnly = true;
            this.d30.Width = 25;
            // 
            // d31
            // 
            this.d31.HeaderText = "31";
            this.d31.Name = "d31";
            this.d31.ReadOnly = true;
            this.d31.Width = 25;
            // 
            // dataGridViewShift
            // 
            this.dataGridViewShift.AllowUserToAddRows = false;
            this.dataGridViewShift.AllowUserToDeleteRows = false;
            this.dataGridViewShift.AllowUserToResizeRows = false;
            this.dataGridViewShift.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewShift.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewShift.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_type,
            this.short_name,
            this.type_name});
            this.dataGridViewShift.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewShift.Name = "dataGridViewShift";
            this.dataGridViewShift.ReadOnly = true;
            this.dataGridViewShift.RowHeadersVisible = false;
            this.dataGridViewShift.Size = new System.Drawing.Size(412, 114);
            this.dataGridViewShift.TabIndex = 5;
            // 
            // id_type
            // 
            this.id_type.DataPropertyName = "id";
            this.id_type.HeaderText = "id_type";
            this.id_type.Name = "id_type";
            this.id_type.ReadOnly = true;
            this.id_type.Visible = false;
            // 
            // short_name
            // 
            this.short_name.DataPropertyName = "short_name";
            this.short_name.HeaderText = "short_name";
            this.short_name.Name = "short_name";
            this.short_name.ReadOnly = true;
            this.short_name.Visible = false;
            // 
            // type_name
            // 
            this.type_name.DataPropertyName = "name";
            this.type_name.HeaderText = "Смена";
            this.type_name.Name = "type_name";
            this.type_name.ReadOnly = true;
            this.type_name.Width = 250;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(12, 12);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView_sheduleEmp);
            this.splitContainer1.Size = new System.Drawing.Size(988, 270);
            this.splitContainer1.SplitterDistance = 126;
            this.splitContainer1.TabIndex = 6;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.dataGridView_addEmp);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dataGridViewShift);
            this.splitContainer2.Size = new System.Drawing.Size(982, 120);
            this.splitContainer2.SplitterDistance = 560;
            this.splitContainer2.TabIndex = 0;
            // 
            // Form_shedule_add_emp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 323);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_shedule_add_emp";
            this.Text = "Добавление сотрудника";
            this.Load += new System.EventHandler(this.Form_shedule_add_emp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_addEmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_sheduleEmp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShift)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView_addEmp;
        private System.Windows.Forms.DataGridView dataGridView_sheduleEmp;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_emp;
        private System.Windows.Forms.DataGridViewTextBoxColumn dep;
        private System.Windows.Forms.DataGridViewTextBoxColumn name_ru;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn d1;
        private System.Windows.Forms.DataGridViewTextBoxColumn d2;
        private System.Windows.Forms.DataGridViewTextBoxColumn d3;
        private System.Windows.Forms.DataGridViewTextBoxColumn d4;
        private System.Windows.Forms.DataGridViewTextBoxColumn d5;
        private System.Windows.Forms.DataGridViewTextBoxColumn d6;
        private System.Windows.Forms.DataGridViewTextBoxColumn d7;
        private System.Windows.Forms.DataGridViewTextBoxColumn d8;
        private System.Windows.Forms.DataGridViewTextBoxColumn d9;
        private System.Windows.Forms.DataGridViewTextBoxColumn d10;
        private System.Windows.Forms.DataGridViewTextBoxColumn d11;
        private System.Windows.Forms.DataGridViewTextBoxColumn d12;
        private System.Windows.Forms.DataGridViewTextBoxColumn d13;
        private System.Windows.Forms.DataGridViewTextBoxColumn d14;
        private System.Windows.Forms.DataGridViewTextBoxColumn d15;
        private System.Windows.Forms.DataGridViewTextBoxColumn d16;
        private System.Windows.Forms.DataGridViewTextBoxColumn d17;
        private System.Windows.Forms.DataGridViewTextBoxColumn d18;
        private System.Windows.Forms.DataGridViewTextBoxColumn d19;
        private System.Windows.Forms.DataGridViewTextBoxColumn d20;
        private System.Windows.Forms.DataGridViewTextBoxColumn d21;
        private System.Windows.Forms.DataGridViewTextBoxColumn d22;
        private System.Windows.Forms.DataGridViewTextBoxColumn d23;
        private System.Windows.Forms.DataGridViewTextBoxColumn d24;
        private System.Windows.Forms.DataGridViewTextBoxColumn d25;
        private System.Windows.Forms.DataGridViewTextBoxColumn d26;
        private System.Windows.Forms.DataGridViewTextBoxColumn d27;
        private System.Windows.Forms.DataGridViewTextBoxColumn d28;
        private System.Windows.Forms.DataGridViewTextBoxColumn d29;
        private System.Windows.Forms.DataGridViewTextBoxColumn d30;
        private System.Windows.Forms.DataGridViewTextBoxColumn d31;
        private System.Windows.Forms.DataGridView dataGridViewShift;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_type;
        private System.Windows.Forms.DataGridViewTextBoxColumn short_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn type_name;
    }
}