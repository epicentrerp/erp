﻿namespace Shedule
{
    partial class Form_shedule_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewShedule = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.status_nach = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.doc_numder_full = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shedule_year = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name_ru = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date_create = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time_create = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.утвердитьНачальникомToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.начToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.дирToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.дирСнятьПроведениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.печатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.печатьТабельToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.созданиеИзгToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newShedule = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxDep = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownMonthBegin = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownYearBegin = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMonthEnd = new System.Windows.Forms.NumericUpDown();
            this.button3 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.историяДокументаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShedule)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMonthBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownYearBegin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMonthEnd)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewShedule
            // 
            this.dataGridViewShedule.AllowUserToAddRows = false;
            this.dataGridViewShedule.AllowUserToDeleteRows = false;
            this.dataGridViewShedule.AllowUserToResizeRows = false;
            this.dataGridViewShedule.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewShedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewShedule.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.status,
            this.status_nach,
            this.dep,
            this.doc_numder_full,
            this.shedule_year,
            this.name_ru,
            this.date_create,
            this.time_create});
            this.dataGridViewShedule.ContextMenuStrip = this.contextMenuStrip1;
            this.dataGridViewShedule.Location = new System.Drawing.Point(12, 41);
            this.dataGridViewShedule.MultiSelect = false;
            this.dataGridViewShedule.Name = "dataGridViewShedule";
            this.dataGridViewShedule.ReadOnly = true;
            this.dataGridViewShedule.RowHeadersVisible = false;
            this.dataGridViewShedule.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridViewShedule.Size = new System.Drawing.Size(849, 308);
            this.dataGridViewShedule.TabIndex = 0;
            this.dataGridViewShedule.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // status
            // 
            this.status.DataPropertyName = "status";
            this.status.HeaderText = "Дир";
            this.status.Name = "status";
            this.status.ReadOnly = true;
            this.status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.status.Width = 50;
            // 
            // status_nach
            // 
            this.status_nach.DataPropertyName = "status_nach";
            this.status_nach.HeaderText = "Нач";
            this.status_nach.Name = "status_nach";
            this.status_nach.ReadOnly = true;
            this.status_nach.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.status_nach.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.status_nach.Width = 50;
            // 
            // dep
            // 
            this.dep.DataPropertyName = "dep";
            this.dep.HeaderText = "Отдел";
            this.dep.Name = "dep";
            this.dep.ReadOnly = true;
            // 
            // doc_numder_full
            // 
            this.doc_numder_full.DataPropertyName = "doc_numder_full";
            this.doc_numder_full.HeaderText = "Название";
            this.doc_numder_full.Name = "doc_numder_full";
            this.doc_numder_full.ReadOnly = true;
            // 
            // shedule_year
            // 
            this.shedule_year.DataPropertyName = "shedule_date";
            this.shedule_year.HeaderText = "Период";
            this.shedule_year.Name = "shedule_year";
            this.shedule_year.ReadOnly = true;
            // 
            // name_ru
            // 
            this.name_ru.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.name_ru.DataPropertyName = "name_ru";
            this.name_ru.HeaderText = "Создал";
            this.name_ru.Name = "name_ru";
            this.name_ru.ReadOnly = true;
            // 
            // date_create
            // 
            this.date_create.DataPropertyName = "date_create";
            this.date_create.HeaderText = "Дата";
            this.date_create.Name = "date_create";
            this.date_create.ReadOnly = true;
            // 
            // time_create
            // 
            this.time_create.DataPropertyName = "time_create";
            this.time_create.HeaderText = "Время";
            this.time_create.Name = "time_create";
            this.time_create.ReadOnly = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.утвердитьНачальникомToolStripMenuItem,
            this.начToolStripMenuItem,
            this.toolStripSeparator1,
            this.дирToolStripMenuItem,
            this.дирСнятьПроведениеToolStripMenuItem,
            this.toolStripSeparator2,
            this.печатьToolStripMenuItem,
            this.печатьТабельToolStripMenuItem,
            this.toolStripSeparator3,
            this.созданиеИзгToolStripMenuItem,
            this.историяДокументаToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(261, 220);
            // 
            // утвердитьНачальникомToolStripMenuItem
            // 
            this.утвердитьНачальникомToolStripMenuItem.Enabled = false;
            this.утвердитьНачальникомToolStripMenuItem.Name = "утвердитьНачальникомToolStripMenuItem";
            this.утвердитьНачальникомToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.утвердитьНачальникомToolStripMenuItem.Text = "Нач Провести";
            this.утвердитьНачальникомToolStripMenuItem.Click += new System.EventHandler(this.утвердитьНачальникомToolStripMenuItem_Click);
            // 
            // начToolStripMenuItem
            // 
            this.начToolStripMenuItem.Enabled = false;
            this.начToolStripMenuItem.Name = "начToolStripMenuItem";
            this.начToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.начToolStripMenuItem.Text = "Нач Снять проведение";
            this.начToolStripMenuItem.Click += new System.EventHandler(this.начToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(257, 6);
            this.toolStripSeparator1.Click += new System.EventHandler(this.toolStripSeparator1_Click);
            // 
            // дирToolStripMenuItem
            // 
            this.дирToolStripMenuItem.Enabled = false;
            this.дирToolStripMenuItem.Name = "дирToolStripMenuItem";
            this.дирToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.дирToolStripMenuItem.Text = "Дир Провести";
            this.дирToolStripMenuItem.Click += new System.EventHandler(this.дирToolStripMenuItem_Click);
            // 
            // дирСнятьПроведениеToolStripMenuItem
            // 
            this.дирСнятьПроведениеToolStripMenuItem.Enabled = false;
            this.дирСнятьПроведениеToolStripMenuItem.Name = "дирСнятьПроведениеToolStripMenuItem";
            this.дирСнятьПроведениеToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.дирСнятьПроведениеToolStripMenuItem.Text = "Дир Снять проведение";
            this.дирСнятьПроведениеToolStripMenuItem.Click += new System.EventHandler(this.дирСнятьПроведениеToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(257, 6);
            // 
            // печатьToolStripMenuItem
            // 
            this.печатьToolStripMenuItem.Name = "печатьToolStripMenuItem";
            this.печатьToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.печатьToolStripMenuItem.Text = "Печать";
            this.печатьToolStripMenuItem.Click += new System.EventHandler(this.печатьToolStripMenuItem_Click);
            // 
            // печатьТабельToolStripMenuItem
            // 
            this.печатьТабельToolStripMenuItem.Name = "печатьТабельToolStripMenuItem";
            this.печатьТабельToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.печатьТабельToolStripMenuItem.Text = "Печать табель";
            this.печатьТабельToolStripMenuItem.Click += new System.EventHandler(this.печатьТабельToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(257, 6);
            // 
            // созданиеИзгToolStripMenuItem
            // 
            this.созданиеИзгToolStripMenuItem.Name = "созданиеИзгToolStripMenuItem";
            this.созданиеИзгToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.созданиеИзгToolStripMenuItem.Text = "Создание изменения графика Изг";
            this.созданиеИзгToolStripMenuItem.Click += new System.EventHandler(this.созданиеИзгToolStripMenuItem_Click);
            // 
            // newShedule
            // 
            this.newShedule.Location = new System.Drawing.Point(12, 12);
            this.newShedule.Name = "newShedule";
            this.newShedule.Size = new System.Drawing.Size(75, 23);
            this.newShedule.TabIndex = 1;
            this.newShedule.Text = "Новое";
            this.newShedule.UseVisualStyleBackColor = true;
            this.newShedule.Click += new System.EventHandler(this.newShedule_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(299, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = ".";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // textBoxDep
            // 
            this.textBoxDep.Enabled = false;
            this.textBoxDep.Location = new System.Drawing.Point(193, 14);
            this.textBoxDep.Name = "textBoxDep";
            this.textBoxDep.Size = new System.Drawing.Size(100, 20);
            this.textBoxDep.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(334, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(38, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Все";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(152, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Отдел";
            // 
            // numericUpDownMonthBegin
            // 
            this.numericUpDownMonthBegin.Location = new System.Drawing.Point(474, 14);
            this.numericUpDownMonthBegin.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numericUpDownMonthBegin.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMonthBegin.Name = "numericUpDownMonthBegin";
            this.numericUpDownMonthBegin.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownMonthBegin.TabIndex = 8;
            this.numericUpDownMonthBegin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDownYearBegin
            // 
            this.numericUpDownYearBegin.Location = new System.Drawing.Point(576, 14);
            this.numericUpDownYearBegin.Maximum = new decimal(new int[] {
            2999,
            0,
            0,
            0});
            this.numericUpDownYearBegin.Minimum = new decimal(new int[] {
            2016,
            0,
            0,
            0});
            this.numericUpDownYearBegin.Name = "numericUpDownYearBegin";
            this.numericUpDownYearBegin.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownYearBegin.TabIndex = 9;
            this.numericUpDownYearBegin.Value = new decimal(new int[] {
            2016,
            0,
            0,
            0});
            // 
            // numericUpDownMonthEnd
            // 
            this.numericUpDownMonthEnd.Location = new System.Drawing.Point(525, 14);
            this.numericUpDownMonthEnd.Maximum = new decimal(new int[] {
            12,
            0,
            0,
            0});
            this.numericUpDownMonthEnd.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMonthEnd.Name = "numericUpDownMonthEnd";
            this.numericUpDownMonthEnd.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownMonthEnd.TabIndex = 10;
            this.numericUpDownMonthEnd.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(645, 11);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(36, 23);
            this.button3.TabIndex = 12;
            this.button3.Text = "Ок";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(410, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Диапазон";
            // 
            // историяДокументаToolStripMenuItem
            // 
            this.историяДокументаToolStripMenuItem.Name = "историяДокументаToolStripMenuItem";
            this.историяДокументаToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.историяДокументаToolStripMenuItem.Text = "История документа";
            this.историяДокументаToolStripMenuItem.Click += new System.EventHandler(this.историяДокументаToolStripMenuItem_Click);
            // 
            // Form_shedule_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 361);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.numericUpDownMonthEnd);
            this.Controls.Add(this.numericUpDownYearBegin);
            this.Controls.Add(this.numericUpDownMonthBegin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBoxDep);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.newShedule);
            this.Controls.Add(this.dataGridViewShedule);
            this.Name = "Form_shedule_main";
            this.Text = "Расписания";
            this.Load += new System.EventHandler(this.Form_shedule_main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewShedule)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMonthBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownYearBegin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMonthEnd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewShedule;
        private System.Windows.Forms.Button newShedule;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem утвердитьНачальникомToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem начToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem дирToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem печатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem дирСнятьПроведениеToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewCheckBoxColumn status;
        private System.Windows.Forms.DataGridViewCheckBoxColumn status_nach;
        private System.Windows.Forms.DataGridViewTextBoxColumn dep;
        private System.Windows.Forms.DataGridViewTextBoxColumn doc_numder_full;
        private System.Windows.Forms.DataGridViewTextBoxColumn shedule_year;
        private System.Windows.Forms.DataGridViewTextBoxColumn name_ru;
        private System.Windows.Forms.DataGridViewTextBoxColumn date_create;
        private System.Windows.Forms.DataGridViewTextBoxColumn time_create;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxDep;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownMonthBegin;
        private System.Windows.Forms.NumericUpDown numericUpDownYearBegin;
        private System.Windows.Forms.NumericUpDown numericUpDownMonthEnd;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStripMenuItem созданиеИзгToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem печатьТабельToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem историяДокументаToolStripMenuItem;
    }
}