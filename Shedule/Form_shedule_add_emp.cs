﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Shedule
{
    public partial class Form_shedule_add_emp : Form
    {
        private int id_doc;

        // дописать функцию записи смены для конкретного отдела
        // дописать функцию получения количества дней в месяце.
        private int[] shiftArr = new int[31];
        private string nameShift;
        private int idShift;

        public Form_shedule_add_emp(int id_doc)
        {
            InitializeComponent();
            this.id_doc = id_doc;
        }

        private void dataGridView_addEmp_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            for (int i = 0; i < 31; i++)
            {
                shiftArr[i] = idShift;
            }
            dataGridView_sheduleEmp.Rows.Clear();
            dataGridView_sheduleEmp.Rows.Add(dataGridView_addEmp[0, dataGridView_addEmp.CurrentCell.RowIndex].Value.ToString(), 
                dataGridView_addEmp[1, dataGridView_addEmp.CurrentCell.RowIndex].Value.ToString()
                , nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift, nameShift);
        }

        private void Form_shedule_add_emp_Load(object sender, EventArgs e)
        {
            // заполнение массива для отображения нового распсиния
            SqlConnection sqlConnS = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmdS = new SqlCommand();
            SqlDataAdapter dataAdapterS = new SqlDataAdapter();
            DataSet dataSetS = new DataSet();
            string shiftS = "SELECT TOP 1 [id],[shedule_shift_type].[name]"
                            + " FROM [shedule_shift_type] inner join [shedule_shift_type_dep]"
                            + " ON [shedule_shift_type].id = [shedule_shift_type_dep].[id_shift] "
                            + " WHERE [shedule_shift_type_dep].dep = (SELECT dep FROM docs WHERE id = @id_doc) ORDER BY name";

            sqlCmdS.Parameters.Clear();
            sqlCmdS.CommandText = shiftS;
            sqlCmdS.Connection = sqlConnS;
            sqlCmdS.Parameters.AddWithValue("@id_doc", id_doc);

            sqlConnS.Open();
            using (SqlDataReader dr = sqlCmdS.ExecuteReader())
            {
                while (dr.Read())
                {
                    idShift = (dr.GetInt32(dr.GetOrdinal("id")));
                    nameShift = (Convert.ToString(dr.GetString(dr.GetOrdinal("name"))));
                }
            }
            sqlConnS.Close();


            for (int i = 0; i < 31; i++)
            {
                shiftArr[i] = idShift;
            }

            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd1 = new SqlCommand();
            SqlDataAdapter dataAdapter1 = new SqlDataAdapter();
            DataSet dataSet = new DataSet();

            SqlCommand sqlCmd = new SqlCommand("shedule_emp_not_in", sqlConn);

            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@input_shedule", id_doc); 

            sqlConn.Open();
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = sqlCmd;
                DataSet ds = new DataSet();
                dataAdapter.Fill(ds);
                dataGridView_addEmp.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();

            string shift = "SELECT [id], name as short_name, ([shedule_shift_type].[name]+' '+comment+' '+ISNULL(Convert(varchar,[shift_start]),'')+' '+ISNULL(Convert(varchar,[shift_end]),'')) as [name] "
                                + " FROM [shedule_shift_type] inner join [shedule_shift_type_dep]"
                                + " ON [shedule_shift_type].id = [shedule_shift_type_dep].[id_shift] "
                                + " WHERE [shedule_shift_type_dep].dep = (SELECT dep FROM docs WHERE id = @id_doc) ORDER BY id"; 
            DataSet dataSet1 = new DataSet();
            sqlCmd1.Parameters.Clear();
            sqlCmd1.CommandText = shift;
            sqlCmd1.Connection = sqlConn;
            sqlCmd1.Parameters.AddWithValue("@id_doc", id_doc);

            sqlConn.Open();
            dataAdapter1.SelectCommand = sqlCmd1;
            dataAdapter1.Fill(dataSet1);
            dataGridViewShift.DataSource = dataSet1.Tables[0].DefaultView;
            sqlConn.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView_sheduleEmp_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // запрещаем редактировать первую строку таблицы, первые две колонки где хранится фио и айди, плюс последнюю строку количества выходных
            if (dataGridView_sheduleEmp.CurrentCell.ColumnIndex > 1 )
            {
                shiftArr[dataGridView_sheduleEmp.CurrentCell.ColumnIndex - 2] = Convert.ToInt32(dataGridViewShift[0, dataGridViewShift.CurrentCell.RowIndex].Value.ToString());
                dataGridView_sheduleEmp[dataGridView_sheduleEmp.CurrentCell.ColumnIndex, 0].Value = dataGridViewShift[1,dataGridViewShift.CurrentCell.RowIndex].Value.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(ProSetting.ConnetionString);
            try
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "INSERT INTO [shedule_shift] ([id_doc],[id_emp],[day],[id_shift])"
                                        +"VALUES (@id_doc, @id_emp, @day, @id_shift)";
                connection.Open();
                for (int i = 0; i < 31 ; i++)
                {
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("@id_doc", id_doc);
                    command.Parameters.AddWithValue("@id_emp", dataGridView_sheduleEmp[0, 0].Value.ToString());
                    command.Parameters.AddWithValue("@day", i + 1);
                    command.Parameters.AddWithValue("@id_shift", shiftArr[i].ToString());
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException)
            {
               
            }
            finally
            {
                connection.Close();
            }
            this.Close();
        }
    }
}
