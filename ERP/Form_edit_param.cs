﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP
{
    public partial class Form_edit_param : Form
    {

        private string param, value;

        public Form_edit_param(string param, string value)
        {
            InitializeComponent();
            this.param = param;
            this.value = value;
        }

        private void Form_edit_param_Load(object sender, EventArgs e)
        {
            if (param != "")
            {
                textParam.Text = param;
                textParam.Enabled = false;
                textValue.Text = value;
            }
        }

        private void ok_Click(object sender, EventArgs e)
        {
            if (param == "")
            {
                if (textValue.Text != "")
                {
                    this.syssetingTableAdapter1.InsertQuery_param(textParam.Text, textValue.Text);
                    this.Close();
                    // написать функцию сохранения изменения
                }
            }
            else
            {
                if (textParam.Text!= "" && textValue.Text != "")
                {
                    this.syssetingTableAdapter1.UpdateQuery_param(textValue.Text, textParam.Text);
                    this.Close();
                    // написать функцию добавления нового 
                }
            }
            this.Close();
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
