﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP
{
    public partial class UserControlPeriod : UserControl
    {
        public UserControlPeriod()
        {
            InitializeComponent();
        }

        private void UserControlPeriod_Load(object sender, EventArgs e)
        {
            dateTimePickerBegin.Value = DateTime.Today;
            dateTimePickerEnd.Value = DateTime.Today;
        }

        private void dateTimePickerEnd_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
