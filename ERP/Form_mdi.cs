﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Data.SqlClient;
using Shedule;
using SystemCore;
using Report;

namespace ERP
{

    public partial class Form_mdi : Form
    {

        private string versionCurr;
        private string versionNew;
        private string idUser ;
        private int exitUn;

        public Form_mdi()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] param = null;
            string[] ver = null;
            //обновление
            try
            {
                //System.Threading.Thread.Sleep(000);
                param = File.ReadAllLines(@"param.txt");
                idUser = param[0];
                string[] lines = { "" };

                File.WriteAllLines(@"param.txt", lines);
            }
            catch (Exception)
            {
                Application.Exit();
            }

            try
            {
                ver = File.ReadAllLines(@"version.txt");
                versionCurr = ver[0];
                toolStripStatusLabel1.Text = toolStripStatusLabel1.Text + versionCurr;
            }
            catch (IOException)
            {
                MessageBox.Show("Ошибка чтения файлов version.");
            }

            // защита от открытия с неправильными парамтерами,. дописать проверку на айди
            if (idUser == "")
            {
                Application.Exit();
            }
            else
            {
                ProSetting.ConnetionString = Properties.Settings.Default.baluConnectionString;
                ProSetting.User = Convert.ToInt32(idUser);

                using (var sqlConn = new SqlConnection(ProSetting.ConnetionString))
                {
                    var sqlCmd = new SqlCommand("SELECT [id_access] FROM [users] WHERE [id] = @id", sqlConn);
                    sqlCmd.CommandType = CommandType.Text;

                    sqlCmd.Parameters.AddWithValue("@id", ProSetting.User); // ели значение писаное из базы то через value

                    sqlConn.Open();
                    using (SqlDataReader dr = sqlCmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ProSetting.Access = dr.GetInt32(dr.GetOrdinal("id_access"));
                        }
                    }
                }

                // вывод фио сотрудника в нижнюю часть программы
                nameOfUsers();
                accessLevel();
                
                // присвоить параметры для дллки шедул
                Shedule.ProSetting.ConnetionString = ProSetting.ConnetionString;
                Shedule.ProSetting.User = ProSetting.User;
                Shedule.ProSetting.Access = ProSetting.Access;
                // присвоить параметры для дллки systemcore
                Report.ProSetting.ConnetionString = ProSetting.ConnetionString;
                Report.ProSetting.User = ProSetting.User;
                Report.ProSetting.Access = ProSetting.Access;
                // присвоить параметры для дллки systemcore
                SystemCore.ProSetting.ConnetionString = ProSetting.ConnetionString;
                SystemCore.ProSetting.User = ProSetting.User;
                SystemCore.ProSetting.Access = ProSetting.Access;
                // присвоить параметры для дллки systemcore
                Journal.ProSetting.ConnetionString = ProSetting.ConnetionString;
                Journal.ProSetting.User = ProSetting.User;
                Journal.ProSetting.Access = ProSetting.Access;
            }
        }

        private void отчетыToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void перерывыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_rep_brake_dinner brakeReport = new Form_rep_brake_dinner();
            brakeReport.MdiParent = this;
            brakeReport.Show();
        }

        private void текущееРасписаниеToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void сотрудникиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_emp_main brakeReport = new Form_emp_main();
            brakeReport.MdiParent = this;
            brakeReport.Show();
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            //MessageBox.Show("Лет зе файт бигинс!");
            try
            {
                versionNew = this.updateTableAdapter.GetDataBy_version("erp")[0][0].ToString();
            }
            catch (SqlException)
            {
                versionNew = versionCurr;
            }

        }

        private void питаниеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_rep_dinner_count dinner_rep = new Form_rep_dinner_count();
            dinner_rep.MdiParent = this;
            dinner_rep.Show();
        }
        
        private void текущееРасписаниеToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Form_shedule_main sheduleMain = new Form_shedule_main();
            sheduleMain.MdiParent = this;
            sheduleMain.Show();
        }

        private void сменыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Вызвать смены
            Form_sys_shift_type shedule_shift_type = new Form_sys_shift_type();
            shedule_shift_type.MdiParent = this;
            shedule_shift_type.Show();
        }

        private void ошибочныеСканированияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_error_logs errorLogs = new Form_error_logs();
            errorLogs.MdiParent = this;
            errorLogs.Show();
        }

        private void Form_mdi_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (idUser != "")
            {
                DialogResult dialogResult = new DialogResult();
                if (exitUn == 0)
                {
                    dialogResult = MessageBox.Show("Вы действительно хотите закрыть программу?", "", MessageBoxButtons.YesNo);
                }
                else 
                {
                    dialogResult = DialogResult.Yes;
                }

                if (dialogResult == DialogResult.Yes)
                {
                    SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                    try
                    {
                        SqlCommand command = sqlConn.CreateCommand();
                        command.CommandText = "UPDATE [users] SET [status_connect] = 0 ,comp = null, data_con = null WHERE id = @id_user";
                        sqlConn.Open();
                        command.Parameters.AddWithValue("@id_user", idUser);
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("Ошибка отключени пользователя.");
                    }
                    finally
                    {
                        sqlConn.Close();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void системныеНастройкиToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form_system_setting sysSetting = new Form_system_setting();
            sysSetting.MdiParent = this;
            sysSetting.Show();
        }

        private void accessLevel()
        {
            if (ProSetting.Access != 0) // администратор
            {
                сервисToolStripMenuItem.Visible = false;
                сменыToolStripMenuItem.Visible = false;
                отделыToolStripMenuItem.Visible = false;
                секцииToolStripMenuItem.Visible = false;
            }

            if (ProSetting.Access != 5 && ProSetting.Access != 0) // администратор
            {
                заказПитанияToolStripMenuItem.Visible = false;
            }

            if (ProSetting.Access != 0 && ProSetting.Access != 4) // администратор
            {
                расписаниеToolStripMenuItem1.Visible = false;
            }
        }

        private void nameOfUsers()
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            var sqlCmd = new SqlCommand("SELECT [name_ru] FROM [employees] WHERE [id] = (SELECT id_emp FROM users WHERE id = @id)", sqlConn);
            sqlCmd.CommandType = CommandType.Text;

            sqlCmd.Parameters.AddWithValue("@id", ProSetting.User); // ели значение писаное из базы то через value

            sqlConn.Open();
            using (SqlDataReader dr = sqlCmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    toolStripStatusLabel2.Text = toolStripStatusLabel2.Text + dr.GetString(dr.GetOrdinal("name_ru"));
                }
            }

        }

        private void списокПользователейToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_sys_login loginERP = new Form_sys_login();
            loginERP.MdiParent = this;
            loginERP.Show();
        }

        private void активныеПользователиToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form_sys_active_users activeUsers = new Form_sys_active_users();
            activeUsers.MdiParent = this;
            activeUsers.Show();
        }

        private void блокированныеДокументыToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form_sys_doc_blocked doc_bloked = new Form_sys_doc_blocked();
            doc_bloked.MdiParent = this;
            doc_bloked.Show();
        }

        private void изменениеРасписанияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_shedule_chenge chengeShift = new Form_shedule_chenge();
            chengeShift.MdiParent = this;
            chengeShift.Show();
        }

        private void расписаниеToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form_rep_shedule_sb sheduleSb = new Form_rep_shedule_sb();
            sheduleSb.MdiParent = this;
            sheduleSb.Show();
        }

        private void заказПитанияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_dinner_order_main dinneOrder = new Form_dinner_order_main();
            dinneOrder.MdiParent = this;
            dinneOrder.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int checkedUsers = 0;
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            //sqlConn.ConnectionString = ;
            SqlCommand sqlCmd = new SqlCommand();
            string sql = "SELECT count(id) as id  FROM users_active WHERE [id] = @id and [comp] = @pc";

            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = sql;
            sqlCmd.Connection = sqlConn;
            sqlCmd.Parameters.AddWithValue("@id", ProSetting.User);
            sqlCmd.Parameters.AddWithValue("@pc", Environment.MachineName.ToString()); // ели значение писаное из базы то через value

            sqlConn.Open();
            using (SqlDataReader dr = sqlCmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    checkedUsers = dr.GetInt32(dr.GetOrdinal("id"));
                }
            }
            sqlConn.Close();

            if (checkedUsers == 0)
            {
                exitUn = 1;
                Application.Exit();
            }

        }

        private void отделыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_sys_dep sheduleSb = new Form_sys_dep();
            sheduleSb.MdiParent = this;
            sheduleSb.Show();
        }

        private void секцииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_sys_section section = new Form_sys_section();
            section.MdiParent = this;
            section.Show();
        }
    }
}
