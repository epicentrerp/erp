﻿namespace ERP
{
    partial class Form_emp_edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxDep = new System.Windows.Forms.TextBox();
            this.textBoxFIO = new System.Windows.Forms.TextBox();
            this.textBoxBar = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.employeesTableAdater = new ERP.baluDataSetTableAdapters.employeesTableAdapter();
            this.checkBoxDel = new System.Windows.Forms.CheckBox();
            this.checkBoxOffice = new System.Windows.Forms.CheckBox();
            this.buttonDep = new System.Windows.Forms.Button();
            this.buttonSection = new System.Windows.Forms.Button();
            this.textBoxSection = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxDep
            // 
            this.textBoxDep.Enabled = false;
            this.textBoxDep.Location = new System.Drawing.Point(114, 15);
            this.textBoxDep.Name = "textBoxDep";
            this.textBoxDep.Size = new System.Drawing.Size(53, 20);
            this.textBoxDep.TabIndex = 0;
            // 
            // textBoxFIO
            // 
            this.textBoxFIO.Location = new System.Drawing.Point(114, 73);
            this.textBoxFIO.Name = "textBoxFIO";
            this.textBoxFIO.Size = new System.Drawing.Size(293, 20);
            this.textBoxFIO.TabIndex = 1;
            // 
            // textBoxBar
            // 
            this.textBoxBar.Location = new System.Drawing.Point(114, 99);
            this.textBoxBar.Name = "textBoxBar";
            this.textBoxBar.Size = new System.Drawing.Size(293, 20);
            this.textBoxBar.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Отдел";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "ФИО";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Штрихкод";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(235, 140);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(316, 140);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Отмена";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // employeesTableAdater
            // 
            this.employeesTableAdater.ClearBeforeFill = true;
            // 
            // checkBoxDel
            // 
            this.checkBoxDel.AutoSize = true;
            this.checkBoxDel.Location = new System.Drawing.Point(15, 148);
            this.checkBoxDel.Name = "checkBoxDel";
            this.checkBoxDel.Size = new System.Drawing.Size(64, 17);
            this.checkBoxDel.TabIndex = 12;
            this.checkBoxDel.Text = "Уволен";
            this.checkBoxDel.UseVisualStyleBackColor = true;
            // 
            // checkBoxOffice
            // 
            this.checkBoxOffice.AutoSize = true;
            this.checkBoxOffice.Location = new System.Drawing.Point(224, 18);
            this.checkBoxOffice.Name = "checkBoxOffice";
            this.checkBoxOffice.Size = new System.Drawing.Size(54, 17);
            this.checkBoxOffice.TabIndex = 13;
            this.checkBoxOffice.Text = "Офис";
            this.checkBoxOffice.UseVisualStyleBackColor = true;
            // 
            // buttonDep
            // 
            this.buttonDep.Location = new System.Drawing.Point(173, 13);
            this.buttonDep.Name = "buttonDep";
            this.buttonDep.Size = new System.Drawing.Size(27, 23);
            this.buttonDep.TabIndex = 14;
            this.buttonDep.Text = ".";
            this.buttonDep.UseVisualStyleBackColor = true;
            this.buttonDep.Click += new System.EventHandler(this.buttonDep_Click);
            // 
            // buttonSection
            // 
            this.buttonSection.Location = new System.Drawing.Point(274, 40);
            this.buttonSection.Name = "buttonSection";
            this.buttonSection.Size = new System.Drawing.Size(27, 23);
            this.buttonSection.TabIndex = 15;
            this.buttonSection.Text = ".";
            this.buttonSection.UseVisualStyleBackColor = true;
            this.buttonSection.Click += new System.EventHandler(this.buttonSection_Click);
            // 
            // textBoxSection
            // 
            this.textBoxSection.Enabled = false;
            this.textBoxSection.Location = new System.Drawing.Point(114, 42);
            this.textBoxSection.Name = "textBoxSection";
            this.textBoxSection.Size = new System.Drawing.Size(154, 20);
            this.textBoxSection.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Секция";
            // 
            // Form_emp_edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 178);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxSection);
            this.Controls.Add(this.buttonSection);
            this.Controls.Add(this.buttonDep);
            this.Controls.Add(this.checkBoxOffice);
            this.Controls.Add(this.checkBoxDel);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxBar);
            this.Controls.Add(this.textBoxFIO);
            this.Controls.Add(this.textBoxDep);
            this.Name = "Form_emp_edit";
            this.Text = "Сотрдуник";
            this.Load += new System.EventHandler(this.Form_new_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxDep;
        private System.Windows.Forms.TextBox textBoxFIO;
        private System.Windows.Forms.TextBox textBoxBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private baluDataSetTableAdapters.employeesTableAdapter employeesTableAdater;
        private System.Windows.Forms.CheckBox checkBoxDel;
        private System.Windows.Forms.CheckBox checkBoxOffice;
        private System.Windows.Forms.Button buttonDep;
        private System.Windows.Forms.Button buttonSection;
        private System.Windows.Forms.TextBox textBoxSection;
        private System.Windows.Forms.Label label4;
    }
}