﻿namespace ERP
{
    partial class Form_system_setting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.paramDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.syssetingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.baluDataSet = new ERP.baluDataSet();
            this.newParam = new System.Windows.Forms.Button();
            this.editParam = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.delParam = new System.Windows.Forms.Button();
            this.syssetingTableAdapter = new ERP.baluDataSetTableAdapters.syssetingTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.syssetingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baluDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.paramDataGridViewTextBoxColumn,
            this.valueDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.syssetingBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 53);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(598, 210);
            this.dataGridView1.TabIndex = 0;
            // 
            // paramDataGridViewTextBoxColumn
            // 
            this.paramDataGridViewTextBoxColumn.DataPropertyName = "param";
            this.paramDataGridViewTextBoxColumn.HeaderText = "Параметр";
            this.paramDataGridViewTextBoxColumn.Name = "paramDataGridViewTextBoxColumn";
            this.paramDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // valueDataGridViewTextBoxColumn
            // 
            this.valueDataGridViewTextBoxColumn.DataPropertyName = "value";
            this.valueDataGridViewTextBoxColumn.HeaderText = "Значение";
            this.valueDataGridViewTextBoxColumn.Name = "valueDataGridViewTextBoxColumn";
            this.valueDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // syssetingBindingSource
            // 
            this.syssetingBindingSource.DataMember = "sysseting";
            this.syssetingBindingSource.DataSource = this.baluDataSet;
            // 
            // baluDataSet
            // 
            this.baluDataSet.DataSetName = "baluDataSet";
            this.baluDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // newParam
            // 
            this.newParam.Location = new System.Drawing.Point(12, 12);
            this.newParam.Name = "newParam";
            this.newParam.Size = new System.Drawing.Size(75, 23);
            this.newParam.TabIndex = 1;
            this.newParam.Text = "Новый";
            this.newParam.UseVisualStyleBackColor = true;
            this.newParam.Click += new System.EventHandler(this.newParam_Click);
            // 
            // editParam
            // 
            this.editParam.Location = new System.Drawing.Point(93, 12);
            this.editParam.Name = "editParam";
            this.editParam.Size = new System.Drawing.Size(75, 23);
            this.editParam.TabIndex = 2;
            this.editParam.Text = "Редактировать";
            this.editParam.UseVisualStyleBackColor = true;
            this.editParam.Click += new System.EventHandler(this.editParam_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(272, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Обновить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // delParam
            // 
            this.delParam.Location = new System.Drawing.Point(174, 12);
            this.delParam.Name = "delParam";
            this.delParam.Size = new System.Drawing.Size(75, 23);
            this.delParam.TabIndex = 4;
            this.delParam.Text = "Удалить";
            this.delParam.UseVisualStyleBackColor = true;
            this.delParam.Click += new System.EventHandler(this.delParam_Click);
            // 
            // syssetingTableAdapter
            // 
            this.syssetingTableAdapter.ClearBeforeFill = true;
            // 
            // Form_system_setting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 275);
            this.Controls.Add(this.delParam);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.editParam);
            this.Controls.Add(this.newParam);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form_system_setting";
            this.Text = "Form_system_setting";
            this.Load += new System.EventHandler(this.Form_system_setting_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.syssetingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baluDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private baluDataSet baluDataSet;
        private System.Windows.Forms.BindingSource syssetingBindingSource;
        private baluDataSetTableAdapters.syssetingTableAdapter syssetingTableAdapter;
        private System.Windows.Forms.Button newParam;
        private System.Windows.Forms.Button editParam;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button delParam;
        private System.Windows.Forms.DataGridViewTextBoxColumn paramDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;
    }
}