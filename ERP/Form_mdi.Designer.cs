﻿namespace ERP
{
    partial class Form_mdi
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.справочникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сотрудникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сменыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отделыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.журналыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.расписаниеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.текущиеРасписаниеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.изменениеРасписанияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.заказПитанияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.перерывыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.питаниеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.расписаниеToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.сервисToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ошибочныеСканированияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.системныеНастройкиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.блокированныеДокументыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.типыДокументовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.блокированныеДокументыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.активныеПользователиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.списокПользователейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.активныеПользователиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.updateTableAdapter = new ERP.baluDataSetTableAdapters.updateTableAdapter();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.секцииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.справочникиToolStripMenuItem,
            this.журналыToolStripMenuItem,
            this.отчетыToolStripMenuItem,
            this.сервисToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(967, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // справочникиToolStripMenuItem
            // 
            this.справочникиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сотрудникиToolStripMenuItem,
            this.сменыToolStripMenuItem,
            this.отделыToolStripMenuItem,
            this.секцииToolStripMenuItem});
            this.справочникиToolStripMenuItem.Name = "справочникиToolStripMenuItem";
            this.справочникиToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.справочникиToolStripMenuItem.Text = "Справочники";
            // 
            // сотрудникиToolStripMenuItem
            // 
            this.сотрудникиToolStripMenuItem.Name = "сотрудникиToolStripMenuItem";
            this.сотрудникиToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.сотрудникиToolStripMenuItem.Text = "Сотрудники";
            this.сотрудникиToolStripMenuItem.Click += new System.EventHandler(this.сотрудникиToolStripMenuItem_Click);
            // 
            // сменыToolStripMenuItem
            // 
            this.сменыToolStripMenuItem.Name = "сменыToolStripMenuItem";
            this.сменыToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.сменыToolStripMenuItem.Text = "Смены";
            this.сменыToolStripMenuItem.Click += new System.EventHandler(this.сменыToolStripMenuItem_Click);
            // 
            // отделыToolStripMenuItem
            // 
            this.отделыToolStripMenuItem.Name = "отделыToolStripMenuItem";
            this.отделыToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.отделыToolStripMenuItem.Text = "Отделы";
            this.отделыToolStripMenuItem.Click += new System.EventHandler(this.отделыToolStripMenuItem_Click);
            // 
            // журналыToolStripMenuItem
            // 
            this.журналыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.расписаниеToolStripMenuItem,
            this.заказПитанияToolStripMenuItem});
            this.журналыToolStripMenuItem.Name = "журналыToolStripMenuItem";
            this.журналыToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.журналыToolStripMenuItem.Text = "Журналы";
            // 
            // расписаниеToolStripMenuItem
            // 
            this.расписаниеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.текущиеРасписаниеToolStripMenuItem,
            this.изменениеРасписанияToolStripMenuItem});
            this.расписаниеToolStripMenuItem.Name = "расписаниеToolStripMenuItem";
            this.расписаниеToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.расписаниеToolStripMenuItem.Text = "Расписание";
            // 
            // текущиеРасписаниеToolStripMenuItem
            // 
            this.текущиеРасписаниеToolStripMenuItem.Name = "текущиеРасписаниеToolStripMenuItem";
            this.текущиеРасписаниеToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.текущиеРасписаниеToolStripMenuItem.Text = "Текущие расписание";
            this.текущиеРасписаниеToolStripMenuItem.Click += new System.EventHandler(this.текущееРасписаниеToolStripMenuItem_Click_1);
            // 
            // изменениеРасписанияToolStripMenuItem
            // 
            this.изменениеРасписанияToolStripMenuItem.Name = "изменениеРасписанияToolStripMenuItem";
            this.изменениеРасписанияToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.изменениеРасписанияToolStripMenuItem.Text = "Изменение расписания";
            this.изменениеРасписанияToolStripMenuItem.Click += new System.EventHandler(this.изменениеРасписанияToolStripMenuItem_Click);
            // 
            // заказПитанияToolStripMenuItem
            // 
            this.заказПитанияToolStripMenuItem.Name = "заказПитанияToolStripMenuItem";
            this.заказПитанияToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.заказПитанияToolStripMenuItem.Text = "Заказ питания";
            this.заказПитанияToolStripMenuItem.Click += new System.EventHandler(this.заказПитанияToolStripMenuItem_Click);
            // 
            // отчетыToolStripMenuItem
            // 
            this.отчетыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.перерывыToolStripMenuItem,
            this.питаниеToolStripMenuItem,
            this.расписаниеToolStripMenuItem1});
            this.отчетыToolStripMenuItem.Name = "отчетыToolStripMenuItem";
            this.отчетыToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.отчетыToolStripMenuItem.Text = "Отчеты";
            this.отчетыToolStripMenuItem.Click += new System.EventHandler(this.отчетыToolStripMenuItem_Click);
            // 
            // перерывыToolStripMenuItem
            // 
            this.перерывыToolStripMenuItem.Name = "перерывыToolStripMenuItem";
            this.перерывыToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.перерывыToolStripMenuItem.Text = "Перерывы";
            this.перерывыToolStripMenuItem.Click += new System.EventHandler(this.перерывыToolStripMenuItem_Click);
            // 
            // питаниеToolStripMenuItem
            // 
            this.питаниеToolStripMenuItem.Name = "питаниеToolStripMenuItem";
            this.питаниеToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.питаниеToolStripMenuItem.Text = "Питание";
            this.питаниеToolStripMenuItem.Click += new System.EventHandler(this.питаниеToolStripMenuItem_Click);
            // 
            // расписаниеToolStripMenuItem1
            // 
            this.расписаниеToolStripMenuItem1.Name = "расписаниеToolStripMenuItem1";
            this.расписаниеToolStripMenuItem1.Size = new System.Drawing.Size(139, 22);
            this.расписаниеToolStripMenuItem1.Text = "Расписание";
            this.расписаниеToolStripMenuItem1.Click += new System.EventHandler(this.расписаниеToolStripMenuItem1_Click);
            // 
            // сервисToolStripMenuItem
            // 
            this.сервисToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ошибочныеСканированияToolStripMenuItem,
            this.системныеНастройкиToolStripMenuItem1,
            this.блокированныеДокументыToolStripMenuItem,
            this.активныеПользователиToolStripMenuItem});
            this.сервисToolStripMenuItem.Name = "сервисToolStripMenuItem";
            this.сервисToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.сервисToolStripMenuItem.Text = "Сервис";
            // 
            // ошибочныеСканированияToolStripMenuItem
            // 
            this.ошибочныеСканированияToolStripMenuItem.Name = "ошибочныеСканированияToolStripMenuItem";
            this.ошибочныеСканированияToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.ошибочныеСканированияToolStripMenuItem.Text = "Ошибочные сканирования";
            this.ошибочныеСканированияToolStripMenuItem.Click += new System.EventHandler(this.ошибочныеСканированияToolStripMenuItem_Click);
            // 
            // системныеНастройкиToolStripMenuItem1
            // 
            this.системныеНастройкиToolStripMenuItem1.Name = "системныеНастройкиToolStripMenuItem1";
            this.системныеНастройкиToolStripMenuItem1.Size = new System.Drawing.Size(225, 22);
            this.системныеНастройкиToolStripMenuItem1.Text = "Системные настройки";
            this.системныеНастройкиToolStripMenuItem1.Click += new System.EventHandler(this.системныеНастройкиToolStripMenuItem1_Click);
            // 
            // блокированныеДокументыToolStripMenuItem
            // 
            this.блокированныеДокументыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.типыДокументовToolStripMenuItem,
            this.блокированныеДокументыToolStripMenuItem1});
            this.блокированныеДокументыToolStripMenuItem.Name = "блокированныеДокументыToolStripMenuItem";
            this.блокированныеДокументыToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.блокированныеДокументыToolStripMenuItem.Text = "Документы";
            // 
            // типыДокументовToolStripMenuItem
            // 
            this.типыДокументовToolStripMenuItem.Name = "типыДокументовToolStripMenuItem";
            this.типыДокументовToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.типыДокументовToolStripMenuItem.Text = "Типы документов";
            // 
            // блокированныеДокументыToolStripMenuItem1
            // 
            this.блокированныеДокументыToolStripMenuItem1.Name = "блокированныеДокументыToolStripMenuItem1";
            this.блокированныеДокументыToolStripMenuItem1.Size = new System.Drawing.Size(227, 22);
            this.блокированныеДокументыToolStripMenuItem1.Text = "Блокированные документы";
            this.блокированныеДокументыToolStripMenuItem1.Click += new System.EventHandler(this.блокированныеДокументыToolStripMenuItem1_Click);
            // 
            // активныеПользователиToolStripMenuItem
            // 
            this.активныеПользователиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.списокПользователейToolStripMenuItem,
            this.активныеПользователиToolStripMenuItem1});
            this.активныеПользователиToolStripMenuItem.Name = "активныеПользователиToolStripMenuItem";
            this.активныеПользователиToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.активныеПользователиToolStripMenuItem.Text = "Пользователи";
            // 
            // списокПользователейToolStripMenuItem
            // 
            this.списокПользователейToolStripMenuItem.Name = "списокПользователейToolStripMenuItem";
            this.списокПользователейToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.списокПользователейToolStripMenuItem.Text = "Список пользователей";
            this.списокПользователейToolStripMenuItem.Click += new System.EventHandler(this.списокПользователейToolStripMenuItem_Click);
            // 
            // активныеПользователиToolStripMenuItem1
            // 
            this.активныеПользователиToolStripMenuItem1.Name = "активныеПользователиToolStripMenuItem1";
            this.активныеПользователиToolStripMenuItem1.Size = new System.Drawing.Size(207, 22);
            this.активныеПользователиToolStripMenuItem1.Text = "Активные пользователи";
            this.активныеПользователиToolStripMenuItem1.Click += new System.EventHandler(this.активныеПользователиToolStripMenuItem1_Click);
            // 
            // updateTableAdapter
            // 
            this.updateTableAdapter.ClearBeforeFill = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 562);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(967, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(107, 17);
            this.toolStripStatusLabel1.Text = "Текущея версия - ";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(95, 17);
            this.toolStripStatusLabel2.Text = "Пользователь - ";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 30000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // секцииToolStripMenuItem
            // 
            this.секцииToolStripMenuItem.Name = "секцииToolStripMenuItem";
            this.секцииToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.секцииToolStripMenuItem.Text = "Секции";
            this.секцииToolStripMenuItem.Click += new System.EventHandler(this.секцииToolStripMenuItem_Click);
            // 
            // Form_mdi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 584);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form_mdi";
            this.Text = "ERP";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_mdi_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem журналыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справочникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem расписаниеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem перерывыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem текущиеРасписаниеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem изменениеРасписанияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сотрудникиToolStripMenuItem;
        private baluDataSetTableAdapters.updateTableAdapter updateTableAdapter;
        private System.Windows.Forms.ToolStripMenuItem питаниеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сменыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сервисToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ошибочныеСканированияToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem системныеНастройкиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem блокированныеДокументыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem активныеПользователиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem списокПользователейToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem активныеПользователиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem типыДокументовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem блокированныеДокументыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripMenuItem расписаниеToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem заказПитанияToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem отделыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem секцииToolStripMenuItem;
    }
}

