﻿namespace ERP
{
    partial class Form_error_logs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.depDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameruDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errornameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.errorlogsviewBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.baluDataSet = new ERP.baluDataSet();
            this.error_logs_viewTableAdapter = new ERP.baluDataSetTableAdapters.error_logs_viewTableAdapter();
            this.userControlPeriod1 = new ERP.UserControlPeriod();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorlogsviewBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.baluDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(249, 15);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Сформировать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.depDataGridViewTextBoxColumn,
            this.nameruDataGridViewTextBoxColumn,
            this.dateDataGridViewTextBoxColumn,
            this.timeDataGridViewTextBoxColumn,
            this.errornameDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.errorlogsviewBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 44);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(682, 212);
            this.dataGridView1.TabIndex = 2;
            // 
            // depDataGridViewTextBoxColumn
            // 
            this.depDataGridViewTextBoxColumn.DataPropertyName = "dep";
            this.depDataGridViewTextBoxColumn.HeaderText = "dep";
            this.depDataGridViewTextBoxColumn.Name = "depDataGridViewTextBoxColumn";
            this.depDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nameruDataGridViewTextBoxColumn
            // 
            this.nameruDataGridViewTextBoxColumn.DataPropertyName = "name_ru";
            this.nameruDataGridViewTextBoxColumn.HeaderText = "name_ru";
            this.nameruDataGridViewTextBoxColumn.Name = "nameruDataGridViewTextBoxColumn";
            this.nameruDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            this.dateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // timeDataGridViewTextBoxColumn
            // 
            this.timeDataGridViewTextBoxColumn.DataPropertyName = "time";
            this.timeDataGridViewTextBoxColumn.HeaderText = "time";
            this.timeDataGridViewTextBoxColumn.Name = "timeDataGridViewTextBoxColumn";
            this.timeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // errornameDataGridViewTextBoxColumn
            // 
            this.errornameDataGridViewTextBoxColumn.DataPropertyName = "error_name";
            this.errornameDataGridViewTextBoxColumn.HeaderText = "error_name";
            this.errornameDataGridViewTextBoxColumn.Name = "errornameDataGridViewTextBoxColumn";
            this.errornameDataGridViewTextBoxColumn.ReadOnly = true;
            this.errornameDataGridViewTextBoxColumn.Width = 200;
            // 
            // errorlogsviewBindingSource
            // 
            this.errorlogsviewBindingSource.DataMember = "error_logs_view";
            this.errorlogsviewBindingSource.DataSource = this.baluDataSet;
            // 
            // baluDataSet
            // 
            this.baluDataSet.DataSetName = "baluDataSet";
            this.baluDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // error_logs_viewTableAdapter
            // 
            this.error_logs_viewTableAdapter.ClearBeforeFill = true;
            // 
            // userControlPeriod1
            // 
            this.userControlPeriod1.Location = new System.Drawing.Point(12, 12);
            this.userControlPeriod1.Name = "userControlPeriod1";
            this.userControlPeriod1.Size = new System.Drawing.Size(231, 26);
            this.userControlPeriod1.TabIndex = 0;
            // 
            // Form_error_logs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 269);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.userControlPeriod1);
            this.Name = "Form_error_logs";
            this.Text = "Ошибки отметок в столовой";
            this.Load += new System.EventHandler(this.Form_error_logs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorlogsviewBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.baluDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UserControlPeriod userControlPeriod1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private baluDataSet baluDataSet;
        private System.Windows.Forms.BindingSource errorlogsviewBindingSource;
        private baluDataSetTableAdapters.error_logs_viewTableAdapter error_logs_viewTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn depDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameruDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn errornameDataGridViewTextBoxColumn;
    }
}