﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP
{
    public partial class Form_system_setting : Form
    {
        public Form_system_setting()
        {
            InitializeComponent();
        }

        private void Form_system_setting_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "baluDataSet.sysseting". При необходимости она может быть перемещена или удалена.
            this.syssetingTableAdapter.Fill(this.baluDataSet.sysseting);

        }

        private void editParam_Click(object sender, EventArgs e)
        {
            Form_edit_param editParam = new Form_edit_param(dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString(),
                dataGridView1[1, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            editParam.MdiParent = this.MdiParent;
            editParam.Show();
        }

        private void newParam_Click(object sender, EventArgs e)
        {
            Form_edit_param newParam = new Form_edit_param("", "");
            newParam.MdiParent = this.MdiParent;
            newParam.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.syssetingTableAdapter.Fill(this.baluDataSet.sysseting);
        }

        private void delParam_Click(object sender, EventArgs e)
        {
            this.syssetingTableAdapter.DeleteQuery_param(dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            this.syssetingTableAdapter.Fill(this.baluDataSet.sysseting);
        }
    }
}
