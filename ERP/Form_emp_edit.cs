﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Journal;

namespace ERP
{
    public partial class Form_emp_edit : Form
    {
        private string id, id_section;
        Form_dep depSelect;
        Form_section secSelect;

        public Form_emp_edit(string id)
        {
            this.id = id;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string barcode;
            barcode = textBoxBar.Text.Replace("р", "");
            barcode = barcode.Replace("h", "");
            barcode = barcode.Replace("р", "");
            barcode = barcode.Replace("H", "");

            int unicBarcode = Convert.ToInt32(this.employeesTableAdater.GetDataBy_unic_barcode(Convert.ToInt64(barcode))[0][0].ToString());


            if (id == "0")
            {
                if (unicBarcode == 0)
                {
                    try
                    {
                        // переписать создание пользователя
                        employeesTableAdater.InsertQuery_new_empl(textBoxFIO.Text, Convert.ToInt32(textBoxDep.Text), Convert.ToInt64(textBoxBar.Text), Convert.ToInt32(checkBoxOffice.Checked), Convert.ToInt32(id_section));
                        this.Close();
                    }
                    catch (Exception ex1)
                    {
                        MessageBox.Show("Не всё поля заполненны корректно.");
                    }
                }
                else
                {
                    MessageBox.Show("Сотрудник с таким бейджем уже внесен в базу!");
                }
            }
            else
            {
                try
                {
                    this.employeesTableAdater.UpdateQuery_barcode(textBoxFIO.Text, Convert.ToInt32(textBoxDep.Text), Convert.ToInt64(textBoxBar.Text), Convert.ToInt32(checkBoxOffice.Checked),
                         Convert.ToInt32(checkBoxDel.Checked), Convert.ToInt32(id_section), Convert.ToInt32(id));
                    this.Close();
                }
                catch (Exception ex2)
                {
                    MessageBox.Show("Не всё поля заполненны корректно.");
                }
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form_new_Load(object sender, EventArgs e)
        {
            if (id != "0")
            {
                SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
                SqlCommand sqlCmd = new SqlCommand();

                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Connection = sqlConn;
                string sql = "SELECT [employees].[id],[name_ru],[employees].[dep],[status_work],[barcode],[office],section.name,section.id as id_sec "
                            + " FROM [balu].[dbo].[employees] full outer join [balu].[dbo].[section] on [employees].section = [balu].[dbo].[section].id"
                            + " WHERE [employees].[id] = @id_emp";
                sqlCmd.CommandText = sql;
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Parameters.AddWithValue("@id_emp", id);

                sqlConn.Open();

                SqlDataReader dr = sqlCmd.ExecuteReader();
                {
                    while (dr.Read())
                    {
                        id_section = dr.GetInt32(dr.GetOrdinal("id_sec")).ToString();
                        textBoxDep.Text = dr.GetInt32(dr.GetOrdinal("dep")).ToString(); 
                        textBoxFIO.Text = dr.GetString(dr.GetOrdinal("name_ru"));
                        textBoxBar.Text = dr.GetInt64(dr.GetOrdinal("barcode")).ToString();
                        checkBoxOffice.Checked = Convert.ToBoolean(dr.GetInt32(dr.GetOrdinal("office")));
                        checkBoxDel.Checked = Convert.ToBoolean(dr.GetInt32(dr.GetOrdinal("status_work")));
                        textBoxSection.Text = dr.GetString(dr.GetOrdinal("name"));
                        // чтение данных из ридера 
                    }
                }
                sqlConn.Close();
            }
        }

        private void buttonDep_Click(object sender, EventArgs e)
        {
            if (depSelect == null || depSelect.IsDisposed)
            {
                depSelect = new Form_dep(ProSetting.User);
                depSelect.MdiParent = this.MdiParent;
                depSelect.FormClosing += (sender1, e1) =>
                {
                    if (depSelect.dep != null)
                    {
                        textBoxDep.Text = depSelect.dep;
                    }
                };
                depSelect.Show();
            }
            else
            {
                depSelect.Activate();
            }
        }

        private void buttonSection_Click(object sender, EventArgs e)
        {
            if (secSelect == null || secSelect.IsDisposed)
            {
                secSelect = new Form_section();
                secSelect.MdiParent = this.MdiParent;
                secSelect.FormClosing += (sender1, e1) =>
                {
                    if (secSelect.id_section != null)
                    {
                        id_section = secSelect.id_section;
                        textBoxSection.Text = secSelect.name_section;
                    }
                };
                secSelect.Show();
            }
            else
            {
                secSelect.Activate();
            }
        }
    }
}
