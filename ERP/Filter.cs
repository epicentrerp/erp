﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ERP
{
    class Filter
    {
        private int idEmp;
        private int dep;
        private string dateBegin;
        private string dateEnd;


        public int IdEmp
        {
            get { return this.idEmp; }
            set { this.idEmp = value; }
        }

        public int Dep
        {
            get { return this.dep; }
            set { this.dep = value; }
        }

        public string DateBegin
        {
            get { return this.dateBegin; }
            set { this.dateBegin = value; }
        }

        public string DateEnd
        {
            get { return this.dateEnd; }
            set { this.dateEnd = value; }
        }
    }
}
