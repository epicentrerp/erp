﻿namespace ERP
{
    partial class Form_edit_param
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textParam = new System.Windows.Forms.TextBox();
            this.textValue = new System.Windows.Forms.TextBox();
            this.ok = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.syssetingTableAdapter1 = new ERP.baluDataSetTableAdapters.syssetingTableAdapter();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Название";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Значение";
            // 
            // textParam
            // 
            this.textParam.Location = new System.Drawing.Point(12, 33);
            this.textParam.MaxLength = 20;
            this.textParam.Name = "textParam";
            this.textParam.Size = new System.Drawing.Size(258, 20);
            this.textParam.TabIndex = 2;
            // 
            // textValue
            // 
            this.textValue.Location = new System.Drawing.Point(12, 85);
            this.textValue.MaxLength = 30;
            this.textValue.Name = "textValue";
            this.textValue.Size = new System.Drawing.Size(258, 20);
            this.textValue.TabIndex = 3;
            // 
            // ok
            // 
            this.ok.Location = new System.Drawing.Point(114, 121);
            this.ok.Name = "ok";
            this.ok.Size = new System.Drawing.Size(75, 23);
            this.ok.TabIndex = 4;
            this.ok.Text = "Ок";
            this.ok.UseVisualStyleBackColor = true;
            this.ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(195, 121);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 5;
            this.cancel.Text = "Отмена";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // syssetingTableAdapter1
            // 
            this.syssetingTableAdapter1.ClearBeforeFill = true;
            // 
            // Form_edit_param
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(297, 156);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.ok);
            this.Controls.Add(this.textValue);
            this.Controls.Add(this.textParam);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form_edit_param";
            this.Text = "Form_edit_param";
            this.Load += new System.EventHandler(this.Form_edit_param_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textParam;
        private System.Windows.Forms.TextBox textValue;
        private System.Windows.Forms.Button ok;
        private System.Windows.Forms.Button cancel;
        private baluDataSetTableAdapters.syssetingTableAdapter syssetingTableAdapter1;
    }
}