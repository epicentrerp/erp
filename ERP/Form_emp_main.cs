﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ERP
{
    public partial class Form_emp_main : Form
    {
       
        public Form_emp_main()
        {
            InitializeComponent();
        }

        private void UpdateEmp()
        {
            //int pcName = Convert.ToInt32(Environment.MachineName.Substring(3, 3));
            // переписать на использование доступных отделов в уровне доступа

            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            string sql = "SELECT employees.id, name_ru, employees.dep, status_work, barcode, dinner, dinner_time, office , section.name as [section] FROM employees "
            + " inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
            + " on [employees].dep = dep_table.dep"
            + " left outer join section ON employees.section = section.id "
            + " Order By dep, name_ru ";
            SqlCommand sqlCmd = new SqlCommand(sql, sqlConn);

            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.Parameters.AddWithValue("@id_user", ProSetting.User);

            sqlConn.Open();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = sqlCmd;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            dataGridViewEmp.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateEMPGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "baluDataSet.employees". При необходимости она может быть перемещена или удалена.

        }
        private void button2_Click(object sender, EventArgs e)
        {
            Form_emp_edit empEdit = new Form_emp_edit("0");
            empEdit.MdiParent = this.MdiParent;
            empEdit.FormClosing += (sender1, e1) =>
            {
                UpdateEMPGrid();
            };
            empEdit.Show();
        }

        private int old_x, old_y;

        private void UpdateEMPGrid()
        {
            if (dataGridViewEmp.RowCount != 0)
            {
                old_y = dataGridViewEmp.CurrentCell.RowIndex;
                old_x = dataGridViewEmp.CurrentCell.ColumnIndex;
            }

            UpdateEmp();

            if (dataGridViewEmp.RowCount > old_y )
            {
                if (old_x == 0)
                    dataGridViewEmp.CurrentCell = dataGridViewEmp[1, 0];
                else
                    dataGridViewEmp.CurrentCell = dataGridViewEmp[old_x, old_y];
            }
            else
            {
                if (dataGridViewEmp.RowCount != 0 && old_y != 0)
                {
                    dataGridViewEmp.CurrentCell = dataGridViewEmp[old_x, dataGridViewEmp.RowCount - 1];
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            UpdateEMPGrid();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Form_emp_edit empEdit = new Form_emp_edit(dataGridViewEmp[0, dataGridViewEmp.CurrentCell.RowIndex].Value.ToString());
            empEdit.MdiParent = this.MdiParent;
            empEdit.FormClosing += (sender1, e1) =>
            {
                UpdateEMPGrid();
            };
            empEdit.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            UpdateEMPGrid();
        }
    }
}
