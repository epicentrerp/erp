﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ERP
{
    public partial class Form_error_logs : Form
    {
        public Form_error_logs()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.error_logs_viewTableAdapter.FillBy_filter_by_date(this.baluDataSet.error_logs_view,
                userControlPeriod1.dateTimePickerBegin.Value.ToString(),
                userControlPeriod1.dateTimePickerEnd.Value.ToString());
        }

        private void Form_error_logs_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "baluDataSet.error_logs_view". При необходимости она может быть перемещена или удалена.
            this.error_logs_viewTableAdapter.FillBy_filter_by_date(this.baluDataSet.error_logs_view, 
                userControlPeriod1.dateTimePickerBegin.Value.ToString(),
                userControlPeriod1.dateTimePickerEnd.Value.ToString());
        }
    }
}
