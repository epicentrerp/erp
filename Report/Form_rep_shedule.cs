﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Report
{
    public partial class Form_rep_shedule : Form_report
    {
        private int id_doc;
        private bool timesheet;

        public Form_rep_shedule(int id_doc, bool timesheet)
        {
            InitializeComponent();
            this.timesheet = timesheet;
            this.id_doc = id_doc;
        }

        private void Form_shedule_report_Load(object sender, EventArgs e)
        {
            if (timesheet)
            {
                int typeRep = 0;
                SqlConnection con = new SqlConnection();
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                DataSet ds = new DataSet();

                con.ConnectionString = ProSetting.ConnetionString;
                cmd.Connection = con;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "shedule_view";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@input_shedule", id_doc);

                con.Open();
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(ds, "shedule_view");
                con.Close();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "shedule_view_count_rep";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@input_shedule", id_doc);

                con.Open();
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(ds, "shedule_view_count_rep");
                con.Close();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT [status] FROM [docs_view] WHERE [id] = @id_doc ";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id_doc", id_doc);

                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    typeRep = dr.GetInt32(dr.GetOrdinal("status"));
                }
                con.Close();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT [shedule_month], [shedule_year] FROM [docs] WHERE [id] = @id_doc ";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id_doc", id_doc);

                con.Open();
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(ds, "docs");
                con.Close();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT [dep_name] FROM [dep] "
                                + " WHERE dep = (SELECT dep FROM [balu].[dbo].[docs] WHERE id = @id_doc)";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id_doc", id_doc);

                con.Open();
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(ds, "dep");
                con.Close();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT [name_ru]FROM [balu].[dbo].[employees]"
                            + " WHERE id = (SELECT [value] FROM [balu].[dbo].[sysseting] "
                            + " WHERE param = 'dir') ";
                cmd.Parameters.Clear();

                con.Open();
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(ds, "employees");
                con.Close();

                if (typeRep == 1)
                {
                    // вывод отчета
                    CrystalReport_shedule report_shedule = new CrystalReport_shedule();
                    report_shedule.SetDataSource(ds);
                    CrystalReport_shedule_stat report_shedule_stat = new CrystalReport_shedule_stat();
                    report_shedule_stat.SetDataSource(ds);
                    crystalReportViewer1.ReportSource = report_shedule;
                }
                else
                {
                    CrystalReport_shedule_pre report_shedule = new CrystalReport_shedule_pre();
                    report_shedule.SetDataSource(ds);
                    CrystalReport_shedule_stat report_shedule_stat = new CrystalReport_shedule_stat();
                    report_shedule_stat.SetDataSource(ds);
                    crystalReportViewer1.ReportSource = report_shedule;
                }
            }
            else
            {

                SqlConnection con = new SqlConnection();
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                DataSet ds = new DataSet();

                con.ConnectionString = ProSetting.ConnetionString;
                cmd.Connection = con;

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "shedule_view_timesheet";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@input_shedule", id_doc);

                con.Open();
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(ds, "shedule_view_timesheet");
                con.Close();
                
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT [shedule_month], [shedule_year] FROM [docs] WHERE [id] = @id_doc ";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id_doc", id_doc);

                con.Open();
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(ds, "docs");
                con.Close();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT [dep_name] FROM [dep] "
                                + " WHERE dep = (SELECT dep FROM [balu].[dbo].[docs] WHERE id = @id_doc)";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@id_doc", id_doc);

                con.Open();
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(ds, "dep");
                con.Close();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT [name_ru]FROM [balu].[dbo].[employees]"
                            + " WHERE id = (SELECT [value] FROM [balu].[dbo].[sysseting] "
                            + " WHERE param = 'dir') ";
                cmd.Parameters.Clear();

                con.Open();
                dataAdapter.SelectCommand = cmd;
                dataAdapter.Fill(ds, "employees");
                con.Close();

                CrystalReport_shedule_timesheet report_timesheet = new CrystalReport_shedule_timesheet();
                report_timesheet.SetDataSource(ds);
                crystalReportViewer1.ReportSource = report_timesheet;
            }
        }
    }
}
