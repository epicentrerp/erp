﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Report
{
    public partial class Form_report : Form
    {
        public Form_report()
        {
            InitializeComponent();
        }

        private void Form_report_Load(object sender, EventArgs e)
        {
            userControl_report1.buttonPrint.Click += new EventHandler(print_Click);
            userControl_report1.buttonExcel.Click += new EventHandler(xls_Click);
            userControl_report1.buttonPDF.Click += new EventHandler(pdf_Click);
            userControl_report1.buttonPrev.Click += new EventHandler(prev_Click);
            userControl_report1.buttonNext.Click += new EventHandler(next_Click);
        }

        private void xls_Click(object sender, System.EventArgs e)
        {
            // дописать выбор папки для экспорта.
            crystalReportViewer1.AllowedExportFormats = 2; // 1 - pdf, 2 - xls
            crystalReportViewer1.ExportReport();
        }

        private void pdf_Click(object sender, System.EventArgs e)
        {
            // дописать выбор папки для экспорта
            crystalReportViewer1.AllowedExportFormats = 1; // 1 - pdf, 2 - xls
            crystalReportViewer1.ExportReport();
        }

        private void print_Click(object sender, System.EventArgs e)
        {
            crystalReportViewer1.PrintReport();
        }

        private void prev_Click(object sender, System.EventArgs e)
        {
            crystalReportViewer1.ShowPreviousPage();
        }

        private void next_Click(object sender, System.EventArgs e)
        {
            crystalReportViewer1.ShowNextPage();
        }
    }
}
