﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Report
{
    public partial class Form_rep_dinner_count_rep : Form_report
    {
        private string dataBegin, dataEnd, dep;
        private bool det, part;
        public Form_rep_dinner_count_rep(string dataBegin, string dataEnd, string dep, bool det, bool part)
        {
            InitializeComponent();
            this.dataBegin = dataBegin;
            this.dataEnd = dataEnd;
            this.dep = dep;
            this.det = det;
            this.part = part;
        }

        private void Form_rep_dinner_count_rep_Load(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            
            
            string sql = "";

            if (det == true)
            {
                if (part)
                {
                    if (dep != "")
                    {
                        sql = "SELECT [id],[dep],[date],[time],[name_ru],[dinner],[id_type] FROM [dinner_rep]"
                        + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and dep = @dep and time < (SELECT value FROM [balu].[dbo].[sysseting] WHERE [param]='dinner')"
                        + " ORDER BY dep, date, time";
                        sqlCmd.Parameters.AddWithValue("@dep", dep);
                    }
                    else
                    {
                        if (ProSetting.Access == 3)
                        {
                            sql = "SELECT [id],dep,[date],[time],[name_ru],[dinner],[id_type] FROM [dinner_rep] "
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and time < (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                            + " ORDER BY dep, date, time";
                        }
                        else
                        {
                            sql = "SELECT [id],dep_table.dep,[date],[time],[name_ru],[dinner],[id_type] FROM [dinner_rep] "
                            + " inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
                            + " ON [dinner_rep].dep = dep_table.dep"
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and time < (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                            + " ORDER BY dep_table.dep, date, time";

                        }
                    }
                }
                else
                {
                    if (dep != "")
                    {
                        sql = "SELECT [id],[dep],[date],[time],[name_ru],[dinner],[id_type] FROM [dinner_rep]"
                        + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and dep = @dep and time > (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                        + " ORDER BY dep, date, time";
                        sqlCmd.Parameters.AddWithValue("@dep", dep);
                    }
                    else
                    {
                        if (ProSetting.Access == 3)
                        {
                            sql = "SELECT [id],dep,[date],[time],[name_ru],[dinner],[id_type] FROM [dinner_rep]"
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and time > (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                            + " ORDER BY dep_table.dep, date, time";

                        }
                        else
                        {
                            sql = "SELECT [id],dep_table.dep,[date],[time],[name_ru],[dinner],[id_type] FROM [dinner_rep]"
                                + " inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
                                + " ON [dinner_rep].dep = dep_table.dep"
                                + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and time > (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                                + " ORDER BY dep_table.dep, date, time";
                        }
                    }
                }

                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.CommandText = sql;
                sqlCmd.Connection = sqlConn;

                sqlCmd.Parameters.AddWithValue("@id_user", ProSetting.User);
                sqlCmd.Parameters.AddWithValue("@dataBegin", Convert.ToDateTime(dataBegin));
                sqlCmd.Parameters.AddWithValue("@dataEnd", Convert.ToDateTime(dataEnd));
            }
            else
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandText = "[dinner_rep_count]";
                sqlCmd.Connection = sqlConn;

                if (dep == "")
                    dep = "0";
                sqlCmd.Parameters.AddWithValue("@access", ProSetting.Access);
                sqlCmd.Parameters.AddWithValue("@dep", dep);
                sqlCmd.Parameters.AddWithValue("@id_user", ProSetting.User);
                sqlCmd.Parameters.AddWithValue("@part", Convert.ToInt32(part));
                sqlCmd.Parameters.AddWithValue("@dataBegin", Convert.ToDateTime(dataBegin));
                sqlCmd.Parameters.AddWithValue("@dataEnd", Convert.ToDateTime(dataEnd));
            }
                

            /*
            // переделать эту хуйню в храненку
            if (part)
            {
                if (det == true)
                {
                
                    if (dep != "")
                    {
                        sql = "SELECT [id],[dep],[date],[time],[name_ru],[dinner],[id_type] FROM [dinner_rep]"
                        + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and dep = @dep and time < (SELECT value FROM [balu].[dbo].[sysseting] WHERE [param]='dinner')"
                        + " ORDER BY dep, date, time";
                        sqlCmd.Parameters.AddWithValue("@dep", dep);
                    }
                    else
                    {
                        if (ProSetting.Access == 3)
                        {
                            sql = "SELECT [id],dep,[date],[time],[name_ru],[dinner],[id_type] FROM [dinner_rep] "
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and time < (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                            + " ORDER BY dep, date, time";
                        }
                        else
                        {
                            sql = "SELECT [id],dep_table.dep,[date],[time],[name_ru],[dinner],[id_type] FROM [dinner_rep] "
                            + " inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
                            + " ON [dinner_rep].dep = dep_table.dep"
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and time < (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                            + " ORDER BY dep_table.dep, date, time";
                            
                        }
                    }
                }
                else
                {
                    // count
                    if (dep != "")
                    {
                        sql = "SELECT [dep],[date],count([id_type] ) as [id_type] FROM [dinner_rep]"
                        + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and dep = @dep and time < (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                        + " GROUP BY [dep],[date] ORDER BY dep, date";
                        sqlCmd.Parameters.AddWithValue("@dep", dep);
                    }
                    else
                    {
                        if (ProSetting.Access == 3)
                        {
                        sql = "SELECT dep,[date],count([id_type] ) as [id_type] FROM [dinner_rep]"
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and time < (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                            + " GROUP BY [dep],[date] ORDER BY dep, date";
                            
                        }
                        else
                        {
                            sql = "SELECT dep_table.dep,[date],count([id_type] ) as [id_type] FROM [dinner_rep]"
                            + " inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
                            + " ON [dinner_rep].dep = dep_table.dep"
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and time < (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                            + " GROUP BY dep_table.dep,[date] ORDER BY dep_table.dep, date";
                        }
                    }
                }
            }
            else
            {
                if (det == true)
                {
                    if (dep != "")
                    {
                        sql = "SELECT [id],[dep],[date],[time],[name_ru],[dinner],[id_type] FROM [dinner_rep]"
                        + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and dep = @dep and time > (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                        + " ORDER BY dep, date, time";
                        sqlCmd.Parameters.AddWithValue("@dep", dep);
                    }
                    else
                    {
                        if (ProSetting.Access == 3)
                        {
                            sql = "SELECT [id],dep,[date],[time],[name_ru],[dinner],[id_type] FROM [dinner_rep]"
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and time > (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                            + " ORDER BY dep_table.dep, date, time";
                            
                        }
                        else
                        {
                            sql = "SELECT [id],dep_table.dep,[date],[time],[name_ru],[dinner],[id_type] FROM [dinner_rep]"
                                + " inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
                                + " ON [dinner_rep].dep = dep_table.dep"
                                + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and time > (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                                + " ORDER BY dep_table.dep, date, time";
                        }
                    }
                }
                else
                {
                    // count
                    if (dep != "")
                    {
                        sql = "SELECT [dep],[date],count([id_type] ) as [id_type] FROM [dinner_rep]"
                        + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and dep = @dep and time > (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                        + " GROUP BY [dep],[date] ORDER BY dep, date";
                        sqlCmd.Parameters.AddWithValue("@dep", dep);
                    }
                    else
                    {
                        if (ProSetting.Access == 3)
                        {
                            sql = "SELECT dep,[date],count([id_type] ) as [id_type] FROM [dinner_rep]"
                                + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and time > (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                                + " GROUP BY dep_table.dep,[date] ORDER BY dep, date";
                            
                        }
                        else
                        {
                            sql = "SELECT dep_table.dep,[date],count([id_type] ) as [id_type] FROM [dinner_rep]"
                                + " inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
                                + " ON [dinner_rep].dep = dep_table.dep"
                                + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and time > (SELECT value FROM [sysseting] WHERE [param]='dinner')"
                                + " GROUP BY dep_table.dep,[date] ORDER BY dep_table.dep, date";
                        }
                    }
                }
            }
            */


            if (det)
            {
                sqlConn.Open();
                dataAdapter.SelectCommand = sqlCmd;
                dataAdapter.Fill(ds, "dinner_rep");
                sqlConn.Close();

                CrystalReport_dinner_fio brake_dinner = new CrystalReport_dinner_fio();
                brake_dinner.SetDataSource(ds);
                crystalReportViewer1.ReportSource = brake_dinner;
            }
            else
            {
                sqlConn.Open();
                dataAdapter.SelectCommand = sqlCmd;
                dataAdapter.Fill(ds, "dinner_rep_count");
                sqlConn.Close();
                CrystalReport_dinner_count brake_dinner = new CrystalReport_dinner_count();
                brake_dinner.SetDataSource(ds);
                crystalReportViewer1.ReportSource = brake_dinner;
            }

        }
    }
}
