﻿namespace Report
{
    partial class UserControlEmp
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxEmp = new System.Windows.Forms.TextBox();
            this.buttonEmpSelect = new System.Windows.Forms.Button();
            this.buttonEmpClear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxEmp
            // 
            this.textBoxEmp.Enabled = false;
            this.textBoxEmp.Location = new System.Drawing.Point(3, 22);
            this.textBoxEmp.Name = "textBoxEmp";
            this.textBoxEmp.Size = new System.Drawing.Size(186, 20);
            this.textBoxEmp.TabIndex = 0;
            // 
            // buttonEmpSelect
            // 
            this.buttonEmpSelect.Location = new System.Drawing.Point(195, 20);
            this.buttonEmpSelect.Name = "buttonEmpSelect";
            this.buttonEmpSelect.Size = new System.Drawing.Size(26, 23);
            this.buttonEmpSelect.TabIndex = 1;
            this.buttonEmpSelect.Text = ".";
            this.buttonEmpSelect.UseVisualStyleBackColor = true;
            // 
            // buttonEmpClear
            // 
            this.buttonEmpClear.Location = new System.Drawing.Point(227, 20);
            this.buttonEmpClear.Name = "buttonEmpClear";
            this.buttonEmpClear.Size = new System.Drawing.Size(47, 23);
            this.buttonEmpClear.TabIndex = 2;
            this.buttonEmpClear.Text = "Все";
            this.buttonEmpClear.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Сотрудник";
            // 
            // UserControlEmp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonEmpClear);
            this.Controls.Add(this.buttonEmpSelect);
            this.Controls.Add(this.textBoxEmp);
            this.Name = "UserControlEmp";
            this.Size = new System.Drawing.Size(325, 55);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textBoxEmp;
        public System.Windows.Forms.Button buttonEmpSelect;
        public System.Windows.Forms.Button buttonEmpClear;
    }
}
