﻿namespace Report
{
    partial class Form_rep_dinner_count_rep
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // userControl_report1
            // 
            this.userControl_report1.Size = new System.Drawing.Size(1523, 28);
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.Size = new System.Drawing.Size(985, 547);
            // 
            // Form_rep_dinner_count_rep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(985, 547);
            this.Name = "Form_rep_dinner_count_rep";
            this.Text = "Form_rep_dinner_count_rep";
            this.Load += new System.EventHandler(this.Form_rep_dinner_count_rep_Load);
            this.ResumeLayout(false);

        }

        #endregion
    }
}