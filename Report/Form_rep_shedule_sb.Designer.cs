﻿namespace Report
{
    partial class Form_rep_shedule_sb
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.buttonRep = new System.Windows.Forms.Button();
            this.userControlEmp1 = new Report.UserControlEmp();
            this.userControlDep1 = new Report.UserControlDep();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(12, 33);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(88, 20);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // buttonRep
            // 
            this.buttonRep.Location = new System.Drawing.Point(233, 192);
            this.buttonRep.Name = "buttonRep";
            this.buttonRep.Size = new System.Drawing.Size(75, 23);
            this.buttonRep.TabIndex = 2;
            this.buttonRep.Text = "Сформировать";
            this.buttonRep.UseVisualStyleBackColor = true;
            this.buttonRep.Click += new System.EventHandler(this.buttonRep_Click);
            // 
            // userControlEmp1
            // 
            this.userControlEmp1.Location = new System.Drawing.Point(12, 118);
            this.userControlEmp1.Name = "userControlEmp1";
            this.userControlEmp1.Size = new System.Drawing.Size(284, 55);
            this.userControlEmp1.TabIndex = 3;
            // 
            // userControlDep1
            // 
            this.userControlDep1.Location = new System.Drawing.Point(12, 70);
            this.userControlDep1.Name = "userControlDep1";
            this.userControlDep1.Size = new System.Drawing.Size(284, 42);
            this.userControlDep1.TabIndex = 0;
            // 
            // Form_rep_shedule_sb
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 227);
            this.Controls.Add(this.userControlEmp1);
            this.Controls.Add(this.buttonRep);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.userControlDep1);
            this.Name = "Form_rep_shedule_sb";
            this.Text = "Form_rep_shedule_sb";
            this.Load += new System.EventHandler(this.Form_rep_shedule_sb_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private UserControlDep userControlDep1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button buttonRep;
        private UserControlEmp userControlEmp1;
    }
}