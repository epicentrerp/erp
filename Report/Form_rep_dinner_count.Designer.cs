﻿namespace Report
{
    partial class Form_rep_dinner_count
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonForm = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.radioButtonFirst = new System.Windows.Forms.RadioButton();
            this.radioButtonSecond = new System.Windows.Forms.RadioButton();
            this.userControlDep1 = new Report.UserControlDep();
            this.userControlPeriod1 = new Report.UserControlPeriod();
            this.SuspendLayout();
            // 
            // buttonForm
            // 
            this.buttonForm.Location = new System.Drawing.Point(218, 203);
            this.buttonForm.Name = "buttonForm";
            this.buttonForm.Size = new System.Drawing.Size(95, 23);
            this.buttonForm.TabIndex = 2;
            this.buttonForm.Text = "Сформировать";
            this.buttonForm.UseVisualStyleBackColor = true;
            this.buttonForm.Click += new System.EventHandler(this.buttonForm_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(24, 119);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(138, 17);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "Развернутый по ФИО";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // radioButtonFirst
            // 
            this.radioButtonFirst.AutoSize = true;
            this.radioButtonFirst.Checked = true;
            this.radioButtonFirst.Location = new System.Drawing.Point(24, 147);
            this.radioButtonFirst.Name = "radioButtonFirst";
            this.radioButtonFirst.Size = new System.Drawing.Size(51, 17);
            this.radioButtonFirst.TabIndex = 4;
            this.radioButtonFirst.TabStop = true;
            this.radioButtonFirst.Text = "Обед";
            this.radioButtonFirst.UseVisualStyleBackColor = true;
            // 
            // radioButtonSecond
            // 
            this.radioButtonSecond.AutoSize = true;
            this.radioButtonSecond.Location = new System.Drawing.Point(24, 170);
            this.radioButtonSecond.Name = "radioButtonSecond";
            this.radioButtonSecond.Size = new System.Drawing.Size(53, 17);
            this.radioButtonSecond.TabIndex = 5;
            this.radioButtonSecond.Text = "Ужин";
            this.radioButtonSecond.UseVisualStyleBackColor = true;
            // 
            // userControlDep1
            // 
            this.userControlDep1.Location = new System.Drawing.Point(24, 62);
            this.userControlDep1.Name = "userControlDep1";
            this.userControlDep1.Size = new System.Drawing.Size(289, 51);
            this.userControlDep1.TabIndex = 1;
            // 
            // userControlPeriod1
            // 
            this.userControlPeriod1.Location = new System.Drawing.Point(12, 30);
            this.userControlPeriod1.Name = "userControlPeriod1";
            this.userControlPeriod1.Size = new System.Drawing.Size(240, 26);
            this.userControlPeriod1.TabIndex = 0;
            // 
            // Form_rep_dinner_count
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(325, 238);
            this.Controls.Add(this.radioButtonSecond);
            this.Controls.Add(this.radioButtonFirst);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.buttonForm);
            this.Controls.Add(this.userControlDep1);
            this.Controls.Add(this.userControlPeriod1);
            this.Name = "Form_rep_dinner_count";
            this.Text = "Form_rep_dinner_count";
            this.Load += new System.EventHandler(this.Form_rep_dinner_count_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UserControlPeriod userControlPeriod1;
        private UserControlDep userControlDep1;
        private System.Windows.Forms.Button buttonForm;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.RadioButton radioButtonFirst;
        private System.Windows.Forms.RadioButton radioButtonSecond;
    }
}