﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Report
{
    public partial class Form_rep_order_by_ass : Form_report
    {
        private int part;
        private string id_doc;

        public Form_rep_order_by_ass(int part, string id_doc)
        {
            InitializeComponent();
            this.part = part;
            this.id_doc = id_doc;
        }

        private void Form_rep_order_by_ass_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            DataSet ds = new DataSet();

            con.ConnectionString = ProSetting.ConnetionString;
            cmd.Connection = con;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "dinner_order_rep";
            cmd.Parameters.Clear();
 
            cmd.Parameters.AddWithValue("@input_id_doc", id_doc);
            cmd.Parameters.AddWithValue("@input_part", part);

            con.Open();
            dataAdapter.SelectCommand = cmd;
            dataAdapter.Fill(ds, "dinner_order_rep");
            con.Close();

            CrystalReport_order_ass report_order_ass = new CrystalReport_order_ass();
            report_order_ass.SetDataSource(ds);
            crystalReportViewer1.ReportSource = report_order_ass;
        }
    }
}
