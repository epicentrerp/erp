﻿namespace Report
{
    partial class Form_rep_brake_dinner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxDep = new System.Windows.Forms.TextBox();
            this.textBoxEmp = new System.Windows.Forms.TextBox();
            this.buttonDep = new System.Windows.Forms.Button();
            this.buttonDepCln = new System.Windows.Forms.Button();
            this.buttonEmp = new System.Windows.Forms.Button();
            this.buttonEmpCln = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.userControlPeriod1 = new Report.UserControlPeriod();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // textBoxDep
            // 
            this.textBoxDep.Enabled = false;
            this.textBoxDep.Location = new System.Drawing.Point(12, 111);
            this.textBoxDep.Name = "textBoxDep";
            this.textBoxDep.Size = new System.Drawing.Size(155, 20);
            this.textBoxDep.TabIndex = 1;
            // 
            // textBoxEmp
            // 
            this.textBoxEmp.Enabled = false;
            this.textBoxEmp.Location = new System.Drawing.Point(12, 164);
            this.textBoxEmp.Name = "textBoxEmp";
            this.textBoxEmp.Size = new System.Drawing.Size(155, 20);
            this.textBoxEmp.TabIndex = 2;
            // 
            // buttonDep
            // 
            this.buttonDep.Location = new System.Drawing.Point(176, 109);
            this.buttonDep.Name = "buttonDep";
            this.buttonDep.Size = new System.Drawing.Size(26, 23);
            this.buttonDep.TabIndex = 3;
            this.buttonDep.Text = ".";
            this.buttonDep.UseVisualStyleBackColor = true;
            this.buttonDep.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonDepCln
            // 
            this.buttonDepCln.Location = new System.Drawing.Point(208, 109);
            this.buttonDepCln.Name = "buttonDepCln";
            this.buttonDepCln.Size = new System.Drawing.Size(44, 23);
            this.buttonDepCln.TabIndex = 4;
            this.buttonDepCln.Text = "Все";
            this.buttonDepCln.UseVisualStyleBackColor = true;
            this.buttonDepCln.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonEmp
            // 
            this.buttonEmp.Location = new System.Drawing.Point(176, 162);
            this.buttonEmp.Name = "buttonEmp";
            this.buttonEmp.Size = new System.Drawing.Size(26, 23);
            this.buttonEmp.TabIndex = 5;
            this.buttonEmp.Text = ".";
            this.buttonEmp.UseVisualStyleBackColor = true;
            this.buttonEmp.Click += new System.EventHandler(this.button3_Click);
            // 
            // buttonEmpCln
            // 
            this.buttonEmpCln.Location = new System.Drawing.Point(208, 162);
            this.buttonEmpCln.Name = "buttonEmpCln";
            this.buttonEmpCln.Size = new System.Drawing.Size(44, 23);
            this.buttonEmpCln.TabIndex = 6;
            this.buttonEmpCln.Text = "Все";
            this.buttonEmpCln.UseVisualStyleBackColor = true;
            this.buttonEmpCln.Click += new System.EventHandler(this.button4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Период";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Отдел";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Сотрудник";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(154, 215);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(97, 23);
            this.button5.TabIndex = 11;
            this.button5.Text = "Сформировать";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // userControlPeriod1
            // 
            this.userControlPeriod1.Location = new System.Drawing.Point(11, 39);
            this.userControlPeriod1.Name = "userControlPeriod1";
            this.userControlPeriod1.Size = new System.Drawing.Size(240, 26);
            this.userControlPeriod1.TabIndex = 0;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(12, 200);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(87, 17);
            this.checkBox1.TabIndex = 12;
            this.checkBox1.Text = "Не взявшие";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // Form_brake_dinner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(263, 249);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonEmpCln);
            this.Controls.Add(this.buttonEmp);
            this.Controls.Add(this.buttonDepCln);
            this.Controls.Add(this.buttonDep);
            this.Controls.Add(this.textBoxEmp);
            this.Controls.Add(this.textBoxDep);
            this.Controls.Add(this.userControlPeriod1);
            this.Name = "Form_brake_dinner";
            this.Text = "Отчет по перерывам";
            this.Load += new System.EventHandler(this.Form_brake_dinner_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UserControlPeriod userControlPeriod1;
        private System.Windows.Forms.TextBox textBoxDep;
        private System.Windows.Forms.TextBox textBoxEmp;
        private System.Windows.Forms.Button buttonDep;
        private System.Windows.Forms.Button buttonDepCln;
        private System.Windows.Forms.Button buttonEmp;
        private System.Windows.Forms.Button buttonEmpCln;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}