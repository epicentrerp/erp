﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Report
{
    public partial class Form_rep_history : Form_report
    {
        private string id_doc;

        public Form_rep_history(string id_doc)
        {
            InitializeComponent();
            this.id_doc = id_doc;
        }

        private void Form_rep_history_Load(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();

            string sql = "SELECT [dep],[name_ru],[date],[time],[discribe],[doc_numder_full] FROM [balu].[dbo].[history_view] "
                + " WHERE id_doc = @id_doc ORDER BY date, time";

            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = sql;
            sqlCmd.Connection = sqlConn;

            sqlCmd.Parameters.AddWithValue("@id_doc", id_doc);

            sqlConn.Open();
            dataAdapter.SelectCommand = sqlCmd;
            dataAdapter.Fill(ds, "history_view");
            sqlConn.Close();

            CrystalReport_history history = new CrystalReport_history();
            history.SetDataSource(ds);
            crystalReportViewer1.ReportSource = history; 
        }
    }
}
