﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Report
{
    public partial class Form_rep_shedule_sb_rep : Form_report
    {
    
        string day, month, year, dep, id_emp;

        public Form_rep_shedule_sb_rep(string day, string month, string year, string dep, string id_emp)
        {
            InitializeComponent();
            this.day = day;
            this.month = month;
            this.year = year;
            this.dep = dep;
            this.id_emp = id_emp;
        }

        private void Form_rep_shedule_sb_rep_Load(object sender, EventArgs e)
        {
            if (id_emp == "")
                id_emp = "0";

            if (dep == "")
                dep = "0";
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            DataSet ds = new DataSet();

            con.ConnectionString = ProSetting.ConnetionString;
            cmd.Connection = con;

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "shedule_report_sb";
            cmd.Parameters.Clear();
 
            cmd.Parameters.AddWithValue("@input_day", Convert.ToInt32(day));
            cmd.Parameters.AddWithValue("@input_month", Convert.ToInt32(month));
            cmd.Parameters.AddWithValue("@input_year", year);
            cmd.Parameters.AddWithValue("@input_id_emp", id_emp);
            cmd.Parameters.AddWithValue("@input_dep", dep);

            con.Open();
            dataAdapter.SelectCommand = cmd;
            dataAdapter.Fill(ds, "shedule_report_sb");
            con.Close();

            CrystalReport_shedule_sb report_shedule_sb = new CrystalReport_shedule_sb();
            report_shedule_sb.SetDataSource(ds);
            crystalReportViewer1.ReportSource = report_shedule_sb;
        }
    }
}
