﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Report
{
    public partial class Form_rep_brake_dinner_rep : Form_report
    {
        private string dataBegin, dataEnd, dep, idEmp;
        private bool noTake;

        public Form_rep_brake_dinner_rep(string dataBegin, string dataEnd, string dep, string idEmp, bool noTake)
        {
            InitializeComponent();
            this.dataBegin = dataBegin;
            this.dataEnd = dataEnd;
            this.dep = dep;
            this.idEmp = idEmp;
            this.noTake = noTake;
        }

        private void Form_brake_dinner_rep_Load(object sender, EventArgs e)
        {

            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();
            DataSet ds = new DataSet();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();

            string sql = "";
            if (noTake)
            {
                sql = "SELECT dep_table.dep,[name_ru],[date],[time1],[dinner],[time2],[dif] FROM [dinner_rep_diff]"
                        + " inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
                        + " ON [dinner_rep_diff].dep = dep_table.dep"
                        + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and dinner is null "
                        + " and id not in (SELECT id_emp FROM [dinner_history] WHERE [dinner] = 2 and (date >= @dataBegin) AND (date <= @dataEnd) )"
                        + " ORDER BY dep_table.dep, date, time1";
            }
            else
            {


                if (ProSetting.Access != 4)
                {
                    if (dep == "" && idEmp == "")
                    {
                        sql = "SELECT dep_table.dep,[name_ru],[date],[time1],[dinner],[time2],[dif] FROM [dinner_rep_diff]"
                           + " inner join (SELECT dep FROM users_dep WHERE id_users = @id_user)as dep_table "
                           + " ON [dinner_rep_diff].dep = dep_table.dep"
                           + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) ORDER BY dep_table.dep, date, time1";
                    }

                    if (dep != "" && idEmp == "")
                    {
                        sql = "SELECT dep_table.dep,[name_ru],[date],[time1],[dinner],[time2],[dif] FROM [dinner_rep_diff] "
                            + " inner join (SELECT dep FROM users_dep WHERE id_users = @id_user )as dep_table "
                            + " ON [dinner_rep_diff].dep = dep_table.dep"
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and dep_table.dep = @dep ORDER BY dep_table.dep, date, time1 ";
                        sqlCmd.Parameters.AddWithValue("@dep", dep);
                    }

                    if (dep == "" && idEmp != "")
                    {
                        sql = "SELECT dep_table.dep,[name_ru],[date],[time1],[dinner],[time2],[dif] FROM [dinner_rep_diff] "
                            + " inner join (SELECT dep FROM users_dep WHERE id_users = @id_user) as dep_table "
                            + " ON [dinner_rep_diff].dep = dep_table.dep"
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and id = @idEmp ORDER BY dep_table.dep, date, time1";
                        sqlCmd.Parameters.AddWithValue("@idEmp", idEmp);
                    }
                    if (dep != "" && idEmp != "")
                    {
                        sql = "SELECT [dep],[name_ru],[date],[time1],[dinner],[time2],[dif] FROM [dinner_rep_diff] "
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and dep = @dep and id = @idEmp ORDER BY dep, date, time1";
                        sqlCmd.Parameters.AddWithValue("@dep", dep);
                        sqlCmd.Parameters.AddWithValue("@idEmp", idEmp);
                    }
                }
                else
                {
                    if (dep == "" && idEmp == "")
                    {
                        sql = "SELECT dep,[name_ru],[date],[time1],[dinner],[time2],[dif] FROM [dinner_rep_diff]"
                           + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) ORDER BY dep, date, time1";
                    }

                    if (dep != "" && idEmp == "")
                    {
                        sql = "SELECT dep,[name_ru],[date],[time1],[dinner],[time2],[dif] FROM [dinner_rep_diff] "
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and dep = @dep ORDER BY dep, date, time1 ";
                        sqlCmd.Parameters.AddWithValue("@dep", dep);
                    }

                    if (dep == "" && idEmp != "")
                    {
                        sql = "SELECT dep,[name_ru],[date],[time1],[dinner],[time2],[dif] FROM [dinner_rep_diff] "
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and id = @idEmp ORDER BY dep, date, time1";
                        sqlCmd.Parameters.AddWithValue("@idEmp", idEmp);
                    }
                    if (dep != "" && idEmp != "")
                    {
                        sql = "SELECT [dep],[name_ru],[date],[time1],[dinner],[time2],[dif] FROM [dinner_rep_diff] "
                            + " WHERE (date >= @dataBegin) AND (date <= @dataEnd) and dep = @dep and id = @idEmp ORDER BY dep, date, time1";
                        sqlCmd.Parameters.AddWithValue("@dep", dep);
                        sqlCmd.Parameters.AddWithValue("@idEmp", idEmp);
                    }
                }
            }

            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = sql;
            sqlCmd.Connection = sqlConn;
            
            sqlCmd.Parameters.AddWithValue("@id_user", ProSetting.User);
            sqlCmd.Parameters.AddWithValue("@dataBegin", Convert.ToDateTime(dataBegin));
            sqlCmd.Parameters.AddWithValue("@dataEnd", Convert.ToDateTime(dataEnd));

            sqlConn.Open();
            dataAdapter.SelectCommand = sqlCmd;
            dataAdapter.Fill(ds, "dinner_rep_diff");
            sqlConn.Close();
            
            CrystalReport_brake_dinner brake_dinner = new CrystalReport_brake_dinner();
            brake_dinner.SetDataSource(ds);
            crystalReportViewer1.ReportSource = brake_dinner; 
        }
    }
}
