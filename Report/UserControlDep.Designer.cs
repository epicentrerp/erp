﻿namespace Report
{
    partial class UserControlDep
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.textBoxDep = new System.Windows.Forms.TextBox();
            this.buttonDepSelect = new System.Windows.Forms.Button();
            this.buttonAll = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxDep
            // 
            this.textBoxDep.Enabled = false;
            this.textBoxDep.Location = new System.Drawing.Point(3, 16);
            this.textBoxDep.Name = "textBoxDep";
            this.textBoxDep.Size = new System.Drawing.Size(186, 20);
            this.textBoxDep.TabIndex = 0;
            // 
            // buttonDepSelect
            // 
            this.buttonDepSelect.Location = new System.Drawing.Point(195, 14);
            this.buttonDepSelect.Name = "buttonDepSelect";
            this.buttonDepSelect.Size = new System.Drawing.Size(26, 23);
            this.buttonDepSelect.TabIndex = 1;
            this.buttonDepSelect.Text = ".";
            this.buttonDepSelect.UseVisualStyleBackColor = true;
            // 
            // buttonAll
            // 
            this.buttonAll.Location = new System.Drawing.Point(225, 14);
            this.buttonAll.Name = "buttonAll";
            this.buttonAll.Size = new System.Drawing.Size(47, 23);
            this.buttonAll.TabIndex = 2;
            this.buttonAll.Text = "Все";
            this.buttonAll.UseVisualStyleBackColor = true;
            this.buttonAll.Click += new System.EventHandler(this.buttonAll_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Отдел";
            // 
            // UserControlDep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonAll);
            this.Controls.Add(this.buttonDepSelect);
            this.Controls.Add(this.textBoxDep);
            this.Name = "UserControlDep";
            this.Size = new System.Drawing.Size(284, 42);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button buttonAll;
        public System.Windows.Forms.TextBox textBoxDep;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button buttonDepSelect;
    }
}
