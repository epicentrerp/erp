﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Journal;

namespace Report
{
    public partial class Form_rep_dinner_count : Form
    {
        Form_dep depSelect;

        public Form_rep_dinner_count()
        {
            InitializeComponent();
        }

        private void Form_rep_dinner_count_Load(object sender, EventArgs e)
        {
            userControlDep1.buttonDepSelect.Click += new EventHandler(dep_Click);
        }

        private void dep_Click(object sender, System.EventArgs e)
        {
            if (depSelect == null || depSelect.IsDisposed)
            {
                if (ProSetting.Access == 3)
                    depSelect = new Form_dep(0);
                else
                    depSelect = new Form_dep(ProSetting.User);
                depSelect.MdiParent = this.MdiParent;
                depSelect.FormClosing += (sender1, e1) =>
                {
                    if (depSelect.dep != null)
                    {
                        userControlDep1.textBoxDep.Text = depSelect.dep;
                    }
                };
                depSelect.Show();
            }
            else
            {
                depSelect.Activate();
            }
        }

        private void buttonForm_Click(object sender, EventArgs e)
        {
            Form_rep_dinner_count_rep dinnerCountRep = new Form_rep_dinner_count_rep(userControlPeriod1.dateTimePickerBegin.Value.ToShortDateString(),
                userControlPeriod1.dateTimePickerEnd.Value.ToShortDateString(), userControlDep1.textBoxDep.Text, checkBox1.Checked, radioButtonFirst.Checked);
            dinnerCountRep.MdiParent = this.MdiParent;
            dinnerCountRep.Show();
        }

    }
}
