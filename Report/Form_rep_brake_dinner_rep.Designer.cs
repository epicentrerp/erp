﻿namespace Report
{
    partial class Form_rep_brake_dinner_rep
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // userControl_report1
            // 
            this.userControl_report1.Size = new System.Drawing.Size(936, 28);
            // 
            // crystalReportViewer1
            // 
            this.crystalReportViewer1.Size = new System.Drawing.Size(936, 453);
            // 
            // Form_brake_dinner_rep
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 453);
            this.Name = "Form_brake_dinner_rep";
            this.Text = "Перерывы";
            this.Load += new System.EventHandler(this.Form_brake_dinner_rep_Load);
            this.ResumeLayout(false);

        }

        #endregion

    }
}