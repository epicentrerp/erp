﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Journal;
namespace Report
{
    public partial class Form_rep_shedule_sb : Form
    {
        Form_dep depSelect;
        Form_emp empSelect;

        string id_emp="0";

        public Form_rep_shedule_sb()
        {
            InitializeComponent();
        }

        private void Form_rep_shedule_sb_Load(object sender, EventArgs e)
        {
            userControlDep1.buttonDepSelect.Click += new EventHandler(dep_Click);
            userControlEmp1.buttonEmpSelect.Click += new EventHandler(emp_Click);
        }

        private void dep_Click(object sender, System.EventArgs e)
        {
            if (depSelect == null || depSelect.IsDisposed)
            {
                if (ProSetting.Access == 4)
                    depSelect = new Form_dep(0);
                else
                    depSelect = new Form_dep(ProSetting.User);
                depSelect.MdiParent = this.MdiParent;
                depSelect.FormClosing += (sender1, e1) =>
                {
                    if (depSelect.dep != null)
                    {
                        userControlDep1.textBoxDep.Text = depSelect.dep;
                    }
                };
                depSelect.Show();
            }
            else
            {
                depSelect.Activate();
            }
        }

        private void emp_Click(object sender, System.EventArgs e)
        {
            if (empSelect == null || empSelect.IsDisposed)
            {
                if (ProSetting.Access == 4)
                    empSelect = new Form_emp("");
                else
                    empSelect = new Form_emp(userControlDep1.textBoxDep.Text);
                empSelect.MdiParent = this.MdiParent;
                empSelect.FormClosing += (sender1, e1) =>
                {
                    if (empSelect.id != null)
                    {
                        userControlDep1.textBoxDep.Text = empSelect.dep;
                        id_emp = empSelect.id;
                        userControlEmp1.textBoxEmp.Text = empSelect.name;
                    }
                };
                empSelect.Show();
            }
            else
            {
                empSelect.Activate();
            }
        }

        private void buttonRep_Click(object sender, EventArgs e)
        {
            string date = dateTimePicker1.Value.ToShortDateString();
            
            string day = date.Substring(0, 2);
            string month = date.Substring(3, 2);
            string year = date.Substring(6, 4);

            Form_rep_shedule_sb_rep sheduleRepSb = new Form_rep_shedule_sb_rep(day, month, year, userControlDep1.textBoxDep.Text, id_emp);
            sheduleRepSb.MdiParent = this.MdiParent;
            sheduleRepSb.Show();
        }

    }
}

