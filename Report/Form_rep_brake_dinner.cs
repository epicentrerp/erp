﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Journal;

namespace Report
{
    public partial class Form_rep_brake_dinner : Form
    {
        private string idEmp;
        Form_emp empSelect;
        Form_dep depSelect;

        public Form_rep_brake_dinner()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBoxEmp.Text = "";
            idEmp = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (depSelect == null || depSelect.IsDisposed)
            {
                if (ProSetting.Access == 4)
                    depSelect = new Form_dep(0);
                else
                    depSelect = new Form_dep(ProSetting.User);            
                depSelect.MdiParent = this.MdiParent;
                depSelect.FormClosing += (sender1, e1) =>
                {
                    if (depSelect.dep != null)
                    {
                        textBoxDep.Text = depSelect.dep;
                    }
                };
                depSelect.Show();
            }
            else
            {
                depSelect.Activate();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBoxDep.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (empSelect == null || empSelect.IsDisposed)
            {
                empSelect = new Form_emp(textBoxDep.Text);
                empSelect.MdiParent = this.MdiParent;
                empSelect.FormClosing += (sender1, e1) =>
                {
                    if (empSelect.id != null)
                    {
                        textBoxEmp.Text = empSelect.name;
                        idEmp = empSelect.id;
                    }
                };
                empSelect.Show();
            }
            else
            {
                empSelect.Activate();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form_rep_brake_dinner_rep brakeRep = new Form_rep_brake_dinner_rep(userControlPeriod1.dateTimePickerBegin.Value.ToShortDateString(),
                userControlPeriod1.dateTimePickerEnd.Value.ToShortDateString(), textBoxDep.Text, idEmp, checkBox1.Checked);
            brakeRep.MdiParent = this.MdiParent;
            brakeRep.Show();
        }

        private void Form_brake_dinner_Load(object sender, EventArgs e)
        {
            idEmp = "";
        }
    }
}
