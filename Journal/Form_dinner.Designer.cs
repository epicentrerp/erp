﻿namespace Journal
{
    partial class Form_dinner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewDinner = new System.Windows.Forms.DataGridView();
            this.id_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dinner_type_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDinner)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewDinner
            // 
            this.dataGridViewDinner.AllowUserToAddRows = false;
            this.dataGridViewDinner.AllowUserToDeleteRows = false;
            this.dataGridViewDinner.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewDinner.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDinner.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_type,
            this.dinner_type_name});
            this.dataGridViewDinner.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewDinner.Name = "dataGridViewDinner";
            this.dataGridViewDinner.ReadOnly = true;
            this.dataGridViewDinner.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewDinner.RowHeadersVisible = false;
            this.dataGridViewDinner.Size = new System.Drawing.Size(260, 238);
            this.dataGridViewDinner.TabIndex = 0;
            this.dataGridViewDinner.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // id_type
            // 
            this.id_type.DataPropertyName = "id_type";
            this.id_type.HeaderText = "Column1";
            this.id_type.Name = "id_type";
            this.id_type.ReadOnly = true;
            this.id_type.Visible = false;
            // 
            // dinner_type_name
            // 
            this.dinner_type_name.DataPropertyName = "dinner_type_name";
            this.dinner_type_name.HeaderText = "Название";
            this.dinner_type_name.Name = "dinner_type_name";
            this.dinner_type_name.ReadOnly = true;
            this.dinner_type_name.Width = 200;
            // 
            // Form_dinner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.dataGridViewDinner);
            this.Name = "Form_dinner";
            this.Text = "Тип питания";
            this.Load += new System.EventHandler(this.Form_dinner_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDinner)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewDinner;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_type;
        private System.Windows.Forms.DataGridViewTextBoxColumn dinner_type_name;
    }
}