﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Journal
{
    public partial class Form_reason : Form
    {
        public string id_reason, name_reason;

        public Form_reason()
        {
            InitializeComponent();
        }

        private void Form_reason_Load(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand(); 
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            DataSet ds = new DataSet();

            string sql = "SELECT id, reasons FROM [shedule_shift_chenge_reason] ORDER BY id";

            sqlCmd.Connection = sqlConn;
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = sql;
            sqlCmd.Parameters.AddWithValue("@id_user", ProSetting.User);

            try
            {
                sqlConn.Open();

                dataAdapter.SelectCommand = sqlCmd;
                dataAdapter.Fill(ds);
                dataGridViewReason.DataSource = ds.Tables[0].DefaultView;
            }
            catch (SqlException)
            {
                
            }
            finally
            {
                sqlConn.Close();
            }
        }

        private void dataGridViewReason_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            id_reason = dataGridViewReason[0, dataGridViewReason.CurrentCell.RowIndex].Value.ToString();
            name_reason = dataGridViewReason[1, dataGridViewReason.CurrentCell.RowIndex].Value.ToString();
            this.Close();
        }
    }
}
