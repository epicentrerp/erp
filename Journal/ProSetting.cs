﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Journal
{
    public static class ProSetting
    {
        private static int idUser;
        private static int IdAccess;
        private static string connString;

        public static int User
        {
            get { return idUser; }
            set { idUser = value; }
        }

        public static int Access
        {
            get { return IdAccess; }
            set { IdAccess = value; }
        }

        public static string ConnetionString
        {
            get { return connString; }
            set { connString = value; }
        }
    }
}
