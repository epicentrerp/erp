﻿namespace Journal
{
    partial class Form_reason
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewReason = new System.Windows.Forms.DataGridView();
            this.id_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dinner_type_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReason)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewReason
            // 
            this.dataGridViewReason.AllowUserToAddRows = false;
            this.dataGridViewReason.AllowUserToDeleteRows = false;
            this.dataGridViewReason.AllowUserToResizeRows = false;
            this.dataGridViewReason.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewReason.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReason.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_type,
            this.dinner_type_name});
            this.dataGridViewReason.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewReason.Name = "dataGridViewReason";
            this.dataGridViewReason.ReadOnly = true;
            this.dataGridViewReason.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewReason.RowHeadersVisible = false;
            this.dataGridViewReason.Size = new System.Drawing.Size(352, 269);
            this.dataGridViewReason.TabIndex = 0;
            this.dataGridViewReason.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewReason_CellDoubleClick);
            // 
            // id_type
            // 
            this.id_type.DataPropertyName = "id";
            this.id_type.HeaderText = "id";
            this.id_type.Name = "id_type";
            this.id_type.ReadOnly = true;
            this.id_type.Visible = false;
            // 
            // dinner_type_name
            // 
            this.dinner_type_name.DataPropertyName = "reasons";
            this.dinner_type_name.HeaderText = "Название";
            this.dinner_type_name.Name = "dinner_type_name";
            this.dinner_type_name.ReadOnly = true;
            this.dinner_type_name.Width = 200;
            // 
            // Form_reason
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 293);
            this.Controls.Add(this.dataGridViewReason);
            this.Name = "Form_reason";
            this.Text = "Причины";
            this.Load += new System.EventHandler(this.Form_reason_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReason)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewReason;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_type;
        private System.Windows.Forms.DataGridViewTextBoxColumn dinner_type_name;
    }
}