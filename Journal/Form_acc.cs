﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Journal
{
    public partial class Form_acc : Form
    {
        public string id_acc, name_acc;
        public Form_acc()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            id_acc = dataGridViewAccess[0, dataGridViewAccess.CurrentCell.RowIndex].Value.ToString();
            name_acc = dataGridViewAccess[1, dataGridViewAccess.CurrentCell.RowIndex].Value.ToString();
            this.Close();
        }

        private void Form_sys_access_Load(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            string sql = "SELECT [id_access],[name_acccess] FROM [users_access]";
            SqlCommand sqlCmd = new SqlCommand(sql, sqlConn);

            sqlCmd.CommandType = CommandType.Text;

            sqlConn.Open();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = sqlCmd;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            dataGridViewAccess.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }
    }
}
