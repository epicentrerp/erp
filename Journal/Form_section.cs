﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Journal
{
    public partial class Form_section : Form
    {
        public string id_section, name_section;
        public Form_section()
        {
            InitializeComponent();
        }

        private void Form_section_Load(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            DataSet ds = new DataSet();

            string sql = "SELECT [id], dep, [name] FROM [section] WHERE dep in (SELECT [dep]  FROM [users_dep]  WHERE [id_users] = @id_user) ORDER BY dep";


            sqlCmd.Connection = sqlConn;
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.Parameters.AddWithValue("@id_user", ProSetting.User);
            sqlCmd.CommandText = sql;

            try
            {
                sqlConn.Open();

                dataAdapter.SelectCommand = sqlCmd;
                dataAdapter.Fill(ds);
                dataGridViewSection.DataSource = ds.Tables[0].DefaultView;
            }
            catch (SqlException)
            {

            }
            finally
            {
                sqlConn.Close();
            }
        }

        private void dataGridViewSection_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            id_section = dataGridViewSection[0, dataGridViewSection.CurrentCell.RowIndex].Value.ToString();
            name_section = dataGridViewSection[2, dataGridViewSection.CurrentCell.RowIndex].Value.ToString();
            this.Close();
        }
    }
}
