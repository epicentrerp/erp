﻿namespace Journal
{
    partial class Form_acc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewAccess = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAccess)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewAccess
            // 
            this.dataGridViewAccess.AllowUserToAddRows = false;
            this.dataGridViewAccess.AllowUserToDeleteRows = false;
            this.dataGridViewAccess.AllowUserToResizeRows = false;
            this.dataGridViewAccess.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewAccess.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAccess.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewAccess.Name = "dataGridViewAccess";
            this.dataGridViewAccess.ReadOnly = true;
            this.dataGridViewAccess.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewAccess.RowHeadersVisible = false;
            this.dataGridViewAccess.Size = new System.Drawing.Size(491, 389);
            this.dataGridViewAccess.TabIndex = 0;
            this.dataGridViewAccess.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // Form_acc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 413);
            this.Controls.Add(this.dataGridViewAccess);
            this.Name = "Form_acc";
            this.Text = "Уровень доступа";
            this.Load += new System.EventHandler(this.Form_sys_access_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAccess)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewAccess;

    }
}