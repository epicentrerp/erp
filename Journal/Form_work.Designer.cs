﻿namespace Journal
{
    partial class Form_work
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewWork = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWork)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewWork
            // 
            this.dataGridViewWork.AllowUserToAddRows = false;
            this.dataGridViewWork.AllowUserToDeleteRows = false;
            this.dataGridViewWork.AllowUserToResizeRows = false;
            this.dataGridViewWork.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewWork.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewWork.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewWork.Name = "dataGridViewWork";
            this.dataGridViewWork.ReadOnly = true;
            this.dataGridViewWork.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewWork.RowHeadersVisible = false;
            this.dataGridViewWork.Size = new System.Drawing.Size(397, 325);
            this.dataGridViewWork.TabIndex = 0;
            this.dataGridViewWork.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // Form_work
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 349);
            this.Controls.Add(this.dataGridViewWork);
            this.Name = "Form_work";
            this.Text = "Form_work";
            this.Load += new System.EventHandler(this.Form_work_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWork)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewWork;
    }
}