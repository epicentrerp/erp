﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Journal
{
    public partial class Form_emp : Form
    {
        public string id ="", name, dep;
        
        public Form_emp(string dep)
        {
            InitializeComponent();
            this.dep = dep;
        }

        private void Form_emp_Load(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            DataSet ds = new DataSet();

            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.Connection = sqlConn;

            string sql = "";
            if (dep == "")
            {
                sql = "SELECT [id],[dep],[name_ru],[barcode] FROM [employees] order by dep, name_ru ";
            }
            else
            {
                sql = "SELECT [id],[dep],[name_ru],[barcode] FROM [employees] WHERE dep = @dep ORDER BY dep, name_ru ";
                sqlCmd.Parameters.AddWithValue("@dep", dep);
            }

            sqlCmd.CommandText = sql;

            sqlConn.Open();
                dataAdapter.SelectCommand = sqlCmd;
                dataAdapter.Fill(ds);
                dataGridViewEmp.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            id = dataGridViewEmp[0, dataGridViewEmp.CurrentCell.RowIndex].Value.ToString();
            name = dataGridViewEmp[2, dataGridViewEmp.CurrentCell.RowIndex].Value.ToString();
            this.Close();
        }
    }
}
