﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Journal
{
    public partial class Form_dep : Form
    {
        public string dep;
        public int idUser;
        public Form_dep(int idUser)
        {
            InitializeComponent();
            this.idUser = idUser;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            dep = dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString();
            this.Close();
        }

        private void Form_dep_Load(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = null;
            string sql;
            if (idUser == 0)
            {
                sql = "SELECT [dep] FROM [dep] order by dep";
                sqlCmd = new SqlCommand(sql, sqlConn);
            }
            else
            {
                sql = "SELECT [dep] FROM [dep]  WHERE dep in (SELECT [dep]  FROM [users_dep]  WHERE [id_users] = @id_user) order by dep";
                sqlCmd = new SqlCommand(sql, sqlConn);
                sqlCmd.Parameters.AddWithValue("@id_user", ProSetting.User);
            }

            sqlCmd.CommandType = CommandType.Text;

            sqlConn.Open();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = sqlCmd;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            dataGridView1.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }
    }
}
