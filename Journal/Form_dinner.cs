﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Journal
{
    public partial class Form_dinner : Form
    {
        public string dinnerName, dinnerId;

        public Form_dinner()
        {
            InitializeComponent();
        }

        private void Form_dinner_Load(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = null;
            string sql = "SELECT [id_type],[dinner_type_name] FROM [shedule_shift_dinner] ";
            sqlCmd = new SqlCommand(sql, sqlConn);
            sqlCmd.Parameters.AddWithValue("@id_user", ProSetting.User);


            sqlCmd.CommandType = CommandType.Text;

            sqlConn.Open();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = sqlCmd;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            dataGridViewDinner.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            dinnerId = dataGridViewDinner[0, dataGridViewDinner.CurrentCell.RowIndex].Value.ToString();
            dinnerName = dataGridViewDinner[1, dataGridViewDinner.CurrentCell.RowIndex].Value.ToString();
            this.Close();
        }
    }
}
