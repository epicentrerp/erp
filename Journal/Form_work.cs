﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Journal
{
    public partial class Form_work : Form
    {
        public string id="", name;
        public Form_work()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            id = dataGridViewWork[0, dataGridViewWork.CurrentCell.RowIndex].Value.ToString();
            name = dataGridViewWork[1, dataGridViewWork.CurrentCell.RowIndex].Value.ToString();
            this.Close();
        }

        private void Form_work_Load(object sender, EventArgs e)
        {
            SqlConnection sqlConn = new SqlConnection(ProSetting.ConnetionString);
            SqlCommand sqlCmd = new SqlCommand();
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            DataSet ds = new DataSet();

            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.Connection = sqlConn;

            string sql = "SELECT [id],[work] FROM [balu].[dbo].[shedule_shift_work] ORDER BY id";


            sqlCmd.CommandText = sql;

            sqlConn.Open();
            dataAdapter.SelectCommand = sqlCmd;
            dataAdapter.Fill(ds);
            dataGridViewWork.DataSource = ds.Tables[0].DefaultView;
            sqlConn.Close();
        }
    }
}
