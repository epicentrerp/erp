﻿namespace Journal
{
    partial class Form_emp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewEmp = new System.Windows.Forms.DataGridView();
            this.id_emp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dep_column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name_ru = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmp)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewEmp
            // 
            this.dataGridViewEmp.AllowUserToAddRows = false;
            this.dataGridViewEmp.AllowUserToDeleteRows = false;
            this.dataGridViewEmp.AllowUserToResizeRows = false;
            this.dataGridViewEmp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewEmp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_emp,
            this.dep_column,
            this.name_ru,
            this.barcode});
            this.dataGridViewEmp.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewEmp.MultiSelect = false;
            this.dataGridViewEmp.Name = "dataGridViewEmp";
            this.dataGridViewEmp.ReadOnly = true;
            this.dataGridViewEmp.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewEmp.RowHeadersVisible = false;
            this.dataGridViewEmp.Size = new System.Drawing.Size(780, 482);
            this.dataGridViewEmp.TabIndex = 0;
            this.dataGridViewEmp.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // id_emp
            // 
            this.id_emp.DataPropertyName = "id";
            this.id_emp.HeaderText = "";
            this.id_emp.Name = "id_emp";
            this.id_emp.ReadOnly = true;
            this.id_emp.Width = 50;
            // 
            // dep_column
            // 
            this.dep_column.DataPropertyName = "dep";
            this.dep_column.HeaderText = "Отдел";
            this.dep_column.Name = "dep_column";
            this.dep_column.ReadOnly = true;
            // 
            // name_ru
            // 
            this.name_ru.DataPropertyName = "name_ru";
            this.name_ru.HeaderText = "ФИО";
            this.name_ru.Name = "name_ru";
            this.name_ru.ReadOnly = true;
            this.name_ru.Width = 300;
            // 
            // barcode
            // 
            this.barcode.DataPropertyName = "barcode";
            this.barcode.HeaderText = "barcode";
            this.barcode.Name = "barcode";
            this.barcode.ReadOnly = true;
            this.barcode.Visible = false;
            // 
            // Form_emp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 506);
            this.Controls.Add(this.dataGridViewEmp);
            this.Name = "Form_emp";
            this.Text = "Сотрдуники";
            this.Load += new System.EventHandler(this.Form_emp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEmp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewEmp;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_emp;
        private System.Windows.Forms.DataGridViewTextBoxColumn dep_column;
        private System.Windows.Forms.DataGridViewTextBoxColumn name_ru;
        private System.Windows.Forms.DataGridViewTextBoxColumn barcode;
    }
}